<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use webvimark\modules\UserManagement\UserManagementModule;
use app\models\Profesionista;
use app\models\Usuaria;

use yii\helpers\Url;
$url = Url::to(['/src/img/servicio.png']);

//var_dump(Citas::descartados());die;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<aside class="main-sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src=<?= $url ?> class="img-circle"/>
            </div>
            <div class="pull-left info">
                <p><?= \Yii::$app->user->username ?></p>

                <a href="/user-management/auth/logout"><i class="fa fa-circle text-success"></i> Cerrar Sesión</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    [
                        'label' => 'Menu', 
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Gii', 
                        'icon' => 'file-code-o', 
                        'url' => ['/gii'],
                        'visible'=>Yii::$app->user->identity->superadmin,
                    ],
                    [
                        'label' => 'Estadisticas', 
                        'icon' => 'laptop', 
                        'url' => ['/usuaria/index-municipio'],
                        'visible'=>Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin']) : false,
                    ],
                    [
                        'label' => 'Mis citas', 
                        'icon' => 'calendar', 
                        'url' => ['/profesionista/profesionista'],
                        'visible'=> Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['profesionista']) : false,
                    ],
                    [
                        'label' => 'Administración',
                        'icon' => 'cog',
                        'url' => '#',
                        'visible' =>Yii::$app->user->identity->superadmin || Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin']) : false,
                        'items' => [
                            [
                                'label'=>'Usuarios',
                                'icon'=>'user',
                                'url'=>'/user-management/user/index',
                            ],
                            [
                                'label'=>'Roles',
                                'icon'=>'500px',
                                'url'=>'/user-management/role/index',
                                'visible' =>Yii::$app->user->identity->superadmin,
                            ],
                            [
                                'label'=>'Permisos',
                                'icon'=>'cubes',
                                'url'=>'/user-management/permission/index',
                                'visible' =>Yii::$app->user->identity->superadmin,
                            ],
                            [
                                'label'=>'Grupos de permisos',
                                'icon'=>'th',
                                'url'=>'/user-management/auth-item-group/index',
                                'visible' =>Yii::$app->user->identity->superadmin,
                            ],
                            [
                                'label'=>'Registro de visitas',
                                'icon'=>'edit ',
                                'url'=>'/user-management/user-visit-log/index',
                                'visible' =>Yii::$app->user->identity->superadmin,
                            ],
                        ],
                    ],
                    [
                        'label' => 'Capacitaciones', 
                        'icon' => 'laptop', 
                        'url' => ['/capacitaciones'],
                        'visible'=>Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin']) : false,
                    ],
                    [
                        'label' => 'Catálogos',
                        'icon' => 'bars',
                        'url' => '#',
                        'visible' => Yii::$app->user->identity->superadmin || Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin']) : false,
                        'items' => [
                            ['label' => 'Discapacidad', 'icon' => 'wheelchair', 'url' => ['/discapacidad'],],
                            ['label' => 'Enfermedades Cronicas', 'icon' => 'ambulance', 'url' => ['/enfermedades-cronicas'],],
                            ['label' => 'Especialidad', 'icon' => 'ellipsis-v ', 'url' => ['/especialidad'],],
                            ['label' => 'Estado civil', 'icon' => 'heart ', 'url' => ['/estado-civil'],],
                            ['label' => 'Estatus', 'icon' => 'spinner  ', 'url' => ['/estatus'],],
                            ['label' => 'Modalodad Violencia', 'icon' => 'spinner  ', 'url' => ['/modalidad-violencia'],],
                            ['label' => 'Nivel Estudio', 'icon' => 'spinner  ', 'url' => ['/nivel-estudio'],],
                            ['label' => 'Tipo Capacitación', 'icon' => 'spinner  ', 'url' => ['/tipo-capacitacion'],],
                            ['label' => 'Tipo Servicio', 'icon' => 'spinner  ', 'url' => ['/tipo-servicio'],],
                            ['label' => 'Tipos Violencia', 'icon' => 'spinner  ', 'url' => ['/tipos-violencia'],],
                        ],
                    ],
                    [
                        'label' => 'Citas', 
                        'icon' => 'calendar', 
                        'url' => ['/citas'],
                        'visible'=> Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['superadmin', 'profesionista', 'callcenter']) : false,
                    ],
                    [
                        'label' => 'Horarios', 
                        'icon' => 'clock-o ', 
                        'url' => ['/horarios'],
                        'visible'=> Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['superadmin' ,'seguimiento']) : false,
                    ],
                    [
                        'label' => 'Profesionista', 
                        'icon' => 'group ', 
                        'url' => ['/profesionista'],
                        'visible'=> Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin']) : false,
                    ],
                    [
                        'label' => 'Registro', 
                        'icon' => 'female ', 
                        'url' => ['/usuaria'],
                        'visible'=> Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'callcenter', 'supervisor' ]) : false,
                    ],
                    [
                        'label' => 'Configuracion de agenda',
                        'icon' => 'calendar',
                        'url' => '#',
                        'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor', 'profesionista']) : false || Yii::$app->user->identity->superadmin,
                        'items' => [
                            ['label' => 'Configuracion de ausencias', 
                            'icon' =>"calendar-times-o", 
                            'aria-hidden'=>'true', 
                            'url' => ['/agenda-configuracion'],
                            'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor']) : false || Yii::$app->user->identity->superadmin,
                            ],
                            ['label' => 'Configuracion de dias', 
                            'icon' =>"calendar-times-o", 
                            'aria-hidden'=>'true', 
                            'url' => ['/dias/update/1'],
                            'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor']) : false || Yii::$app->user->identity->superadmin,
                            ],
                            ['label' => 'Configuracion de profesionistas', 
                            'icon' => 'calendar', 
                            'url' => ['/configuracion-profesionista'],
                            'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor', 'profesionista']) : false || Yii::$app->user->identity->superadmin,
                            ],
                            
                        ],
                    ],
                    [
                        'label' => 'Concluidos: ' .  Usuaria::concluidos(),
                        'options' => ['class' => 'header background-bluedark text-bluedark'],
                        'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor']) : false || Yii::$app->user->identity->superadmin,
                    ],
                    [
                        'label' => 'Descartados: ' . Usuaria::descartados(),
                        'options' => ['class' => 'header background-bluedark text-bluedark'],
                        'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor']) : false || Yii::$app->user->identity->superadmin,
                    ],
                    [
                        'label' => 'Sin actualizar: ' . Usuaria::sinActualizar(),
                        'options' => ['class' => 'header background-bluedark text-bluedark'],
                        'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor']) : false || Yii::$app->user->identity->superadmin,
                    ],
                    [
                        'label' => 'En proceso: ' . Usuaria::enProceso(),
                        'options' => ['class' => 'header background-bluedark text-bluedark'],
                        'visible' => Yii::$app->user->isGuest == false ? Yii::$app->user->identity->hasRole(['Admin', 'supervisor']) : false || Yii::$app->user->identity->superadmin,
                    ],
                    
                    
                ],
            ]
        ) ?>

    </section>
    
</aside>

<?php

$this->registerCss("

.background-blue { background: #007bff!important; }
.background-green { background: #28a745!important; }
.background-red { background: #dc3545!important; }
.background-yellow { background: #ffc107!important; }
.background-bluedark { background: #3c8dbc !important;}
.text-bluedark {color: #fff !important;}

");

?>