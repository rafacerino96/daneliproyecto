<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgendaConfiguracion;

/**
 * AgendaConfiguracionSearch represents the model behind the search form of `app\models\AgendaConfiguracion`.
 */
class AgendaConfiguracionSearch extends AgendaConfiguracion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age_id_configuracion'], 'integer'],
            [['age_inicio', 'age_fin', 'age_motivo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgendaConfiguracion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'age_id_configuracion' => $this->age_id_configuracion,
            'age_inicio' => $this->age_inicio,
            'age_fin' => $this->age_fin,
        ]);

        $query->andFilterWhere(['like', 'age_motivo', $this->age_motivo]);

        return $dataProvider;
    }
}
