<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tipos_violencia}}".
 *
 * @property int $vio_id_tipo
 * @property string|null $vio_nombre
 *
 * @property Usuaria[] $usuarias
 */
class TiposViolencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tipos_violencia}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vio_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vio_id_tipo' => 'Id Tipo de violencia',
            'vio_nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id_tipo_violencia' => 'vio_id_tipo']);
    }
}
