<?php

namespace app\models;

use yii\base\Model; 
use yii\data\ActiveDataProvider;
use app\models\ConfiguracionProfesionista;

/**
 * ConfiguracionProfesionistaSearch represents the model behind the search form of `app\models\ConfiguracionProfesionista`.
 */
class ConfiguracionProfesionistaSearch extends ConfiguracionProfesionista
{
    /**
     * {@inheritdoc}
     */

    public $nombreProfesionista;
    public $fkProfesionistaMunicipio;
    public $status;

    public function rules()
    {
        return [
            [['con_id', 'con_fkProfesionista', 'con_status'], 'integer'],
            [['status', 'nombreProfesionista', 'fkProfesionistaMunicipio', 'con_dia_inicial', 'con_dia_final'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
            
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConfiguracionProfesionista::find()
            ->where(['con_status' => [1,2,3]])
            ->joinWith(['conFkProfesionista']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => array_merge(
                $dataProvider->getSort()->attributes,
                [
                    'nombreProfesionista' => [
                        'asc'       => ['pro_nombre' => SORT_ASC],
                        'desc'      => ['pro_nombre' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                ]
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'con_id' => $this->con_id,
            'con_fkProfesionista' => $this->con_fkProfesionista,
            'con_dia_inicial' => $this->con_dia_inicial,
            'con_dia_final' => $this->con_dia_final,
            'con_status' => $this->con_status,
            'profesionista.pro_municipio_atencion' => $this->fkProfesionistaMunicipio,
        ]);

        $query->andFilterWhere(['like', "concat_ws(' ', pro_nombre, pro_apellidop, pro_apellidom)", $this->nombreProfesionista]);

        return $dataProvider;
    }
}
