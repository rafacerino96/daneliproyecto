<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%modalidad_violencia}}".
 *
 * @property int $mod_id_violencia
 * @property string|null $mod_nombre
 *
 * @property Usuaria[] $usuarias
 */
class ModalidadViolencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%modalidad_violencia}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mod_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mod_id_violencia' => 'Id Violencia',
            'mod_nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id_modalidad_violencia' => 'mod_id_violencia']);
    }
}
