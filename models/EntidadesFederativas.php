<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entidades_federativas".
 *
 * @property int $catalog_key identificador único de la tabla y la clave de la entidad federativa.
 * @property string $entidad_federativa registra el nombre de la entidad federativa
 * @property string|null $abreviatura
 *
 * @property Municipios[] $municipios
 * @property Usuaria[] $usuarias
 */
class EntidadesFederativas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entidades_federativas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['catalog_key', 'entidad_federativa'], 'required'],
            [['catalog_key'], 'string', 'max' => 50],
            [['entidad_federativa'], 'string', 'max' => 255],
            [['abreviatura'], 'string', 'max' => 2],
            [['catalog_key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'catalog_key' => 'Catalog Key',
            'entidad_federativa' => 'Entidad Federativa',
            'abreviatura' => 'Abreviatura',
        ];
    }

    /**
     * Gets query for [[Municipios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipios()
    {
        return $this->hasMany(Municipios::className(), ['id_entidad_federativa' => 'catalog_key']);
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_entidad_federativa' => 'catalog_key']);
    }
}
