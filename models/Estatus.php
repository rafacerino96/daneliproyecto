<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%estatus}}".
 *
 * @property int $est_id_estatus
 * @property string|null $est_nombre
 *
 * @property Usuaria[] $usuarias
 */
class Estatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%estatus}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['est_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'est_id_estatus' => 'Id Estatus',
            'est_nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id_estatus' => 'est_id_estatus']);
    }

    public static function map()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'est_id_estatus', 'est_nombre');
    }
}
