<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "localidades".
 *
 * @property string $cve_ent
 * @property int $cve_mun
 * @property int $cve_loc
 * @property string|null $nom_loc
 * @property string|null $Estatus
 */
class Localidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'localidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cve_ent', 'cve_mun', 'cve_loc'], 'required'],
            [['cve_ent', 'cve_mun', 'cve_loc', 'Estatus'], 'string', 'max' => 50],
            [['nom_loc'], 'string', 'max' => 250],
            [['cve_ent', 'cve_mun', 'cve_loc'], 'unique', 'targetAttribute' => ['cve_ent', 'cve_mun', 'cve_loc']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cve_ent' => 'Cve Ent',
            'cve_mun' => 'Cve Mun',
            'cve_loc' => 'Cve Loc',
            'nom_loc' => 'Nom Loc',
            'Estatus' => 'Estatus',
        ];
    }
}
