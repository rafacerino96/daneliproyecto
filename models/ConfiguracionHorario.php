<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%configuracion_horario}}".
 *
 * @property int $conf_id
 * @property string $conf_descripcion
 *
 * @property Horarios[] $horarios
 */
class ConfiguracionHorario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%configuracion_horario}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['conf_id', 'conf_descripcion'], 'required'],
            [['conf_id'], 'integer'],
            [['conf_descripcion'], 'string', 'max' => 100],
            [['conf_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'conf_id' => 'Conf ID',
            'conf_descripcion' => 'Conf Descripcion',
        ];
    }

    /**
     * Gets query for [[Horarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHorarios()
    {
        return $this->hasMany(Horarios::className(), ['hor_horario' => 'conf_id']);
    }
}
