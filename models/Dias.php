<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dias".
 *
 * @property int $dia_id id
 * @property int|null $dia_lunes Trabaja lunes
 * @property int|null $dia_martes Trabaja martes
 * @property int|null $dia_miercoles Trabaja miercoles
 * @property int|null $dia_jueves Trabaja jueves
 * @property int|null $dia_viernes Trabaja viernes
 * @property int|null $dia_sabado Trabaja sabado
 * @property int|null $dia_domingo Trabaja domingo
 */
class Dias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dia_id'], 'required'],
            [['dia_id', 'dia_lunes', 'dia_martes', 'dia_miercoles', 'dia_jueves', 'dia_viernes', 'dia_sabado', 'dia_domingo'], 'integer'],
            [['dia_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dia_id' => 'id',
            'dia_lunes' => 'Lunes',
            'dia_martes' => 'Martes',
            'dia_miercoles' => 'Miercoles',
            'dia_jueves' => 'Jueves',
            'dia_viernes' => 'Viernes',
            'dia_sabado' => 'Sabado',
            'dia_domingo' => 'Domingo',
        ];
    }
}
