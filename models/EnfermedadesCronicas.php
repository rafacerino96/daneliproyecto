<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%enfermedades_cronicas}}".
 *
 * @property int $enf_id_enfermedades_cronicas
 * @property string|null $enf_nombre
 *
 * @property Usuaria[] $usuarias
 */
class EnfermedadesCronicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%enfermedades_cronicas}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enf_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'enf_id_enfermedades_cronicas' => 'Id Enfermedades Cronicas',
            'enf_nombre' => 'Nombre de enfermedad cronica',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id_enfermedades_cronicas' => 'enf_id_enfermedades_cronicas']);
    }
}
