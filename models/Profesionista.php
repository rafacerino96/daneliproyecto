<?php

namespace app\models;
 
use Yii;

use webvimark\modules\UserManagement\models\User;

/**
 * This is the model class for table "{{%profesionista}}".
 *
 * @property int $pro_id
 * @property string|null $pro_nombre
 * @property string|null $pro_apellidop
 * @property string|null $pro_apellidom
 * @property string $pro_municipio_atencion
 * @property int $pro_id_especialidad
 * @property int $pro_id_user
 *
 * @property Citas[] $citas
 * @property Horarios[] $horarios
 * @property Especialidad $proIdEspecialidad
 * @property User $proIdUser
 */
class Profesionista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%profesionista}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_municipio_atencion', 'pro_id_especialidad', 'pro_id_user', 'pro_nombre', 'pro_apellidop', 'pro_apellidom'], 'required'],
            [['pro_id_especialidad', 'pro_id_user'], 'integer'],
            [['pro_nombre', 'pro_apellidop', 'pro_apellidom', 'pro_municipio_atencion', 'pro_entidad'], 'string', 'max' => 50],
            [['pro_id_especialidad'], 'exist', 'skipOnError' => true, 'targetClass' => Especialidad::className(), 'targetAttribute' => ['pro_id_especialidad' => 'esp_id']],
            [['pro_id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pro_id_user' => 'id']],
            [['pro_municipio_atencion'], 'exist', 'skipOnError' => true, 'targetClass' => Municipios::className(), 'targetAttribute' => ['pro_municipio_atencion' => 'catalog_key_mun']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pro_id' => 'Pro ID',
            'pro_nombre' => 'Nombre',
            'pro_apellidop' => 'Apellido paterno',
            'pro_apellidom' => 'Apellido materno',
            'pro_municipio_atencion' => 'Municipio de atención',
            'pro_id_especialidad' => 'Especialidad',
            'pro_id_user' => 'Usuario',
            'pro_entidad' => 'Entidad Federativa',
            'username' => 'Usuario',
            'especialidad' => 'Especialidad',
            'municipioAtencion' => 'Municipio de atención',
            'nombreCompleto' => 'Nombre completo',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['cit_id_profesionista' => 'pro_id']);
    }

    /**
     * Gets query for [[Horarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHorarios()
    {
        return $this->hasMany(Horarios::className(), ['hor_id_profesionista' => 'pro_id']);
    }

    /**
     * Gets query for [[ProIdEspecialidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProIdEspecialidad()
    {
        return $this->hasOne(Especialidad::className(), ['esp_id' => 'pro_id_especialidad']);
    }

    /**
     * Gets query for [[ProIdUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'pro_id_user']);
    }

    public function getProMunicipioAtencion()
    {
        
        //var_dump($this->hasOne(Municipios::className(), ['catalog_key_mun' => 'pro_municipio_atencion', 'efe_key' => '27']));die;
        //var_dump(Municipios::findOne(['efe_key' => '27']));die;
        return $this->hasOne(Municipios::className(), ['catalog_key_mun' => 'pro_municipio_atencion', 'efe_key' => 'pro_entidad']);
    }

    //funciones creadas
    public function getUsername()
    {
        return $this->proIdUser->username;
    }

    public function getEspecialidad()
    {
        return $this->proIdEspecialidad->esp_nombre;
    }

    public function getMunicipioAtencion()
    {
        return $this->proMunicipioAtencion->municipio;
    }

    public function getNombreCompleto()
    {
        return $this->pro_nombre . " " . $this->pro_apellidop . " " . $this->pro_apellidom;
    }

    public static function isProfesionista($fk =null)
    {
        if($fk == null){
            return false;
        }

        $profesionista = self::findOne(['pro_id_user' => $fk]);

        if($profesionista == null){
            return false;
        }

        return true;
    }

}
