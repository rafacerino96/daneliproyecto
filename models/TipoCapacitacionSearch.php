<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TipoCapacitacion;

/**
 * TipoCapacitacionSearch represents the model behind the search form of `app\models\TipoCapacitacion`.
 */
class TipoCapacitacionSearch extends TipoCapacitacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tip_id_capacitacion'], 'integer'],
            [['tip_nombre', 'tip_descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TipoCapacitacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tip_id_capacitacion' => $this->tip_id_capacitacion,
        ]);

        $query->andFilterWhere(['like', 'tip_nombre', $this->tip_nombre])
            ->andFilterWhere(['like', 'tip_descripcion', $this->tip_descripcion]);

        return $dataProvider;
    }
}
