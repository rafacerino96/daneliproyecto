<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%capacitaciones}}".
 *
 * @property int $cap_id
 * @property string $cap_nombre
 * @property string $cap_feha
 * @property string $cap_hora_inicio
 * @property string $cap_hora_termino
 * @property string $cap_dependencia
 * @property int|null $cap_total
 * @property int|null $cap_hombres
 * @property int|null $cap_mujeres
 * @property int $cap_tip_id
 *
 * @property TipoCapacitacion $capTip
 */
class Capacitaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%capacitaciones}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cap_nombre', 'cap_feha', 'cap_hora_inicio', 'cap_hora_termino', 'cap_dependencia', 'cap_tip_id'], 'required'],
            [['cap_feha', 'cap_hora_inicio', 'cap_hora_termino'], 'safe'],
            [['cap_total', 'cap_hombres', 'cap_mujeres', 'cap_tip_id'], 'integer'],
            [['cap_nombre', 'cap_dependencia'], 'string', 'max' => 200],
            [['cap_tip_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoCapacitacion::className(), 'targetAttribute' => ['cap_tip_id' => 'tip_id_capacitacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cap_id' => 'ID',
            'cap_nombre' => 'Nombre',
            'cap_feha' => 'Fecha',
            'cap_hora_inicio' => 'Hora de inicio',
            'cap_hora_termino' => 'Hora de finalización',
            'cap_dependencia' => 'Dependencia',
            'cap_total' => 'Total de asistentes',
            'cap_hombres' => 'Asitentes hombres',
            'cap_mujeres' => 'Asistentes mujeres',
            'cap_tip_id' => 'Tipo de capacitación',
            'tipoCapacitacion' => 'Tipo de capacitación'
        ];
    }

    /**
     * Gets query for [[CapTip]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapTip()
    {
        return $this->hasOne(TipoCapacitacion::className(), ['tip_id_capacitacion' => 'cap_tip_id']);
    }

    public function getTipoCapacitacion()
    {
        return $this->capTip->tip_nombre;
    }
}
