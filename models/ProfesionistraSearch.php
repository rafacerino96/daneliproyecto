<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profesionista;

/**
 * ProfesionistraSearch represents the model behind the search form of `app\models\Profesionista`.
 */
class ProfesionistraSearch extends Profesionista
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pro_id', 'pro_id_especialidad', 'pro_id_user'], 'integer'],
            [['pro_nombre', 'pro_apellidop', 'pro_apellidom', 'pro_municipio_atencion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profesionista::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pro_id' => $this->pro_id,
            'pro_id_especialidad' => $this->pro_id_especialidad,
            'pro_id_user' => $this->pro_id_user,
        ]);

        $query->andFilterWhere(['like', 'pro_nombre', $this->pro_nombre])
            ->andFilterWhere(['like', 'pro_apellidop', $this->pro_apellidop])
            ->andFilterWhere(['like', 'pro_apellidom', $this->pro_apellidom])
            ->andFilterWhere(['like', 'pro_municipio_atencion', $this->pro_municipio_atencion]);

        return $dataProvider;
    }
}
