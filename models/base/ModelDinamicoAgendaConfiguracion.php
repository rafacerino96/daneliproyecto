<?php

namespace app\models\base;

use Yii;
use yii\helpers\ArrayHelper;

class ModelDinamicoAgendaConfiguracion extends \yii\base\Model
{
    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'age_id_configuracion', 'age_id_configuracion'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['age_id_configuracion']) && !empty($item['age_id_configuracion']) && isset($multipleModels[$item['age_id_configuracion']])) {
                    $models[] = $multipleModels[$item['age_id_configuracion']];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }
}