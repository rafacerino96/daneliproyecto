<?php

namespace app\models\base;

use Yii;
use yii\helpers\ArrayHelper;

class Model extends \yii\base\Model
{
    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model      = new $modelClass;
        $primaryKey = $model->tableSchema->primaryKey;
        $formName   = $model->formName();
        $post       = Yii::$app->request->post($formName);
        $models     = [];

        foreach ($primaryKey as $iK => $pk):

            if (! empty($multipleModels)) {
                $keys = array_keys(ArrayHelper::map($multipleModels, $primaryKey, $primaryKey));
                $multipleModels = array_combine($keys, $multipleModels);
            }

            if ($post && is_array($post)) {
                foreach ($post as $i => $item) {
                    if (isset($item[$pk]) && !empty($item[$pk]) && isset($multipleModels[$item[$pk]])) {
                        $models[] = $multipleModels[$item[$pk]];
                    } else {
                        $models[] = new $modelClass;
                    }
                }
            }

            unset($model, $formName, $post);

        endforeach;

        return $models;
    }
}