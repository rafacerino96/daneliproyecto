<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "municipios".
 *
 * @property int $catalog_key_mun identificador único de la tabla.
 * @property string|null $municipio nombre del municipio
 * @property int $efe_key registra la clave del estado del municipio
 * @property string|null $estatus estatus del municipo
 *
 * @property Usuaria[] $usuarias
 */
class Municipios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'municipios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['catalog_key_mun', 'efe_key'], 'required'],
            [['catalog_key_mun', 'efe_key'], 'string', 'max' => 50],
            [['municipio'], 'string', 'max' => 150],
            [['estatus'], 'string', 'max' => 20],
            [['catalog_key_mun', 'efe_key'], 'unique', 'targetAttribute' => ['catalog_key_mun', 'efe_key']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'catalog_key_mun' => 'Catalog Key Mun',
            'municipio' => 'Municipio',
            'efe_key' => 'Efe Key',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_minicipio' => 'catalog_key_mun']);
    }

    public static function map()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->where(['efe_key' => '27'])->all(), 'catalog_key_mun', 'municipio');
    }
}
