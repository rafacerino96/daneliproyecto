<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, pdf'],
        ];
    }
    
    public function upload($id)
    {
        if ($this->validate()) {
            $documentname = $this->generatename(). '_' . $id . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(getcwd() . '/uploads/' . $documentname);
            //$this->imageFile->saveAs(getcwd() . '/uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return $documentname;
        } else {
            return false;
        }
    }

    public function uploadCapacitacion($id)
    {
        if ($this->validate()) {
            $documentname = $this->generatename(). '_' . $id . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(getcwd() . '/capacitaciones-uploads/' . $documentname);
            //$this->imageFile->saveAs(getcwd() . '/uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return $documentname;
        } else {
            return false;
        }
    }

    public function generatename(){
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 128);
    }
}