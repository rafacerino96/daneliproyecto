<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DocumentosCapacitaciones;

/**
 * DocumentosCapacitacionesSearch represents the model behind the search form of `app\models\DocumentosCapacitaciones`.
 */
class DocumentosCapacitacionesSearch extends DocumentosCapacitaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['docc_id', 'docc_fkcapacitacion', 'docc_estatus'], 'integer'],
            [['docc_ruta', 'docc_nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $capacitacion)
    {
        $query = DocumentosCapacitaciones::find()->where(['docc_estatus' => 1, 'docc_fkcapacitacion' => $capacitacion]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'docc_id' => $this->docc_id,
            'docc_fkcapacitacion' => $this->docc_fkcapacitacion,
            'docc_estatus' => $this->docc_estatus,
        ]);

        $query->andFilterWhere(['like', 'docc_ruta', $this->docc_ruta])
            ->andFilterWhere(['like', 'docc_nombre', $this->docc_nombre]);

        return $dataProvider;
    }
}
