<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%nivel_estudio}}".
 *
 * @property int $niv_id_nivel_estudio
 * @property string|null $niv_nombre
 *
 * @property Usuaria[] $usuarias
 */
class NivelEstudio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%nivel_estudio}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['niv_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'niv_id_nivel_estudio' => 'Id Nivel Estudio',
            'niv_nombre' => 'Nivel de Estudio',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id_nivel_estudio' => 'niv_id_nivel_estudio']);
    }
}
