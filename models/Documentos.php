<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%documentos}}".
 *
 * @property int $doc_id
 * @property string $doc_ruta
 * @property int $doc_fkusuaria
 * @property string $doc_nombre
 *
 * @property Usuaria $docFkusuaria
 */
class Documentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%documentos}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() 
    {
        return [
            [['doc_ruta', 'doc_fkusuaria', 'doc_nombre'], 'required'],
            [['doc_fkusuaria'], 'integer'],
            [['doc_id'], 'unique'],
            [['doc_ruta', 'doc_nombre'], 'string', 'max' => 255],
            [['doc_fkusuaria'], 'exist', 'skipOnError' => true, 'targetClass' => Usuaria::className(), 'targetAttribute' => ['doc_fkusuaria' => 'reg_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doc_id' => 'Doc ID',
            'doc_ruta' => 'Doc Ruta',
            'doc_fkusuaria' => 'Doc Fkusuaria',
            'doc_nombre' => 'Nombre del documento',
        ];
    }

    /**
     * Gets query for [[DocFkusuaria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocFkusuaria()
    {
        return $this->hasOne(Usuaria::className(), ['reg_id' => 'doc_fkusuaria']);
    }
}
