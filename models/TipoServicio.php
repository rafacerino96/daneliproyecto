<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tipo_servicio}}".
 *
 * @property int $serv_id_tipo
 * @property string|null $serv_nombre
 *
 * @property Citas[] $citas
 */
class TipoServicio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tipo_servicio}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['serv_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'serv_id_tipo' => 'Id Servicio',
            'serv_nombre' => 'Nombre del Servicio',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['cit_servicio' => 'serv_id_tipo']);
    }
}
