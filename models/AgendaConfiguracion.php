<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agenda_configuracion".
 *
 * @property int $age_id_configuracion id de la configuracion
 * @property string|null $age_inicio inicio de la ausencia general
 * @property string|null $age_fin fin de la ausencia general
 * @property string|null $age_motivo motivo de la ausencia general
 * @property int|null $age_status estatus de la ausencia general
 */
class AgendaConfiguracion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agenda_configuracion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age_inicio', 'age_fin'], 'safe'],
            [['age_status'], 'integer'],
            [['age_motivo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'age_id_configuracion' => 'id de la configuracion',
            'age_inicio' => 'inicio de la ausencia general',
            'age_fin' => 'fin de la ausencia general',
            'age_motivo' => 'motivo de la ausencia general',
            'age_status' => 'estatus de la ausencia general',
        ];
    }
}
