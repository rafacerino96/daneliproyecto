<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%documentos_capacitaciones}}".
 *
 * @property int $docc_id
 * @property string $docc_ruta
 * @property int $docc_fkcapacitacion
 * @property string $docc_nombre
 * @property int $docc_estatus
 *
 * @property Capacitaciones $doccFkcapacitacion
 */
class DocumentosCapacitaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%documentos_capacitaciones}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['docc_ruta', 'docc_fkcapacitacion', 'docc_nombre'], 'required'],
            [['docc_fkcapacitacion', 'docc_estatus'], 'integer'],
            [['docc_ruta', 'docc_nombre'], 'string', 'max' => 255],
            [['docc_fkcapacitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Capacitaciones::className(), 'targetAttribute' => ['docc_fkcapacitacion' => 'cap_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'docc_id' => 'Docc ID',
            'docc_ruta' => 'Docc Ruta',
            'docc_fkcapacitacion' => 'Docc Fkcapacitacion',
            'docc_nombre' => 'Docc Nombre',
            'docc_estatus' => 'Docc Estatus',
        ];
    }

    /**
     * Gets query for [[DoccFkcapacitacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoccFkcapacitacion()
    {
        return $this->hasOne(Capacitaciones::className(), ['cap_id' => 'docc_fkcapacitacion']);
    }
}
