<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%especialidad}}".
 *
 * @property int $esp_id
 * @property string|null $esp_nombre
 * @property string|null $esp_descripcion
 *
 * @property Profesionista[] $profesionistas
 */
class Especialidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%especialidad}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['esp_nombre'], 'string', 'max' => 100],
            [['esp_descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'esp_id' => 'Id Especialidad',
            'esp_nombre' => 'Nombre',
            'esp_descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Profesionistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesionistas()
    {
        return $this->hasMany(Profesionista::className(), ['pro_id_especialidad' => 'esp_id']);
    }

    public static function map()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'esp_id', 'esp_nombre');
    }
}
