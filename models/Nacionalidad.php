<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidad".
 *
 * @property int $codigo_pais
 * @property string|null $pais
 * @property string|null $clave_nacionalidad
 *
 * @property Usuaria[] $usuarias
 */
class Nacionalidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_pais'], 'required'],
            [['codigo_pais'], 'integer'],
            [['pais', 'clave_nacionalidad'], 'string', 'max' => 255],
            [['codigo_pais'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_pais' => 'Codigo Pais',
            'pais' => 'Pais',
            'clave_nacionalidad' => 'Clave Nacionalidad',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_nacionalidad' => 'codigo_pais']);
    }
}
