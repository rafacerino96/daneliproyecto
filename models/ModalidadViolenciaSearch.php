<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ModalidadViolencia;

/**
 * ModalidadViolenciaSearch represents the model behind the search form of `app\models\ModalidadViolencia`.
 */
class ModalidadViolenciaSearch extends ModalidadViolencia
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mod_id_violencia'], 'integer'],
            [['mod_nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ModalidadViolencia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mod_id_violencia' => $this->mod_id_violencia,
        ]);

        $query->andFilterWhere(['like', 'mod_nombre', $this->mod_nombre]);

        return $dataProvider;
    }
}
