<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HorasAusenciaProfesionista;

/**
 * HorasAusenciaProfesionistaSearch represents the model behind the search form of `app\models\HorasAusenciaProfesionista`.
 */
class HorasAusenciaProfesionistaSearch extends HorasAusenciaProfesionista
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hor_id', 'hor_fkConfiguracionProfesionista', 'hor_fkConfiguracionHorario'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HorasAusenciaProfesionista::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hor_id' => $this->hor_id,
            'hor_fkConfiguracionProfesionista' => $this->hor_fkConfiguracionProfesionista,
            'hor_fkConfiguracionHorario' => $this->hor_fkConfiguracionHorario,
        ]);

        return $dataProvider;
    }
}
