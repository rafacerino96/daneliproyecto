<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TiposViolencia;

/**
 * TipoViolenciaSearch represents the model behind the search form of `app\models\TiposViolencia`.
 */
class TipoViolenciaSearch extends TiposViolencia
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vio_id_tipo'], 'integer'],
            [['vio_nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiposViolencia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vio_id_tipo' => $this->vio_id_tipo,
        ]);

        $query->andFilterWhere(['like', 'vio_nombre', $this->vio_nombre]);

        return $dataProvider;
    }
}
