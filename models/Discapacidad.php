<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%discapacidad}}".
 *
 * @property int $dis_id_discapacidad
 * @property string|null $dis_nombre
 *
 * @property Usuaria[] $usuarias
 */
class Discapacidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%discapacidad}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dis_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dis_id_discapacidad' => 'Id Discapacidad',
            'dis_nombre' => 'Nombre de la discapacidad',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id_discapacidad' => 'dis_id_discapacidad']);
    }
}
