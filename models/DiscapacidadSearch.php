<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Discapacidad;

/**
 * DiscapacidadSearch represents the model behind the search form of `app\models\Discapacidad`.
 */
class DiscapacidadSearch extends Discapacidad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dis_id_discapacidad'], 'integer'],
            [['dis_nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discapacidad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dis_id_discapacidad' => $this->dis_id_discapacidad,
        ]);

        $query->andFilterWhere(['like', 'dis_nombre', $this->dis_nombre]);

        return $dataProvider;
    }
}
