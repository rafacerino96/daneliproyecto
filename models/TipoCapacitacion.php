<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tipo_capacitacion}}".
 *
 * @property int $tip_id_capacitacion
 * @property string|null $tip_nombre
 * @property string|null $tip_descripcion
 *
 * @property Capacitaciones[] $capacitaciones
 */
class TipoCapacitacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tipo_capacitacion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tip_nombre'], 'string', 'max' => 100],
            [['tip_descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tip_id_capacitacion' => 'Id Capacitacion',
            'tip_nombre' => 'Nombre',
            'tip_descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Capacitaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapacitaciones()
    {
        return $this->hasMany(Capacitaciones::className(), ['cap_tip_id' => 'tip_id_capacitacion']);
    }
}
