<?php

namespace app\models; 
use Yii; 
class ConfiguracionProfesionista extends \yii\db\ActiveRecord
{ 
    public static function tableName()
    {
        return 'configuracion_profesionista';
    } 
    public function rules()
    {
        return [
            [['con_fkProfesionista', 'con_fkProfesionista', 'con_dia_inicial', 'con_dia_final', 'con_detalle'], 'required'],
            [['con_fkProfesionista', 'con_status', 'con_todo_el_dia'], 'integer'],
            [['con_dia_inicial', 'con_dia_final'], 'safe'],
            [['con_detalle'], 'string', 'max' => 250],
            [['con_fkProfesionista'], 'exist', 'skipOnError' => true, 'targetClass' => Profesionista::className(), 'targetAttribute' => ['con_fkProfesionista' => 'pro_id']],
        ];
    } 
    public function attributeLabels()
    {
        return [
            'con_id' => 'Con ID',
            'con_fkProfesionista' => 'Profesionista',
            'con_dia_inicial' => 'Día inicial',
            'con_dia_final' => 'Dia final',
            'con_status' => 'Estatus',
            'con_todo_el_dia' => '¿Faltara algunas horas?',
            'con_detalle' => 'Descripcion de ausencia',
            'nombreProfesionista' => 'Profesionista',
            'fkProfesionistaMunicipio' => 'Municipio',
            'status' => 'Estatus de la cita',
        ];
    } 
    public function getConFkProfesionista()
    {
        return $this->hasOne(Profesionista::className(), ['pro_id' => 'con_fkProfesionista']);
    } 
    public function getHorasAusenciaProfesionistas()
    {
        return $this->hasMany(HorasAusenciaProfesionista::className(), ['hor_fkConfiguracionProfesionista' => 'con_id']);
    }

    public function getNombreProfesionista()
    {
        return $this->conFkProfesionista->nombreCompleto;
    }

    public function getFkProfesionistaMunicipio()
    {
        return $this->conFkProfesionista->pro_municipio_atencion;
    }

    public function getStatus()
    {
        if($this->con_status == 0){
            return 'Eliminada';
        }

        if($this->con_status == 1){
            return 'En proceso';
        }

        if($this->con_status == 2){
            return 'Aprobada';
        }

        if($this->con_status == 3){
            return 'No aprobada';
        }
        return 'No aplica';
    }
}
