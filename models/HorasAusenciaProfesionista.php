<?php

namespace app\models;

use Yii; 
class HorasAusenciaProfesionista extends \yii\db\ActiveRecord
{ 
    public static function tableName()
    {
        return 'horas_ausencia_profesionista';
    } 
    public function rules()
    {
        return [
            [['hor_fkConfiguracionProfesionista', 'hor_fkConfiguracionHorario'], 'integer'],
            [['hor_fkConfiguracionProfesionista'], 'exist', 'skipOnError' => true, 'targetClass' => ConfiguracionProfesionista::className(), 'targetAttribute' => ['hor_fkConfiguracionProfesionista' => 'con_id']],
            [['hor_fkConfiguracionHorario'], 'exist', 'skipOnError' => true, 'targetClass' => ConfiguracionHorario::className(), 'targetAttribute' => ['hor_fkConfiguracionHorario' => 'conf_id']],
        ];
    } 
    public function attributeLabels()
    {
        return [
            'hor_id' => 'ID',
            'hor_fkConfiguracionProfesionista' => 'Configuracion Profesionista',
            'hor_fkConfiguracionHorario' => 'Configuracion Horario',
        ];
    } 
    public function getHorFkConfiguracionProfesionista()
    {
        return $this->hasOne(ConfiguracionProfesionista::className(), ['con_id' => 'hor_fkConfiguracionProfesionista']);
    } 
    public function getHorFkConfiguracionHorario()
    {
        return $this->hasOne(ConfiguracionHorario::className(), ['conf_id' => 'hor_fkConfiguracionHorario']);
    }
}
