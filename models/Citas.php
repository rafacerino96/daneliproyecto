<?php

namespace app\models; 

use Yii;

/**
 * This is the model class for table "{{%citas}}".
 *
 * @property int $cit_id_cita 
 * @property string|null $cit_observaciones
 * @property int $cit_id_estatus
 * @property int $cit_servicio
 * @property string $cit_proceso
 * @property string|null $cit_fecha_atencion
 * @property string|null $cit_municipio
 * @property string|null $cit_fecha_registro
 * @property string|null $cit_fecha_modificacion
 * @property int $cit_id_usuaria
 * @property int $cit_id_profesionista
 * @property int $cit_horario
 *
 * @property TipoServicio $citServicio
 * @property Profesionista $citIdProfesionista
 * @property Usuaria $citIdUsuaria
 * @property ConfiguracionHorario $citHorario
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%citas}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cit_observaciones'], 'string'],
            [['cit_servicio', 'cit_proceso', 'cit_id_usuaria', 'cit_id_profesionista', 'cit_horario'], 'required'],
            [['cit_id_estatus', 'cit_servicio', 'cit_id_usuaria', 'cit_id_profesionista', 'cit_horario'], 'integer'],
            [['cit_fecha_atencion', 'cit_fecha_registro', 'cit_fecha_modificacion'], 'safe'],
            [['cit_proceso'], 'string', 'max' => 50],
            [['cit_municipio'], 'string', 'max' => 25],
            [['cit_id_estatus'], 'default', 'value' => 1],
            [['cit_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => TipoServicio::className(), 'targetAttribute' => ['cit_servicio' => 'serv_id_tipo']],
            [['cit_id_profesionista'], 'exist', 'skipOnError' => true, 'targetClass' => Profesionista::className(), 'targetAttribute' => ['cit_id_profesionista' => 'pro_id']],
            [['cit_id_usuaria'], 'exist', 'skipOnError' => true, 'targetClass' => Usuaria::className(), 'targetAttribute' => ['cit_id_usuaria' => 'reg_id']],
            [['cit_horario'], 'exist', 'skipOnError' => true, 'targetClass' => ConfiguracionHorario::className(), 'targetAttribute' => ['cit_horario' => 'conf_id']],
            [['cit_id_estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['cit_id_estatus' => 'est_id_estatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cit_id_cita' => 'Cita id',
            'cit_observaciones' => 'Observaciones',
            'cit_id_estatus' => 'Estatus',
            'cit_servicio' => 'Servicio',
            'cit_proceso' => 'Proceso',
            'cit_fecha_atencion' => 'Fecha de Atencion',
            'cit_municipio' => 'Municipio',
            'cit_fecha_registro' => 'Fecha de Registro',
            'cit_fecha_modificacion' => 'Fecha de Modificacion',
            'cit_id_usuaria' => 'Usuaria',
            'cit_id_profesionista' => 'Profesionista',
            'cit_horario' => 'Horario',
            'estatus' => 'Estatus',
            'servicio' => 'Servicio',
            'municipio' => 'Municipio',
            'usuaria' => 'Usuaria',
            'horario' => 'Horario',
            'profesionista' => 'Profesionista',
            'profesionistaEspecialidad' => 'Especialidad',
        ];
    }

    /**
     * Gets query for [[CitServicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitServicio()
    {
        return $this->hasOne(TipoServicio::className(), ['serv_id_tipo' => 'cit_servicio']);
    }

    /**
     * Gets query for [[CitIdProfesionista]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitIdProfesionista()
    {
        return $this->hasOne(Profesionista::className(), ['pro_id' => 'cit_id_profesionista']);
    }

    /**
     * Gets query for [[CitIdUsuaria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitIdUsuaria()
    {
        return $this->hasOne(Usuaria::className(), ['reg_id' => 'cit_id_usuaria']);
    }

    /**
     * Gets query for [[CitHorario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitHorario()
    {
        return $this->hasOne(ConfiguracionHorario::className(), ['conf_id' => 'cit_horario']);
    }

    public function getCitEstatus()
    {
        return $this->hasOne(Estatus::className(), ['est_id_estatus' => 'cit_id_estatus']);
    }

    public function getCitMunicipio()
    {
        return Municipios::findOne(['efe_key' => '27', 'catalog_key_mun' => $this->cit_municipio]);
    }

    public function getCitUsuaria()
    {
        return $this->hasOne(Usuaria::className(), ['reg_id' => 'cit_id_usuaria']);
    }

    /**
     * Funciones creadas para el modelo 
     **/

    public function getEstatus() 
    {
        return $this->citEstatus->est_nombre;
    }

    public function getServicio()
    {
        return $this->citServicio->serv_nombre;
    }

    public function getMunicipio()
    {
        return $this->citMunicipio->municipio;
    }

    public function getUsuaria()
    {
        return $this->citUsuaria->nombreCompleto;
    }

    public function getHorario()
    {
        return $this->citHorario->conf_descripcion;
    }

    public function getProfesionista()
    {
        return $this->citIdProfesionista->nombreCompleto;
    }

    public function getProfesionistaEspecialidad()
    {
        return $this->citIdProfesionista->especialidad;
    }

    public static function descartados()
    {
        $model = Citas::find()->where(['cit_id_estatus' => 1])->all();
        return sizeof($model);
    }

    public static function sinActualizar()
    {
        $model = Citas::find()->where(['cit_id_estatus' => 2])->all();
        return sizeof($model);
    }

    public static function enProceso()
    {
        $model = Citas::find()->where(['cit_id_estatus' => 3])->all();
        return sizeof($model);
    }

    public static function concluidos()
    {
        $model = Citas::find()->where(['cit_id_estatus' => 4])->all();
        return sizeof($model);
    }
}
