<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider; 
use app\models\Citas;

/**
 * CitasSearch represents the model behind the search form of `app\models\Citas`.
 */
class CitasSearch extends Citas
{
    /**
     * {@inheritdoc}
     */

    public $usuaria;
    public $profesionista;

    public function rules()
    {
        return [
            [['cit_id_cita', 'cit_id_estatus', 'cit_servicio', 'cit_id_usuaria', 'cit_id_profesionista', 'cit_horario'], 'integer'],
            [['usuaria', 'profesionista','cit_observaciones', 'cit_proceso', 'cit_fecha_atencion', 'cit_municipio', 'cit_fecha_registro', 'cit_fecha_modificacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Citas::find()
            ->joinWith(['citIdUsuaria', 'citIdProfesionista']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => array_merge(
                $dataProvider->getSort()->attributes,
                [
                    'usuaria' => [
                        'asc'       => ['concat_ws(" ", reg_nombre_usuaria, reg_apellidop, reg_apellidom)' => SORT_ASC],
                        'desc'      => ['concat_ws(" ", reg_nombre_usuaria, reg_apellidop, reg_apellidom)' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                    'profesionista' => [
                        'asc'       => ['concat_ws(" ", pro_nombre, pro_apellidop, pro_apellidom)' => SORT_ASC],
                        'desc'      => ['concat_ws(" ", pro_nombre, pro_apellidop, pro_apellidom)' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                ]
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cit_id_cita' => $this->cit_id_cita,
            'cit_id_estatus' => $this->cit_id_estatus,
            'cit_servicio' => $this->cit_servicio,
            'cit_fecha_atencion' => $this->cit_fecha_atencion,
            'cit_fecha_registro' => $this->cit_fecha_registro,
            'cit_fecha_modificacion' => $this->cit_fecha_modificacion,
            'cit_id_usuaria' => $this->cit_id_usuaria,
            'cit_id_profesionista' => $this->cit_id_profesionista,
            'cit_horario' => $this->cit_horario,
        ]);

        $query->andFilterWhere(['like', 'cit_observaciones', $this->cit_observaciones])
            ->andFilterWhere(['like', 'cit_proceso', $this->cit_proceso])
            ->andFilterWhere(['like', 'cit_municipio', $this->cit_municipio])
            ->andFilterWhere(['like', 'concat_ws(" ", reg_nombre_usuaria, reg_apellidop, reg_apellidom)', $this->usuaria])
            ->andFilterWhere(['like', 'concat_ws(" ", pro_nombre, pro_apellidop, pro_apellidom)', $this->profesionista]);

        return $dataProvider;
    }

    public function searchProfecionista($params, $date, $profesionistaId = null)
    {
        $query = Citas::find()->where(['cit_fecha_atencion' => $date]);

        if($profesionistaId != null){
            $query->andWhere(['cit_id_profesionista'=> $profesionistaId]);
        }else{
            $query->andWhere(['cit_id_profesionista'=> 0]);
        }

        $query->orderBy(['cit_horario'=> SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cit_id_cita' => $this->cit_id_cita,
            'cit_id_estatus' => $this->cit_id_estatus,
            'cit_servicio' => $this->cit_servicio,
            'cit_fecha_atencion' => $this->cit_fecha_atencion,
            'cit_fecha_registro' => $this->cit_fecha_registro,
            'cit_fecha_modificacion' => $this->cit_fecha_modificacion,
            'cit_id_usuaria' => $this->cit_id_usuaria,
            'cit_id_profesionista' => $this->cit_id_profesionista,
            'cit_horario' => $this->cit_horario,
        ]);

        $query->andFilterWhere(['like', 'cit_observaciones', $this->cit_observaciones])
            ->andFilterWhere(['like', 'cit_proceso', $this->cit_proceso])
            ->andFilterWhere(['like', 'cit_municipio', $this->cit_municipio]);

        return $dataProvider;
    }
}
