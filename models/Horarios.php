<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%horarios}}".
 *
 * @property int $hor_id
 * @property string $hor_fecha_atencion
 * @property int $hor_id_profesionista
 * @property int $hor_horario
 *
 * @property Profesionista $horIdProfesionista
 * @property ConfiguracionHorario $horHorario
 */
class Horarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%horarios}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hor_fecha_atencion', 'hor_id_profesionista', 'hor_horario'], 'required'],
            [['hor_fecha_atencion'], 'safe'],
            [['hor_id_profesionista', 'hor_horario'], 'integer'],
            [['hor_id_profesionista'], 'exist', 'skipOnError' => true, 'targetClass' => Profesionista::className(), 'targetAttribute' => ['hor_id_profesionista' => 'pro_id']],
            [['hor_horario'], 'exist', 'skipOnError' => true, 'targetClass' => ConfiguracionHorario::className(), 'targetAttribute' => ['hor_horario' => 'conf_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hor_id' => 'Hor ID',
            'hor_fecha_atencion' => 'Hor Fecha Atencion',
            'hor_id_profesionista' => 'Hor Id Profesionista',
            'hor_horario' => 'Hor Horario',
        ];
    }

    /**
     * Gets query for [[HorIdProfesionista]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHorIdProfesionista()
    {
        return $this->hasOne(Profesionista::className(), ['pro_id' => 'hor_id_profesionista']);
    }

    /**
     * Gets query for [[HorHorario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHorHorario()
    {
        return $this->hasOne(ConfiguracionHorario::className(), ['conf_id' => 'hor_horario']);
    }
}
