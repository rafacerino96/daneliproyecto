<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuaria;

/**
 * UsuariaSearch represents the model behind the search form of `app\models\Usuaria`.
 */
class UsuariaSearch extends Usuaria
{
    /**
     * {@inheritdoc}
     */

    public $nombreCompleto;
    public $municipio;
    public $estatus;
    //searchMunicipios
    public $estadisticaMunicipioDescartados;
    public $estadisticaMunicipioSinActualizar;
    public $estadisticaMunicipioEnProceso;
    public $estadisticaMunicipioConcluidos;

    public function rules()
    {
        return [
            [['reg_id', 'reg_total_niños', 'reg_niñas', 'reg_id_estatus', 'reg_id_nivel_estudio', 'reg_id__estado_civil', 'reg_id_discapacidad', 'reg_id_enfermedades_cronicas', 'reg_id_tipo_violencia', 'reg_id_modalidad_violencia', 'reg_seguro', 'reg_servicio_social', 'reg_algun_ingreso', 'reg_obtener_oficio', 'reg_aprender_oficio', 'reg_cabeza_familia', 'reg_vivienda_propia', 'reg_bienes_propios'], 'integer'],
            [['reg_minicipio', 'nombreCompleto', 'reg_folio_banavim', 'reg_nombre_usuaria', 'reg_apellidop', 'reg_apellidom', 'reg_telefono', 'reg_fecha_nacimiento', 'reg_id_actividad_economica', 'reg_fecha_registro', 'reg_fecha_modificacion', 'municipio', 'estatus'], 'safe'],
            //searchMunicipios
            [['estadisticaMunicipioDescartados', 'estadisticaMunicipioSinActualizar', 'estadisticaMunicipioEnProceso', 'estadisticaMunicipioConcluidos'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuaria::find()
            ->where(['reg_estatus' => 1])
            ->joinWith(['regMinicipio', 'regIdEstatus']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => array_merge(
                $dataProvider->getSort()->attributes,
                [
                    'nombreCompleto' => [
                        'asc'       => ['reg_nombre_usuaria' => SORT_ASC],
                        'desc'      => ['reg_nombre_usuaria' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                    'municipio' => [
                        'asc'       => ['municipio' => SORT_ASC],
                        'desc'      => ['municipio' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                    'estatus' => [
                        'asc'       => ['est_nombre' => SORT_ASC],
                        'desc'      => ['est_nombre' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                ]
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reg_id' => $this->reg_id,
            'reg_fecha_nacimiento' => $this->reg_fecha_nacimiento,
            'reg_total_niños' => $this->reg_total_niños,
            'reg_niñas' => $this->reg_niñas,
            'reg_id_estatus' => $this->reg_id_estatus,
            'reg_id_nivel_estudio' => $this->reg_id_nivel_estudio,
            'reg_id__estado_civil' => $this->reg_id__estado_civil,
            'reg_id_discapacidad' => $this->reg_id_discapacidad,
            'reg_id_enfermedades_cronicas' => $this->reg_id_enfermedades_cronicas,
            'reg_id_tipo_violencia' => $this->reg_id_tipo_violencia,
            'reg_id_modalidad_violencia' => $this->reg_id_modalidad_violencia,
            'reg_seguro' => $this->reg_seguro,
            'reg_servicio_social' => $this->reg_servicio_social,
            'reg_algun_ingreso' => $this->reg_algun_ingreso,
            'reg_obtener_oficio' => $this->reg_obtener_oficio,
            'reg_aprender_oficio' => $this->reg_aprender_oficio,
            'reg_cabeza_familia' => $this->reg_cabeza_familia,
            'reg_vivienda_propia' => $this->reg_vivienda_propia,
            'reg_bienes_propios' => $this->reg_bienes_propios,
            'reg_fecha_registro' => $this->reg_fecha_registro,
            'reg_fecha_modificacion' => $this->reg_fecha_modificacion,
            'reg_minicipio' => $this->reg_minicipio,
            //'nombreCompleto' => $this->nombreCompleto,
        ]);

        $query->andFilterWhere(['like', 'reg_folio_banavim', $this->reg_folio_banavim])
            //->andFilterWhere(['like', 'reg_nombre_usuaria', $this->reg_nombre_usuaria])
            ->andFilterWhere(['like', 'reg_apellidop', $this->reg_apellidop])
            ->andFilterWhere(['like', 'reg_apellidom', $this->reg_apellidom])
            ->andFilterWhere(['like', 'reg_telefono', $this->reg_telefono])
            ->andFilterWhere(['like', 'reg_id_actividad_economica', $this->reg_id_actividad_economica])
            ->andFilterWhere(['like', "concat_ws(' ', reg_nombre_usuaria, reg_apellidop, reg_apellidom)", $this->nombreCompleto])
            ->andFilterWhere(['like', 'municipios.municipio', $this->municipio])
            ->andFilterWhere(['like', 'est_nombre', $this->estatus]);


        return $dataProvider;
    }

    public function searchMunicipio($params, $year, $mes)
    {
        $query = Usuaria::find()
            ->where(['reg_estatus' => 1, 'reg_entidad_federativa' => '27'])
            ->groupBy(['reg_minicipio'])
            ->joinWith(['regMinicipio', 'regIdEstatus']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => array_merge(
                $dataProvider->getSort()->attributes,
                [
                    'nombreCompleto' => [
                        'asc'       => ['reg_nombre_usuaria' => SORT_ASC],
                        'desc'      => ['reg_nombre_usuaria' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                    'municipio' => [
                        'asc'       => ['municipio' => SORT_ASC],
                        'desc'      => ['municipio' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                    'estatus' => [
                        'asc'       => ['est_nombre' => SORT_ASC],
                        'desc'      => ['est_nombre' => SORT_DESC],
                        'default'   => SORT_ASC,
                    ],
                ]
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reg_id' => $this->reg_id,
            'reg_fecha_nacimiento' => $this->reg_fecha_nacimiento,
            'reg_total_niños' => $this->reg_total_niños,
            'reg_niñas' => $this->reg_niñas,
            'reg_id_estatus' => $this->reg_id_estatus,
            'reg_id_nivel_estudio' => $this->reg_id_nivel_estudio,
            'reg_id__estado_civil' => $this->reg_id__estado_civil,
            'reg_id_discapacidad' => $this->reg_id_discapacidad,
            'reg_id_enfermedades_cronicas' => $this->reg_id_enfermedades_cronicas,
            'reg_id_tipo_violencia' => $this->reg_id_tipo_violencia,
            'reg_id_modalidad_violencia' => $this->reg_id_modalidad_violencia,
            'reg_seguro' => $this->reg_seguro,
            'reg_servicio_social' => $this->reg_servicio_social,
            'reg_algun_ingreso' => $this->reg_algun_ingreso,
            'reg_obtener_oficio' => $this->reg_obtener_oficio,
            'reg_aprender_oficio' => $this->reg_aprender_oficio,
            'reg_cabeza_familia' => $this->reg_cabeza_familia,
            'reg_vivienda_propia' => $this->reg_vivienda_propia,
            'reg_bienes_propios' => $this->reg_bienes_propios,
            'reg_fecha_registro' => $this->reg_fecha_registro,
            'reg_fecha_modificacion' => $this->reg_fecha_modificacion,
            'reg_minicipio' => $this->reg_minicipio,
            //'nombreCompleto' => $this->nombreCompleto,
        ]);

        $query->andFilterWhere(['like', 'reg_folio_banavim', $this->reg_folio_banavim])
            //->andFilterWhere(['like', 'reg_nombre_usuaria', $this->reg_nombre_usuaria])
            ->andFilterWhere(['like', 'reg_apellidop', $this->reg_apellidop])
            ->andFilterWhere(['like', 'reg_apellidom', $this->reg_apellidom])
            ->andFilterWhere(['like', 'reg_telefono', $this->reg_telefono])
            ->andFilterWhere(['like', 'reg_id_actividad_economica', $this->reg_id_actividad_economica])
            ->andFilterWhere(['like', "concat_ws(' ', reg_nombre_usuaria, reg_apellidop, reg_apellidom)", $this->nombreCompleto])
            ->andFilterWhere(['like', 'municipios.municipio', $this->municipio])
            ->andFilterWhere(['like', 'est_nombre', $this->estatus]);

        if($year != 0){
            $query->andFilterWhere(['YEAR(reg_fecha_modificacion)' => $year]);
            if($mes != 0 && $mes != "" && $mes != null && $mes != 13){
                $query->andFilterWhere(['MONTH(reg_fecha_modificacion)' => $mes]);
            }
        }


        return $dataProvider;
    }
}
