<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%estado_civil}}".
 *
 * @property int $civ_id_estado_civil
 * @property string|null $civ_nombre
 *
 * @property Usuaria[] $usuarias
 */
class EstadoCivil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%estado_civil}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['civ_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'civ_id_estado_civil' => 'Id Estado Civil',
            'civ_nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Usuarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarias()
    {
        return $this->hasMany(Usuaria::className(), ['reg_id__estado_civil' => 'civ_id_estado_civil']);
    }
}
