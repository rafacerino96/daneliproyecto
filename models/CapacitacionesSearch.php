<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Capacitaciones;

/**
 * CapacitacionesSearch represents the model behind the search form of `app\models\Capacitaciones`.
 */
class CapacitacionesSearch extends Capacitaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cap_id', 'cap_total', 'cap_hombres', 'cap_mujeres', 'cap_tip_id'], 'integer'],
            [['cap_nombre', 'cap_feha', 'cap_hora_inicio', 'cap_hora_termino', 'cap_dependencia'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Capacitaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cap_id' => $this->cap_id,
            'cap_feha' => $this->cap_feha,
            'cap_hora_inicio' => $this->cap_hora_inicio,
            'cap_hora_termino' => $this->cap_hora_termino,
            'cap_total' => $this->cap_total,
            'cap_hombres' => $this->cap_hombres,
            'cap_mujeres' => $this->cap_mujeres,
            'cap_tip_id' => $this->cap_tip_id,
        ]);

        $query->andFilterWhere(['like', 'cap_nombre', $this->cap_nombre])
            ->andFilterWhere(['like', 'cap_dependencia', $this->cap_dependencia]);

        return $dataProvider;
    }
}
