<?php

namespace app\models; 

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%usuaria}}".
 *
 * @property int $reg_id
 * @property string|null $reg_folio_banavim
 * @property string $reg_nombre_usuaria
 * @property string|null $reg_apellidop
 * @property string|null $reg_apellidom
 * @property string|null $reg_telefono
 * @property string|null $reg_fecha_nacimiento
 * @property int|null $reg_total_niños
 * @property int|null $reg_niñas
 * @property int $reg_id_estatus
 * @property int $reg_id_nivel_estudio
 * @property int $reg_id__estado_civil
 * @property int $reg_id_discapacidad
 * @property string $reg_id_actividad_economica
 * @property int $reg_id_enfermedades_cronicas
 * @property int $reg_id_tipo_violencia
 * @property int $reg_id_modalidad_violencia
 * @property int|null $reg_seguro No/Si
 * @property int|null $reg_servicio_social
 * @property int|null $reg_algun_ingreso
 * @property int|null $reg_obtener_oficio
 * @property int|null $reg_aprender_oficio
 * @property int|null $reg_cabeza_familia
 * @property int|null $reg_vivienda_propia
 * @property int|null $reg_bienes_propios
 * @property string|null $reg_fecha_registro
 * @property string|null $reg_fecha_modificacion
 *
 * @property Citas[] $citas
 * @property Estatus $regIdEstatus
 * @property EstadoCivil $regIdEstadoCivil
 * @property EnfermedadesCronicas $regIdEnfermedadesCronicas
 * @property Discapacidad $regIdDiscapacidad
 * @property NivelEstudio $regIdNivelEstudio
 * @property ModalidadViolencia $regIdModalidadViolencia
 * @property TiposViolencia $regIdTipoViolencia
 */
class Usuaria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%usuaria}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reg_nombre_usuaria', 'reg_id_estatus', 'reg_id_nivel_estudio', 'reg_id__estado_civil', 'reg_id_discapacidad', 'reg_id_actividad_economica', 'reg_id_enfermedades_cronicas', 'reg_id_tipo_violencia', 'reg_id_modalidad_violencia', 'reg_nacionalidad', 'reg_entidad_federativa'], 'required'],
            [['reg_fecha_nacimiento', 'reg_fecha_registro', 'reg_fecha_modificacion', 'reg_id_modalidad_violencia', 'reg_id_tipo_violencia'], 'safe'],
        [['reg_total_niños', 'reg_niñas', 'reg_id_estatus', 'reg_nacionalidad', 'reg_id_nivel_estudio', 'reg_id__estado_civil', 'reg_id_discapacidad', 'reg_id_enfermedades_cronicas', 'reg_seguro', 'reg_servicio_social', 'reg_algun_ingreso', 'reg_obtener_oficio', 'reg_aprender_oficio', 'reg_cabeza_familia', 'reg_vivienda_propia', 'reg_bienes_propios', 'reg_estatus'], 'integer'],
            [['reg_folio_banavim'], 'string', 'max' => 15],
            [['reg_nombre_usuaria', 'reg_id_actividad_economica', 'reg_localidad', 'reg_entidad_federativa', 'reg_minicipio'], 'string', 'max' => 50],
            [['reg_apellidop', 'reg_apellidom'], 'string', 'max' => 100],
            [['reg_telefono'], 'string', 'max' => 10],
            [['reg_id_estatus'], 'exist', 'skipOnError' => true, 'targetClass' => Estatus::className(), 'targetAttribute' => ['reg_id_estatus' => 'est_id_estatus']],
            [['reg_id__estado_civil'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoCivil::className(), 'targetAttribute' => ['reg_id__estado_civil' => 'civ_id_estado_civil']],
            [['reg_id_enfermedades_cronicas'], 'exist', 'skipOnError' => true, 'targetClass' => EnfermedadesCronicas::className(), 'targetAttribute' => ['reg_id_enfermedades_cronicas' => 'enf_id_enfermedades_cronicas']],
            [['reg_id_discapacidad'], 'exist', 'skipOnError' => true, 'targetClass' => Discapacidad::className(), 'targetAttribute' => ['reg_id_discapacidad' => 'dis_id_discapacidad']],
            [['reg_id_nivel_estudio'], 'exist', 'skipOnError' => true, 'targetClass' => NivelEstudio::className(), 'targetAttribute' => ['reg_id_nivel_estudio' => 'niv_id_nivel_estudio']],
            [['reg_entidad_federativa'], 'exist', 'skipOnError' => true, 'targetClass' => EntidadesFederativas::className(), 'targetAttribute' => ['reg_entidad_federativa' => 'catalog_key']],
            [['reg_minicipio'], 'exist', 'skipOnError' => true, 'targetClass' => Municipios::className(), 'targetAttribute' => ['reg_minicipio' => 'catalog_key_mun']],
            [['reg_localidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidades::className(), 'targetAttribute' => ['reg_localidad' => 'cve_loc']],
            [['reg_nacionalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Nacionalidad::className(), 'targetAttribute' => ['reg_nacionalidad' => 'codigo_pais']],

            //[['reg_id_modalidad_violencia'], 'exist', 'skipOnError' => true, 'targetClass' => ModalidadViolencia::className(), 'targetAttribute' => ['reg_id_modalidad_violencia' => 'mod_id_violencia']],
            //[['reg_id_tipo_violencia'], 'exist', 'skipOnError' => true, 'targetClass' => TiposViolencia::className(), 'targetAttribute' => ['reg_id_tipo_violencia' => 'vio_id_tipo']],
            //Modificaciones extras
            [['reg_telefono'], 'number'],
            [['reg_telefono'], 'string', 'min' => 10],
            [['reg_id_estatus'], 'default', 'value'=> 1],
            //[['reg_estatus'], 'default', 'value'=> 1],
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reg_id' => 'Reg ID',
            'reg_folio_banavim' => 'Folio Banavim',
            'reg_nombre_usuaria' => 'Nombre',
            'reg_apellidop' => 'Apellido paterno',
            'reg_apellidom' => 'Apellido materno',
            'reg_telefono' => 'Num. Telefono',
            'reg_fecha_nacimiento' => 'Fecha de nacimiento',
            'reg_total_niños' => 'Niños',
            'reg_niñas' => 'Niñas',
            'reg_id_estatus' => 'Estatus',
            'reg_id_nivel_estudio' => 'Nivel de estudio',
            'reg_id__estado_civil' => 'Estado civil',
            'reg_id_discapacidad' => 'Discapacidad',
            'reg_id_actividad_economica' => 'Actividad economica',
            'reg_id_enfermedades_cronicas' => 'Enfermedades cronicas',
            'reg_id_tipo_violencia' => 'Tipo de violencia',
            'reg_id_modalidad_violencia' => 'Modalidad Violencia',
            'reg_seguro' => 'Seguro',
            'reg_servicio_social' => 'Servicio social',
            'reg_algun_ingreso' => 'Algun Ingreso',
            'reg_obtener_oficio' => 'Obtener Oficio',
            'reg_aprender_oficio' => 'Aprender Oficio',
            'reg_cabeza_familia' => 'Cabeza de familia',
            'reg_vivienda_propia' => 'Vivienda propia',
            'reg_bienes_propios' => 'Bienes propios',
            'reg_fecha_registro' => 'Fecha de registro',
            'reg_fecha_modificacion' => 'Fecha de modificación',
            'reg_nacionalidad' => 'Nacionalidad',
            'reg_entidad_federativa' => 'Entidad federativa',
            'reg_minicipio' => 'Municipio',
            'reg_localidad' => 'Localidad',
            //'reg_estatus' => 'Estatus del registro',
            //Agregados
            'servicio' => 'Servicio social',
            'ingreso' => 'Algun Ingreso',
            'obtenerOficio' => 'Obtener Oficio',
            'aprenderOficio' => 'Aprender Oficio',
            'cabezaFamilia' => 'Cabeza de familia',
            'vivienda' => 'Vivienda propia',
            'bienes' => 'Bienes Propios',
            'nombreCompleto' => 'Nombre completo',
            'totalHijos' => 'Hijos',
            'estatus' => 'Estatus',
            'nacionalidad' => 'Nacionalidad',
            'entidadFederativa' => 'Entidad federativa',
            'municipio' => 'Municipio',
            'localidad' => 'Localidad',
            'estadisticaMunicipioDescartados' => 'Descartados',
            'estadisticaMunicipioSinActualizar' => 'Sin actualizar',
            'estadisticaMunicipioEnProceso' => 'En proceso',
            'estadisticaMunicipioConcluidos' => 'Concluidos',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['cit_id_usuaria' => 'reg_id']);
    }

    /**
     * Gets query for [[RegIdEstatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdEstatus()
    {
        return $this->hasOne(Estatus::className(), ['est_id_estatus' => 'reg_id_estatus']);
    }

    /**
     * Gets query for [[RegIdEstadoCivil]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdEstadoCivil()
    {
        return $this->hasOne(EstadoCivil::className(), ['civ_id_estado_civil' => 'reg_id__estado_civil']);
    }

    /**
     * Gets query for [[RegIdEnfermedadesCronicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdEnfermedadesCronicas()
    {
        return $this->hasOne(EnfermedadesCronicas::className(), ['enf_id_enfermedades_cronicas' => 'reg_id_enfermedades_cronicas']);
    }

    /**
     * Gets query for [[RegIdDiscapacidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdDiscapacidad()
    {
        return $this->hasOne(Discapacidad::className(), ['dis_id_discapacidad' => 'reg_id_discapacidad']);
    }

    /**
     * Gets query for [[RegIdNivelEstudio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdNivelEstudio()
    {
        return $this->hasOne(NivelEstudio::className(), ['niv_id_nivel_estudio' => 'reg_id_nivel_estudio']);
    }

    /**
     * Gets query for [[RegIdModalidadViolencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdModalidadViolencia()
    {
        return $this->hasOne(ModalidadViolencia::className(), ['mod_id_violencia' => 'reg_id_modalidad_violencia']);
    }

    /**
     * Gets query for [[RegIdTipoViolencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegIdTipoViolencia()
    {
        return $this->hasOne(TiposViolencia::className(), ['vio_id_tipo' => 'reg_id_tipo_violencia']);
    }

    public function getRegNacionalidad()
    {
        return $this->hasOne(Nacionalidad::className(), ['codigo_pais' => 'reg_nacionalidad']);
    }

    public function getRegEntidadFederativa()
    {
        return $this->hasOne(EntidadesFederativas::className(), ['catalog_key' => 'reg_entidad_federativa']);
    }

    public function getRegMinicipio()
    {
        return $this->hasOne(Municipios::className(), ['catalog_key_mun' => 'reg_minicipio', 'efe_key' => 'reg_entidad_federativa']);
    }

    public function getRegLocalidad()
    {
        return $this->hasOne(Localidades::className(), ['cve_loc' => 'reg_localidad', 'cve_mun' => 'reg_minicipio', 'cve_ent' => 'reg_entidad_federativa']);
    }

    /**
     * Funciones utilizadas en las vistas
     */

    public function getEstadoCivil()
    {
        return $this->regIdEstadoCivil->civ_nombre;
    }

    public function getNivelEstudio()
    {
        return $this->regIdNivelEstudio->niv_nombre;
    }
    public function getNacionalidad()
    {
        return $this->regNacionalidad->pais;
    }

    public function getEntidadFederativa()
    {
        return $this->regEntidadFederativa->entidad_federativa;
    }

    public function getMunicipio()
    {
        return $this->regMinicipio->municipio;
    }

    public function getLocalidad()
    {
        return $this->regLocalidad->nom_loc;
    }

    public function getDiscapacidad()
    {
        return $this->regIdDiscapacidad->dis_nombre;
    }

    public function getEnfermedadCronica()
    {
        return $this->regIdEnfermedadesCronicas->enf_nombre;
    }

    public function getSeguro()
    {
        return $this->sino($this->reg_seguro);
    }

    public function getServicio()
    {
        return $this->sino($this->reg_servicio_social);
    }

    public function getIngreso()
    {
        return $this->sino($this->reg_algun_ingreso);
    }

    public function getObtenerOficio()
    {
        return $this->sino($this->reg_obtener_oficio);
    }

    public function getAprenderOficio()
    {
        return $this->sino($this->reg_aprender_oficio);
    }

    public function getCabezaFamilia()
    {
        return $this->sino($this->reg_cabeza_familia);
    }

    public function getVivienda()
    {
        return $this->sino($this->reg_vivienda_propia);
    }

    public function getBienes()
    {
        return $this->sino($this->reg_bienes_propios);
    }

    public function getEstatus()
    {
        return $this->regIdEstatus->est_nombre;
    }

    public function getTipoViolencia()
    {
        $arrayTiposVilencia = ArrayHelper::map(TiposViolencia::find()->all(),'vio_id_tipo','vio_nombre');
        $array = json_decode($this->reg_id_tipo_violencia);
        $string = "";
        foreach($array as $a){
            if($a == end($array)){
                $string .= $arrayTiposVilencia[$a] . " ";
            }else{
                $string .= $arrayTiposVilencia[$a] . ", ";
            }
        }
        return $string;
    }

    public function getModalidadViolencia()
    {
        $modalidad_violencia  = ArrayHelper::map(ModalidadViolencia::find()->all(),'mod_id_violencia','mod_nombre');
        $array = json_decode($this->reg_id_modalidad_violencia);
        $string = "";
        foreach($array as $a){
            if($a == end($array)){
                $string .= $modalidad_violencia[$a] . " ";
            }else{
                $string .= $modalidad_violencia[$a] . ", ";
            }
        }
        return $string;
    }

    public function getNombreCompleto() 
    {
        
        return $this->reg_nombre_usuaria . " " . $this->reg_apellidop ." ". $this->reg_apellidom;
    }

    public function getFolioNombreCompleto()
    {
        
        return $this->reg_id . " - " . $this->reg_nombre_usuaria . " " . $this->reg_apellidop ." ". $this->reg_apellidom;
    }

    public function getTotalHijos()
    {
        
        return $this->reg_total_niños + $this->reg_niñas;
    }
    
    public function sino($sino)
    {
        if($sino == 1){
            return "Si";
        }

        if($sino == 0){
            return "No";
        }

        return "Error al obtener respuesta";
    }

    public static function descartados()
    {
        $model = Usuaria::find()->where(['reg_id_estatus' => 1, 'reg_estatus' => 1])->all();
        return sizeof($model);
    }

    public static function sinActualizar()
    {
        $model = Usuaria::find()->where(['reg_id_estatus' => 2, 'reg_estatus' => 1])->all();
        return sizeof($model);
    }

    public static function enProceso()
    {
        $model = Usuaria::find()->where(['reg_id_estatus' => 3, 'reg_estatus' => 1])->all();
        return sizeof($model);
    }

    public static function concluidos()
    {
        $model = Usuaria::find()->where(['reg_id_estatus' => 4, 'reg_estatus' => 1])->all();
        return sizeof($model);
    }

    public static function years()
    {
        $years = (new \yii\db\Query())
            ->select(['YEAR(reg_fecha_modificacion) as year'])
            ->from('usuaria')
            ->where(['reg_estatus' => 1])
            ->groupBy(['year'])
            ->all();
        $years = ArrayHelper::map($years, 'year', 'year');
        $years = $years + ['0' => 'Todos'];
        return $years;
    }

    public static function meses()
    {
        $meses = ['13' => 'Todos', '1' =>'Enero', '2' => 'Febrero', '3' => 'Marzo', '4' => 'Abril', '5' => 'Mayo', '6' => 'Junio', '7' => 'Julio', '8' => 'Agosto', '9' => 'September', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
        return $meses;
    }

    public function getEstadisticaMunicipioDescartados()
    {
        return sizeof(Usuaria::find()->where(['reg_estatus' => 1, 'reg_id_estatus' => 1, 'reg_minicipio' => $this->reg_minicipio])->all());
    }

    public function getEstadisticaMunicipioSinActualizar()
    {
        return sizeof(Usuaria::find()->where(['reg_estatus' => 1, 'reg_id_estatus' => 2, 'reg_minicipio' => $this->reg_minicipio])->all());
    }

    public function getEstadisticaMunicipioEnProceso()
    {
        return sizeof(Usuaria::find()->where(['reg_estatus' => 1, 'reg_id_estatus' => 3, 'reg_minicipio' => $this->reg_minicipio])->all());
    }

    public function getEstadisticaMunicipioConcluidos()
    {
        return sizeof(Usuaria::find()->where(['reg_estatus' => 1, 'reg_id_estatus' => 4, 'reg_minicipio' => $this->reg_minicipio])->all());
    }
}
