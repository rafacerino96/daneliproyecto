<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dias;

/**
 * DiasSearch represents the model behind the search form of `app\models\Dias`.
 */
class DiasSearch extends Dias
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dia_id', 'dia_lunes', 'dia_martes', 'dia_miercoles', 'dia_jueves', 'dia_viernes', 'dia_sabado', 'dia_domingo'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dia_id' => $this->dia_id,
            'dia_lunes' => $this->dia_lunes,
            'dia_martes' => $this->dia_martes,
            'dia_miercoles' => $this->dia_miercoles,
            'dia_jueves' => $this->dia_jueves,
            'dia_viernes' => $this->dia_viernes,
            'dia_sabado' => $this->dia_sabado,
            'dia_domingo' => $this->dia_domingo,
        ]);

        return $dataProvider;
    }
}
