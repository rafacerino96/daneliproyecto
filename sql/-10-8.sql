/*
Navicat MySQL Data Transfer

Source Server         : Nuevo
Source Server Version : 50562
Source Host           : localhost:3306
Source Database       : pre

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2021-08-10 23:37:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('Admin', '1', '1621395237');

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `group_code` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  KEY `rule_name` (`rule_name`) USING BTREE,
  KEY `idx-auth_item-type` (`type`) USING BTREE,
  KEY `fk_auth_item_group_code` (`group_code`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `auth_item_ibfk_2` FOREIGN KEY (`group_code`) REFERENCES `auth_item_group` (`code`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//controller', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//crud', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//extension', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//form', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//model', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('//module', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/asset/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/asset/compress', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/asset/template', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/cache/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/cache/flush', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/cache/flush-all', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/cache/flush-schema', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/cache/index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/fixture/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/fixture/load', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/fixture/unload', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/default/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/default/action', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/default/diff', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/default/index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/default/preview', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/gii/default/view', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/hello/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/hello/index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/help/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/help/index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/message/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/message/config', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/message/config-template', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/message/extract', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/create', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/down', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/history', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/mark', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/new', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/redo', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/to', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/migrate/up', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/serve/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/serve/index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/site/*', '3', null, null, null, '1463510028', '1463510028', null);
INSERT INTO `auth_item` VALUES ('/site/captcha', '3', null, null, null, '1463510028', '1463510028', null);
INSERT INTO `auth_item` VALUES ('/site/error', '3', null, null, null, '1463510028', '1463510028', null);
INSERT INTO `auth_item` VALUES ('/site/index', '3', null, null, null, '1463510028', '1463510028', null);
INSERT INTO `auth_item` VALUES ('/user-management/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/*', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/bulk-activate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/bulk-deactivate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/bulk-delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/create', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/grid-page-size', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/grid-sort', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/index', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/toggle-attribute', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/update', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth-item-group/view', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/*', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/captcha', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/change-own-password', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/confirm-email', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/confirm-email-receive', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/confirm-registration-email', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/login', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/logout', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/password-recovery', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/password-recovery-receive', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/auth/registration', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/*', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/bulk-activate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/bulk-deactivate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/bulk-delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/create', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/grid-page-size', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/grid-sort', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/index', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/refresh-routes', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/set-child-permissions', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/set-child-routes', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/toggle-attribute', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/update', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/permission/view', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/*', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/bulk-activate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/bulk-deactivate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/bulk-delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/create', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/grid-page-size', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/grid-sort', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/index', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/set-child-permissions', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/set-child-roles', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/toggle-attribute', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/update', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/role/view', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-permission/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-permission/set', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-permission/set-roles', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/bulk-activate', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/bulk-deactivate', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/bulk-delete', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/create', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/delete', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/grid-page-size', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/grid-sort', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/index', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/toggle-attribute', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/update', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user-visit-log/view', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/*', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/bulk-activate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/bulk-deactivate', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/bulk-delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/change-password', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/create', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/delete', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/grid-page-size', '3', null, null, null, '1463113428', '1463113428', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/grid-sort', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/index', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/toggle-attribute', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/update', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('/user-management/user/view', '3', null, null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('Admin', '1', 'Admin', null, null, '1463113429', '1463113429', null);
INSERT INTO `auth_item` VALUES ('assignRolesToUsers', '2', 'Assign roles to users', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('bindUserToIp', '2', 'Bind user to IP', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('changeOwnPassword', '2', 'Change own password', null, null, '1463113429', '1463113429', 'userCommonPermissions');
INSERT INTO `auth_item` VALUES ('changeUserPassword', '2', 'Change user password', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('commonPermission', '2', 'Common permission', null, null, '1463113424', '1463113424', null);
INSERT INTO `auth_item` VALUES ('createUsers', '2', 'Create users', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('deleteUsers', '2', 'Delete users', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('editUserEmail', '2', 'Edit user email', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('editUsers', '2', 'Edit users', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('viewRegistrationIp', '2', 'View registration IP', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('viewUserEmail', '2', 'View user email', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('viewUserRoles', '2', 'View user roles', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('viewUsers', '2', 'View users', null, null, '1463113429', '1463113429', 'userManagement');
INSERT INTO `auth_item` VALUES ('viewVisitLog', '2', 'View visit log', null, null, '1463113429', '1463113429', 'userManagement');

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`) USING BTREE,
  KEY `child` (`child`) USING BTREE,
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('changeOwnPassword', '/user-management/auth/change-own-password');
INSERT INTO `auth_item_child` VALUES ('assignRolesToUsers', '/user-management/user-permission/set');
INSERT INTO `auth_item_child` VALUES ('assignRolesToUsers', '/user-management/user-permission/set-roles');
INSERT INTO `auth_item_child` VALUES ('viewVisitLog', '/user-management/user-visit-log/grid-page-size');
INSERT INTO `auth_item_child` VALUES ('viewVisitLog', '/user-management/user-visit-log/index');
INSERT INTO `auth_item_child` VALUES ('viewVisitLog', '/user-management/user-visit-log/view');
INSERT INTO `auth_item_child` VALUES ('editUsers', '/user-management/user/bulk-activate');
INSERT INTO `auth_item_child` VALUES ('editUsers', '/user-management/user/bulk-deactivate');
INSERT INTO `auth_item_child` VALUES ('deleteUsers', '/user-management/user/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('changeUserPassword', '/user-management/user/change-password');
INSERT INTO `auth_item_child` VALUES ('createUsers', '/user-management/user/create');
INSERT INTO `auth_item_child` VALUES ('deleteUsers', '/user-management/user/delete');
INSERT INTO `auth_item_child` VALUES ('viewUsers', '/user-management/user/grid-page-size');
INSERT INTO `auth_item_child` VALUES ('viewUsers', '/user-management/user/index');
INSERT INTO `auth_item_child` VALUES ('editUsers', '/user-management/user/update');
INSERT INTO `auth_item_child` VALUES ('viewUsers', '/user-management/user/view');
INSERT INTO `auth_item_child` VALUES ('Admin', 'assignRolesToUsers');
INSERT INTO `auth_item_child` VALUES ('Admin', 'changeOwnPassword');
INSERT INTO `auth_item_child` VALUES ('Admin', 'changeUserPassword');
INSERT INTO `auth_item_child` VALUES ('Admin', 'createUsers');
INSERT INTO `auth_item_child` VALUES ('Admin', 'deleteUsers');
INSERT INTO `auth_item_child` VALUES ('Admin', 'editUsers');
INSERT INTO `auth_item_child` VALUES ('editUserEmail', 'viewUserEmail');
INSERT INTO `auth_item_child` VALUES ('assignRolesToUsers', 'viewUserRoles');
INSERT INTO `auth_item_child` VALUES ('Admin', 'viewUsers');
INSERT INTO `auth_item_child` VALUES ('assignRolesToUsers', 'viewUsers');
INSERT INTO `auth_item_child` VALUES ('changeUserPassword', 'viewUsers');
INSERT INTO `auth_item_child` VALUES ('createUsers', 'viewUsers');
INSERT INTO `auth_item_child` VALUES ('deleteUsers', 'viewUsers');
INSERT INTO `auth_item_child` VALUES ('editUsers', 'viewUsers');

-- ----------------------------
-- Table structure for auth_item_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_group`;
CREATE TABLE `auth_item_group` (
  `code` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_item_group
-- ----------------------------
INSERT INTO `auth_item_group` VALUES ('userCommonPermissions', 'User common permission', '1463113429', '1463113429');
INSERT INTO `auth_item_group` VALUES ('userManagement', 'User management', '1463113429', '1463113429');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for capacitaciones
-- ----------------------------
DROP TABLE IF EXISTS `capacitaciones`;
CREATE TABLE `capacitaciones` (
  `cap_id` int(11) NOT NULL AUTO_INCREMENT,
  `cap_nombre` varchar(200) DEFAULT NULL,
  `cap_feha` date DEFAULT NULL,
  `cap_hora_inicio` time DEFAULT NULL,
  `cap_hora_termino` time DEFAULT NULL,
  `cap_dependencia` varchar(200) DEFAULT NULL,
  `cap_total` int(11) DEFAULT NULL,
  `cap_hombres` int(11) DEFAULT NULL,
  `cap_mujeres` int(11) DEFAULT NULL,
  `cap_tip_id` int(11) NOT NULL,
  PRIMARY KEY (`cap_id`,`cap_tip_id`),
  KEY `cap_tip_id` (`cap_tip_id`),
  CONSTRAINT `capacitaciones_ibfk_1` FOREIGN KEY (`cap_tip_id`) REFERENCES `tipo_capacitacion` (`tip_id_capacitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of capacitaciones
-- ----------------------------

-- ----------------------------
-- Table structure for citas
-- ----------------------------
DROP TABLE IF EXISTS `citas`;
CREATE TABLE `citas` (
  `cit_id_cita` int(11) NOT NULL AUTO_INCREMENT,
  `cit_observaciones` text,
  `cit_id_estatus` int(11) NOT NULL,
  `cit_servicio` int(11) NOT NULL,
  `cit_proceso` varchar(50) NOT NULL,
  `cit_fecha_atencion` datetime DEFAULT NULL,
  `cit_municipio` varchar(25) DEFAULT NULL,
  `cit_fecha_registro` datetime DEFAULT NULL,
  `cit_fecha_modificacion` datetime DEFAULT NULL,
  `cit_id_usuaria` int(11) NOT NULL,
  `cit_id_profesionista` int(11) NOT NULL,
  PRIMARY KEY (`cit_id_cita`,`cit_id_estatus`,`cit_servicio`,`cit_id_usuaria`,`cit_id_profesionista`),
  KEY `cit_servicio` (`cit_servicio`),
  KEY `cit_proceso` (`cit_proceso`),
  KEY `cit_id_usuaria` (`cit_id_usuaria`),
  KEY `cit_id_profesionista` (`cit_id_profesionista`),
  CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`cit_servicio`) REFERENCES `tipo_servicio` (`serv_id_tipo`),
  CONSTRAINT `citas_ibfk_3` FOREIGN KEY (`cit_id_profesionista`) REFERENCES `profesionista` (`pro_id`),
  CONSTRAINT `citas_ibfk_4` FOREIGN KEY (`cit_id_usuaria`) REFERENCES `usuaria` (`reg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of citas
-- ----------------------------

-- ----------------------------
-- Table structure for discapacidad
-- ----------------------------
DROP TABLE IF EXISTS `discapacidad`;
CREATE TABLE `discapacidad` (
  `dis_id_discapacidad` int(11) NOT NULL AUTO_INCREMENT,
  `dis_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dis_id_discapacidad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of discapacidad
-- ----------------------------
INSERT INTO `discapacidad` VALUES ('1', 'Motriz');
INSERT INTO `discapacidad` VALUES ('2', 'Auditiva');
INSERT INTO `discapacidad` VALUES ('3', 'Visual');
INSERT INTO `discapacidad` VALUES ('4', 'Intelectual');

-- ----------------------------
-- Table structure for enfermedades_cronicas
-- ----------------------------
DROP TABLE IF EXISTS `enfermedades_cronicas`;
CREATE TABLE `enfermedades_cronicas` (
  `enf_id_enfermedades_cronicas` int(11) NOT NULL AUTO_INCREMENT,
  `enf_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`enf_id_enfermedades_cronicas`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enfermedades_cronicas
-- ----------------------------
INSERT INTO `enfermedades_cronicas` VALUES ('1', 'Alzheimer');
INSERT INTO `enfermedades_cronicas` VALUES ('2', 'Artritis');
INSERT INTO `enfermedades_cronicas` VALUES ('3', 'Asma');
INSERT INTO `enfermedades_cronicas` VALUES ('4', 'Cáncer');
INSERT INTO `enfermedades_cronicas` VALUES ('5', 'EPOC');
INSERT INTO `enfermedades_cronicas` VALUES ('6', 'Enfermedad de Crohn');
INSERT INTO `enfermedades_cronicas` VALUES ('7', 'Fibrosis quística');

-- ----------------------------
-- Table structure for especialidad
-- ----------------------------
DROP TABLE IF EXISTS `especialidad`;
CREATE TABLE `especialidad` (
  `esp_id` int(11) NOT NULL AUTO_INCREMENT,
  `esp_nombre` varchar(100) DEFAULT NULL,
  `esp_descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`esp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of especialidad
-- ----------------------------
INSERT INTO `especialidad` VALUES ('1', 'Psicologa', null);
INSERT INTO `especialidad` VALUES ('2', 'Abogada', null);

-- ----------------------------
-- Table structure for estado_civil
-- ----------------------------
DROP TABLE IF EXISTS `estado_civil`;
CREATE TABLE `estado_civil` (
  `civ_id_estado_civil` int(11) NOT NULL AUTO_INCREMENT,
  `civ_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`civ_id_estado_civil`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estado_civil
-- ----------------------------
INSERT INTO `estado_civil` VALUES ('1', 'Soltera');
INSERT INTO `estado_civil` VALUES ('2', 'Casada');
INSERT INTO `estado_civil` VALUES ('3', 'Divorciada');
INSERT INTO `estado_civil` VALUES ('4', 'Separación en proceso judicial');
INSERT INTO `estado_civil` VALUES ('5', 'Viuda');
INSERT INTO `estado_civil` VALUES ('6', 'Concubinato');

-- ----------------------------
-- Table structure for estatus
-- ----------------------------
DROP TABLE IF EXISTS `estatus`;
CREATE TABLE `estatus` (
  `est_id_estatus` int(11) NOT NULL AUTO_INCREMENT,
  `est_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`est_id_estatus`),
  KEY `est_id_estatus` (`est_id_estatus`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estatus
-- ----------------------------
INSERT INTO `estatus` VALUES ('1', 'Descartados');
INSERT INTO `estatus` VALUES ('2', 'Sin actualizar');
INSERT INTO `estatus` VALUES ('3', 'En proceso');
INSERT INTO `estatus` VALUES ('4', 'Concluidos');

-- ----------------------------
-- Table structure for horarios
-- ----------------------------
DROP TABLE IF EXISTS `horarios`;
CREATE TABLE `horarios` (
  `hor_id` int(11) NOT NULL AUTO_INCREMENT,
  `hor_fecha_atencion` date DEFAULT NULL,
  `hor_inicio_atencion` time DEFAULT NULL,
  `hor_fin_atencion` time DEFAULT NULL,
  `hor_id_profesionista` int(11) NOT NULL,
  PRIMARY KEY (`hor_id`,`hor_id_profesionista`),
  KEY `hor_id_profesionista` (`hor_id_profesionista`),
  CONSTRAINT `horarios_ibfk_1` FOREIGN KEY (`hor_id_profesionista`) REFERENCES `profesionista` (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of horarios
-- ----------------------------

-- ----------------------------
-- Table structure for modalidad_violencia
-- ----------------------------
DROP TABLE IF EXISTS `modalidad_violencia`;
CREATE TABLE `modalidad_violencia` (
  `mod_id_violencia` int(11) NOT NULL AUTO_INCREMENT,
  `mod_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mod_id_violencia`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modalidad_violencia
-- ----------------------------
INSERT INTO `modalidad_violencia` VALUES ('1', 'Doméstica');
INSERT INTO `modalidad_violencia` VALUES ('2', 'Institucional');
INSERT INTO `modalidad_violencia` VALUES ('3', 'Laboral');
INSERT INTO `modalidad_violencia` VALUES ('4', 'Libertad Reproductiva');
INSERT INTO `modalidad_violencia` VALUES ('5', 'Obstétrica');
INSERT INTO `modalidad_violencia` VALUES ('6', 'Mediática');

-- ----------------------------
-- Table structure for nivel_estudio
-- ----------------------------
DROP TABLE IF EXISTS `nivel_estudio`;
CREATE TABLE `nivel_estudio` (
  `niv_id_nivel_estudio` int(11) NOT NULL AUTO_INCREMENT,
  `niv_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`niv_id_nivel_estudio`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nivel_estudio
-- ----------------------------
INSERT INTO `nivel_estudio` VALUES ('1', 'Primaria');
INSERT INTO `nivel_estudio` VALUES ('2', 'Secundaria');
INSERT INTO `nivel_estudio` VALUES ('3', 'Preparatoria');
INSERT INTO `nivel_estudio` VALUES ('4', 'Licenciatura');
INSERT INTO `nivel_estudio` VALUES ('5', 'Posgrado');
INSERT INTO `nivel_estudio` VALUES ('6', 'Maestría');
INSERT INTO `nivel_estudio` VALUES ('7', 'Doctorado');

-- ----------------------------
-- Table structure for profesionista
-- ----------------------------
DROP TABLE IF EXISTS `profesionista`;
CREATE TABLE `profesionista` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_nombre` varchar(50) DEFAULT NULL,
  `pro_apellidop` varchar(50) DEFAULT NULL,
  `pro_apellidom` varchar(50) DEFAULT NULL,
  `pro_municipio_atencion` varchar(50) DEFAULT NULL,
  `pro_id_especialidad` int(11) NOT NULL,
  `pro_id_user` int(11) NOT NULL,
  PRIMARY KEY (`pro_id`,`pro_id_especialidad`,`pro_id_user`),
  KEY `pro_id_especialidad` (`pro_id_especialidad`),
  KEY `pro_id_user` (`pro_id_user`),
  CONSTRAINT `profesionista_ibfk_2` FOREIGN KEY (`pro_id_user`) REFERENCES `user` (`id`),
  CONSTRAINT `profesionista_ibfk_1` FOREIGN KEY (`pro_id_especialidad`) REFERENCES `especialidad` (`esp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profesionista
-- ----------------------------
INSERT INTO `profesionista` VALUES ('1', 'VANESSA', 'COLORADO', 'ARIAS', 'CENTRO', '1', '1');

-- ----------------------------
-- Table structure for tipos_violencia
-- ----------------------------
DROP TABLE IF EXISTS `tipos_violencia`;
CREATE TABLE `tipos_violencia` (
  `vio_id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `vio_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`vio_id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipos_violencia
-- ----------------------------
INSERT INTO `tipos_violencia` VALUES ('1', 'Económica');
INSERT INTO `tipos_violencia` VALUES ('2', 'Laboral');
INSERT INTO `tipos_violencia` VALUES ('3', 'Institucional');
INSERT INTO `tipos_violencia` VALUES ('4', 'Psicológica');
INSERT INTO `tipos_violencia` VALUES ('5', 'Física');
INSERT INTO `tipos_violencia` VALUES ('6', 'Sexual');
INSERT INTO `tipos_violencia` VALUES ('7', 'Simbólica');
INSERT INTO `tipos_violencia` VALUES ('8', 'Patrimonial');

-- ----------------------------
-- Table structure for tipo_capacitacion
-- ----------------------------
DROP TABLE IF EXISTS `tipo_capacitacion`;
CREATE TABLE `tipo_capacitacion` (
  `tip_id_capacitacion` int(11) NOT NULL AUTO_INCREMENT,
  `tip_nombre` varchar(100) DEFAULT NULL,
  `tip_descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`tip_id_capacitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo_capacitacion
-- ----------------------------

-- ----------------------------
-- Table structure for tipo_servicio
-- ----------------------------
DROP TABLE IF EXISTS `tipo_servicio`;
CREATE TABLE `tipo_servicio` (
  `serv_id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `serv_nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`serv_id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo_servicio
-- ----------------------------
INSERT INTO `tipo_servicio` VALUES ('1', 'Entrevista de 1ra vez');
INSERT INTO `tipo_servicio` VALUES ('2', 'Asesoria legal');
INSERT INTO `tipo_servicio` VALUES ('3', 'Asesoria psicologica');
INSERT INTO `tipo_servicio` VALUES ('4', 'Canalización');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `superadmin` smallint(6) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'superadmin', '6xFy7zt5U183VhdrhsC4xq2_e9quDmqC', '$2y$13$knP3Jgr9dW.aN9BMNuptOu3JyCjdRNJMphPxZW2vVNGT6phor6u7.', null, '1', '1', '1463113424', '1463113424', null, null, null, '0');

-- ----------------------------
-- Table structure for user_visit_log
-- ----------------------------
DROP TABLE IF EXISTS `user_visit_log`;
CREATE TABLE `user_visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `language` char(2) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visit_time` int(11) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `user_visit_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_visit_log
-- ----------------------------
INSERT INTO `user_visit_log` VALUES ('1', '60a486a75bfee', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621395111', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('2', '60a49b59f25a7', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621400409', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('3', '60a5dcd79bb82', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621482711', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('4', '60a713f4dee1c', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621562356', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('5', '60a716a2ed4d7', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621563042', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('6', '60a7181223a32', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621563410', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('7', '60a725b1808fe', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621566897', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('8', '60a86e1dd1897', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621650973', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('9', '60aac18b27c39', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621803403', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('10', '60aacb24e7541', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621805860', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('11', '60aadc53e36a0', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621810259', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('12', '60ab0a0412079', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621821956', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('13', '60ab1f07531ae', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621827335', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('14', '60ab306fa6848', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621831791', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('15', '60ab399708777', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621834135', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('16', '60ac6f5817568', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621913432', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('17', '60ac7ad37a67a', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1', '1621916371', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('18', '60c1654c7bccd', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623287116', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('19', '60c173204e24e', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623290656', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('20', '60c1757d648b1', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623291261', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('21', '60c2c40ab4cbb', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623376906', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('22', '60c2c84a77a4b', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623377994', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('23', '60c2ce4239eaa', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623379522', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('24', '60c4f8b69498b', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623521462', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('25', '60c4fba15b23c', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623522209', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('26', '60c5667c267fe', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623549564', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('27', '60c56f745ab6d', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623551860', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('28', '60c645e60b697', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623606758', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('29', '60c7981593c76', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36', '1', '1623693333', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('30', '60cb91fa11815', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', '1', '1623953914', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('31', '60ccbaa267b55', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', '1', '1624029858', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('32', '60db4241ac228', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', '1', '1624982081', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('33', '60dc955015681', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', '1', '1625068880', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('34', '60dc9551257c0', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', '1', '1625068881', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('35', '60dde90e34ac7', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', '1', '1625155854', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('36', '6112a42ea6b98', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '1', '1628611630', 'Chrome', 'Windows');
INSERT INTO `user_visit_log` VALUES ('37', '61133f69005ec', '127.0.0.1', 'es', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '1', '1628651369', 'Chrome', 'Windows');

-- ----------------------------
-- Table structure for usuaria
-- ----------------------------
DROP TABLE IF EXISTS `usuaria`;
CREATE TABLE `usuaria` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_folio_banavim` varchar(15) DEFAULT NULL,
  `reg_nombre_usuaria` varchar(50) NOT NULL,
  `reg_apellidop` varchar(100) DEFAULT NULL,
  `reg_apellidom` varchar(100) DEFAULT NULL,
  `reg_telefono` int(10) DEFAULT NULL,
  `reg_fecha_nacimiento` date DEFAULT NULL,
  `reg_total_hijos` int(2) DEFAULT NULL,
  `reg_total_niños` int(2) DEFAULT NULL,
  `reg_niñas` int(2) DEFAULT NULL,
  `reg_id_estatus` int(11) NOT NULL,
  `reg_id_nivel_estudio` int(11) NOT NULL,
  `reg_id__estado_civil` int(11) NOT NULL,
  `reg_id_discapacidad` int(11) NOT NULL,
  `reg_id_actividad_economica` varchar(50) NOT NULL,
  `reg_id_enfermedades_cronicas` int(11) NOT NULL,
  `reg_id_tipo_violencia` int(11) NOT NULL,
  `reg_id_modalidad_violencia` int(11) NOT NULL,
  `reg_seguro` smallint(1) DEFAULT '0',
  `reg_servicio_social` smallint(1) DEFAULT '0',
  `reg_algun_ingreso` smallint(1) DEFAULT '0',
  `reg_obtener_oficio` smallint(1) DEFAULT '0',
  `reg_aprender_oficio` smallint(1) DEFAULT '0',
  `reg_cabeza_familia` smallint(1) DEFAULT '0',
  `reg_vivienda_propia` smallint(1) DEFAULT '0',
  `reg_bienes_propios` smallint(1) DEFAULT '0',
  `reg_fecha_registro` datetime DEFAULT NULL,
  `reg_fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`reg_id`,`reg_id_estatus`,`reg_id_nivel_estudio`,`reg_id__estado_civil`,`reg_id_discapacidad`,`reg_id_enfermedades_cronicas`,`reg_id_tipo_violencia`,`reg_id_modalidad_violencia`),
  KEY `reg_id_estatus` (`reg_id_estatus`),
  KEY `reg_id__estado_civil` (`reg_id__estado_civil`),
  KEY `reg_id_actividad_economica` (`reg_id_actividad_economica`),
  KEY `reg_id_enfermedades_cronicas` (`reg_id_enfermedades_cronicas`),
  KEY `reg_id_discapacidad` (`reg_id_discapacidad`),
  KEY `reg_id_nivel_estudio` (`reg_id_nivel_estudio`),
  KEY `reg_id_tipo_violencia` (`reg_id_tipo_violencia`),
  KEY `reg_id_modalidad_violencia` (`reg_id_modalidad_violencia`),
  KEY `reg_id` (`reg_id`),
  CONSTRAINT `usuaria_ibfk_1` FOREIGN KEY (`reg_id_estatus`) REFERENCES `estatus` (`est_id_estatus`),
  CONSTRAINT `usuaria_ibfk_2` FOREIGN KEY (`reg_id__estado_civil`) REFERENCES `estado_civil` (`civ_id_estado_civil`),
  CONSTRAINT `usuaria_ibfk_4` FOREIGN KEY (`reg_id_enfermedades_cronicas`) REFERENCES `enfermedades_cronicas` (`enf_id_enfermedades_cronicas`),
  CONSTRAINT `usuaria_ibfk_5` FOREIGN KEY (`reg_id_discapacidad`) REFERENCES `discapacidad` (`dis_id_discapacidad`),
  CONSTRAINT `usuaria_ibfk_6` FOREIGN KEY (`reg_id_nivel_estudio`) REFERENCES `nivel_estudio` (`niv_id_nivel_estudio`),
  CONSTRAINT `usuaria_ibfk_7` FOREIGN KEY (`reg_id_tipo_violencia`) REFERENCES `tipos_violencia` (`vio_id_tipo`),
  CONSTRAINT `usuaria_ibfk_8` FOREIGN KEY (`reg_id_modalidad_violencia`) REFERENCES `modalidad_violencia` (`mod_id_violencia`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuaria
-- ----------------------------
INSERT INTO `usuaria` VALUES ('5', '4343', 'fwdfdff', 'dfd', 'fff', '4443455', '2021-07-27', '6', '4', '3', '1', '3', '3', '2', '1', '3', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', null, null);
