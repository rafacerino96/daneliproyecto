<?php

return [
    'adminEmail'    => 'admin@example.com',
    'senderEmail'   => 'noreply@example.com',
    'senderName'    => 'Example.com mailer',
    'sino'          => [1 => "Si", 2 => "No"],
    'estatus'       => [
        1 => "En proceso",
        2 => "Aprobada",
        3 => "No aprobada",
    ],
];
