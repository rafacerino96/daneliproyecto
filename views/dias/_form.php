<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;

?> 
<?php $form = ActiveForm::begin(['options' => ['class' => 'disable-submit-buttons'],]); ?>
 
<div class="row">
        <div class="col-sm-4 col-md-offset-4">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Configuración de dias
                </strong>
            </div>
            <div class="panel-body">
        <div class="row"> 
             <div class="col-sm-12">
                <?= $form->field($model, 'dia_lunes')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Lunes', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,
                    ],
                    'labelSettings' => [
                        'label' => 'Lunes',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
        </div>
            
     
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'dia_martes')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Martes', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,

                    ],
                    'labelSettings' => [
                        'label' => 'Martes',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'dia_miercoles')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Miercoles', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,

                    ],
                    'labelSettings' => [
                        'label' => 'Miercoles',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'dia_jueves')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Jueves', 
                    ],
                    'pluginOptions'=>[ 
                        'threeState' => false,

                    ],
                    'labelSettings' => [
                        'label' => 'Jueves',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'dia_viernes')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Viernes', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,

                    ],
                    'labelSettings' => [
                        'label' => 'Viernes',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'dia_sabado')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Sabado', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,

                    ],
                    'labelSettings' => [
                        'label' => 'Sabado',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'dia_domingo')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'select-Domingo', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,

                    ],
                    'labelSettings' => [
                        'label' => 'Domingo',
                        'position' => CheckboxX::LABEL_RIGHT
                    ]
                ])->label(false) ?>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-4 col-md-offset-4">
        <div class="row">
            <div class="col-sm-4 col-md-offset-3">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success' ]) ?>
                </div>
            </div>
    </div>
</div>
 <?php ActiveForm::end(); ?>


 
