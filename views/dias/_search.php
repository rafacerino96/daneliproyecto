<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DiasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dia_id') ?>

    <?= $form->field($model, 'dia_lunes') ?>

    <?= $form->field($model, 'dia_martes') ?>

    <?= $form->field($model, 'dia_miercoles') ?>

    <?= $form->field($model, 'dia_jueves') ?>

    <?php // echo $form->field($model, 'dia_viernes') ?>

    <?php // echo $form->field($model, 'dia_sabado') ?>

    <?php // echo $form->field($model, 'dia_domingo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
