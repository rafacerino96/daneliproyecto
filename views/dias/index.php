<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Dias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dia_id',
            'dia_lunes',
            'dia_martes',
            'dia_miercoles',
            'dia_jueves',
            //'dia_viernes',
            //'dia_sabado',
            //'dia_domingo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
