<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dias */

$this->title = 'Update Dias: ' . $model->dia_id;
$this->params['breadcrumbs'][] = ['label' => 'Dias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dia_id, 'url' => ['view', 'id' => $model->dia_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dias-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
