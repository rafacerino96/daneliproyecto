<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Profesionista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profesionista-form">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Formulario de Profesionista
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-4">
            <?= $form->field($model, 'pro_nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'pro_apellidop')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'pro_apellidom')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4">
            <?= $form->field($model, 'pro_municipio_atencion')->widget(Select2::classname(), [
            'data' => $municipios,
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'pro_id_especialidad')->widget(Select2::classname(), [
            'data' => $especialidades,
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'pro_id_user')->widget(Select2::classname(), [
            'data' => $usuarios,
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
    </div>

    </div></div></div></div>
    

    <div class="row">
        <div class="col-xs-2 col-xs-offset-5">
            <div class="row">
                <div class="col-xs-4">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div></div></div></div>

    <?php ActiveForm::end(); ?>

    

</div>
