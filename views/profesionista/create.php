<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesionista */

$this->title = 'Create Profesionista';
$this->params['breadcrumbs'][] = ['label' => 'Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesionista-create">

    <?= $this->render('_form', [
        'model' => $model,
        'municipios' => $municipios,
        'especialidades' => $especialidades,
        'usuarios' => $usuarios,
    ]) ?>

</div>
