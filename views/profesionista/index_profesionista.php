<?php //var_dump($date);die;

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\datecontrol\DateControl;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profesionistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesionista-index">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Mis citas
                    </strong>
                </div>
                <div class="panel-body">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
        echo DateControl::widget([
            'id' => 'datecontrol',
            //'onchange' => 'redirectCitas(this)',
            /*'options'=>[
                'id'=>'fkprofesionista',
                'onchange' => 'redirectCitas(data)'
            ],*/
            'type'=>DateControl::FORMAT_DATE,
            'ajaxConversion'=>true,
            'name'=>'kartik-date', 
            'displayFormat' => 'php:Y-m-d',
            //'saveFormat' => 'php:U',
            'value'=>$date,
            'widgetOptions' => [
                'pluginOptions' => [
                    'autoclose' => true
                ],
            ],
            'pluginEvents' =>[
                "change" => "function() {redirectCitas(this);}"
            ],
        ]);
    ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cit_id_cita',
            'citIdUsuaria.nombreCompleto',
            'citHorario.conf_descripcion',
            'estatus',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'controller' => 'citas',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        //$t = 'index.php?r=site/update&id='.$model->id;
                        //return Html::a('Update', $url);
                        return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/citas/update?id=' . $model->cit_id_cita]) );
                    },
                ],
            ],
        ],
    ]); ?>

</div></div></div></div>


</div>

<?php

$js = <<< SCRIPT

function redirectCitas(data){
    fecha = $(data).val();
    if($(data).val() != "") {
        $(location).attr('href','profesionista?date='+fecha);
    }
}

SCRIPT;

$this->registerJs($js, $this::POS_END);
?>

