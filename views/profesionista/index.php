<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profesionistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesionista-index">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Profesionista
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/site/index'], ['class' => 'btn btn-sm btn-warning']) ?>
                    <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['create'], ['class' => 'btn  btn-sm btn-success']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header'  => Html::a('<i class="glyphicon glyphicon-filter"></i>', ['index'], ['title' => 'Limpiar filtros']),
            ],

            //'pro_id',
            'pro_nombre',
            'pro_apellidop',
            'pro_apellidom',
            
            [
                'attribute'         => 'pro_municipio_atencion',
                'filter'            => \app\models\Municipios::map(),
                'filterType'        => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options'   => [
                        'prompt' => 'Seleccione el municipio',
                    ],
                ],
                'value'             => function ($model) {
                    return $model->municipioAtencion;
                }
            ],

            [
                'attribute'         => 'pro_id_especialidad',
                'filter'            => \app\models\Especialidad::map(),
                'filterType'        => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options'   => [
                        'prompt' => 'Seleccione el municipio',
                    ],
                ],
                'value'             => function ($model) {
                    return $model->especialidad;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div></div></div></div>


</div>
