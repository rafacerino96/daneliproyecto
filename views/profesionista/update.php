<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesionista */

$this->title = 'Update Profesionista: ' . $model->pro_id;
$this->params['breadcrumbs'][] = ['label' => 'Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pro_id, 'url' => ['view', 'id' => $model->pro_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profesionista-update">

    <?= $this->render('_form', [
        'model' => $model,
        'municipios' => $municipios,
        'especialidades' => $especialidades,
        'usuarios' => $usuarios,
    ]) ?>

</div>
