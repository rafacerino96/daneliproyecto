<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProfesionistaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profesionista-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pro_id') ?>

    <?= $form->field($model, 'pro_nombre') ?>

    <?= $form->field($model, 'pro_apellidop') ?>

    <?= $form->field($model, 'pro_apellidom') ?>

    <?= $form->field($model, 'pro_municipio_atencion') ?>

    <?php // echo $form->field($model, 'pro_id_especialidad') ?>

    <?php // echo $form->field($model, 'pro_id_user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
