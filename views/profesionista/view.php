<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Profesionista */

$this->title = $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="profesionista-view">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Profesionista
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('Actualizar', ['update?id=' . $model->pro_id], ['class' => 'btn btn-sm btn-primary']) ?>
                        <?= Html::a('Eliminar', ['delete', 'id' => $model->pro_id], [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pro_nombre',
            'pro_apellidop',
            'pro_apellidom',
            'municipioAtencion',
            'especialidad',
            'username',
        ],
    ]) ?>

    </div></div></div></div>

</div>
