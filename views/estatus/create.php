<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estatus */

$this->title = 'Create Estatus';
$this->params['breadcrumbs'][] = ['label' => 'Estatuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estatus-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
