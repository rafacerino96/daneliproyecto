<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estatus */

$this->title = 'Update Estatus: ' . $model->est_id_estatus;
$this->params['breadcrumbs'][] = ['label' => 'Estatuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->est_id_estatus, 'url' => ['view', 'id' => $model->est_id_estatus]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estatus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
