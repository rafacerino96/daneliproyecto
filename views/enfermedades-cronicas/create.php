<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EnfermedadesCronicas */

$this->title = 'Create Enfermedades Cronicas';
$this->params['breadcrumbs'][] = ['label' => 'Enfermedades Cronicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enfermedades-cronicas-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
