<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EnfermedadesCronicas */

$this->title = 'Update Enfermedades Cronicas: ' . $model->enf_id_enfermedades_cronicas;
$this->params['breadcrumbs'][] = ['label' => 'Enfermedades Cronicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->enf_id_enfermedades_cronicas, 'url' => ['view', 'id' => $model->enf_id_enfermedades_cronicas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="enfermedades-cronicas-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
