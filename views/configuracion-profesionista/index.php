<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConfiguracionProfesionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Configuracion Profesionistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configuracion-profesionista-index">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Configuración de Agenda de Profesionista
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/site/index'], ['class' => 'btn btn-sm btn-warning']) ?>
                    <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['create'], ['class' => 'btn  btn-sm btn-success']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header'  => Html::a('<i class="glyphicon glyphicon-filter"></i>', ['index'], ['title' => 'Limpiar filtros']),
            ],


            [
                'attribute'         => 'fkProfesionistaMunicipio',
                'filter'            => \app\models\Municipios::map(),
                'filterType'        => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options'   => [
                        'prompt' => 'Seleccione el municipio',
                    ],
                ],
                'value'             => function ($model) {
                    return $model->conFkProfesionista->municipioAtencion;
                }
            ],
            'nombreProfesionista',
            
            [
                'attribute'         => 'con_dia_inicial',
                'filter'            => false,
            ],

            [
                'attribute'         => 'con_dia_final',
                'filter'            => false,
            ],

            [
                'attribute'         => 'con_status',
                'filter'            => Yii::$app->params['estatus'],
                'filterType'        => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options'   => [
                        'prompt' => 'Seleccione el estatus',
                    ],
                ],
                'value'             => function ($model) {
                    return $model->status;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div></div></div></div>
</div>
