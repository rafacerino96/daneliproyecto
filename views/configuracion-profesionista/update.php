<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracionProfesionista */

$this->title = 'Update Configuracion Profesionista: ' . $model->con_id;
$this->params['breadcrumbs'][] = ['label' => 'Configuracion Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->con_id, 'url' => ['view', 'id' => $model->con_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="configuracion-profesionista-update">


    <?= $this->render('_form', [
        'model' => $model,
        'profesionista'=>$profesionista,
        'configuracionHorario'  => $configuracionHorario,
        'horasAusenciaProfesionista' => $horasAusenciaProfesionista,
        //'escenario' => $escenario,
    ]) ?>

</div>
