<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracionProfesionista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="configuracion-profesionista-form">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Formulario de Agenda de Profesionista
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                </strong>
            </div>
            <div class="panel-body">
                
    <?php $form = ActiveForm::begin(); ?>

        <div class="col-sm-8">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Detalle profesionista
                </strong>
            </div>
            <div class="panel-body">

    <?= $form->field($model, 'con_fkProfesionista')->widget(select2::classname(),[
                        'data' =>$profesionista,
                        'options' => ['placeholder' => 'Seleccione'],
                        'pluginLoading' => false,
                        'pluginOptions' => [
                            'allowClear' => true],         
                    ]); ?>

     
    <?= $form->field($model, "con_dia_inicial")->widget(DatePicker::classname(), [
                        
                        'removeButton' => false,
                        'pluginOptions' => [
                        'autoclose' => true,
                        'startDate' => date('Y-m-d'),
                        'format' => 'yyyy-mm-dd'],
                    ]); ?>

    <?= $form->field($model, "con_dia_final")->widget(DatePicker::classname(), [
                        
                        'removeButton' => false,
                        'pluginOptions' => [
                        'autoclose' => true,
                        'startDate' => date('Y-m-d'),
                        'format' => 'yyyy-mm-dd'],
                    ]); ?> 

    <?= $form->field($model, 'con_detalle')->textInput(['maxlength' => true]) ?>
    
    <?php if(Yii::$app->user->identity->hasRole(['Admin', 'supervisor'])){ ?>

    <?= $form->field($model, 'con_status')->widget(Select2::classname(), [
            'data' => Yii::$app->params['estatus'],
            'options' => ['placeholder' => 'Seleccione un estatus ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    <?php } ?>

    <?= $form->field($model, 'con_todo_el_dia')->widget(CheckboxX::classname(), [
                    'options' => [
                        'id' => 'todo-el-dia', 
                    ],
                    'pluginOptions'=>[
                        'threeState' => false,

                    ]
                ]) ?>
                Por favor seleccione el horario no disponible.

    </div></div></div>

        <div class="col-sm-4">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Detalle ausencia
                </strong>
            </div>
            <div class="panel-body">

    <?php 

    foreach ($configuracionHorario as $key => $hora) {
        echo '<div class="mb-2 has-warning">';
        echo '<label class="cbx-label" for="checkbox-'. $hora->conf_id .'">';

        $value = 0;
        foreach ($horasAusenciaProfesionista as $key => $confHora) {
            if($confHora->hor_fkConfiguracionHorario == $hora->conf_id){
                $value = 1;
            }
        }
        echo CheckboxX::widget([
            'name' => 'checkbox-' . $hora->conf_id,
            'value' => $value,
            'initInputType' => CheckboxX::INPUT_CHECKBOX,
            'autoLabel' => true,
            'labelSettings' => [
                'label' => ''. $hora->conf_descripcion .'',
                'position' => CheckboxX::LABEL_RIGHT
            ]
        ]);

        echo "</label></div>";
    }

    ?>     
        </div></div></div>

    </div></div></div></div>

    <div class="row">
        <div class="col-sm-2 col-md-offset-5">
            <div class="row">
                <div class="col-sm-12">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div></div></div></div>

    <?php ActiveForm::end(); ?>

</div>
