<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracionProfesionistaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="configuracion-profesionista-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'con_id') ?>

    <?= $form->field($model, 'con_fkProfesionista') ?>

    <?= $form->field($model, 'con_dia_inicial') ?>

    <?= $form->field($model, 'con_dia_final') ?>

    <?= $form->field($model, 'con_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
