<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracionProfesionista */

$this->title = 'Create Configuracion Profesionista';
$this->params['breadcrumbs'][] = ['label' => 'Configuracion Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configuracion-profesionista-create">

    <?= $this->render('_form', [
        'model' => $model,
        'profesionista' => $profesionista,
        'horas'=> $horas,
        'configuracionHorario'=> $configuracionHorario,
        'horasAusenciaProfesionista' => $horasAusenciaProfesionista,
        //'escenario' => $escenario,
    ]) ?>

</div>
