<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TipoCapacitacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-capacitacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Tipo de Capacitación
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?= $form->field($model, 'tip_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tip_descripcion')->textInput(['maxlength' => true]) ?>

    </div></div></div></div>

    <div class="row">
        <div class="col-xs-2 col-xs-offset-5">
            <div class="row">
                <div class="col-xs-4">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div></div></div></div>

    <?php ActiveForm::end(); ?>

</div>
