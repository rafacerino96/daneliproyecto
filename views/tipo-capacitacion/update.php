<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoCapacitacion */

$this->title = 'Update Tipo Capacitacion: ' . $model->tip_id_capacitacion;
$this->params['breadcrumbs'][] = ['label' => 'Tipo Capacitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tip_id_capacitacion, 'url' => ['view', 'id' => $model->tip_id_capacitacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipo-capacitacion-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
