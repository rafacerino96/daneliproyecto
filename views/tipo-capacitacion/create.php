<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoCapacitacion */

$this->title = 'Create Tipo Capacitacion';
$this->params['breadcrumbs'][] = ['label' => 'Tipo Capacitacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-capacitacion-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
