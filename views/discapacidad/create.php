<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Discapacidad */

$this->title = 'Create Discapacidad';
$this->params['breadcrumbs'][] = ['label' => 'Discapacidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discapacidad-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
