<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Discapacidad */

$this->title = 'Update Discapacidad: ' . $model->dis_id_discapacidad;
$this->params['breadcrumbs'][] = ['label' => 'Discapacidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dis_id_discapacidad, 'url' => ['view', 'id' => $model->dis_id_discapacidad]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="discapacidad-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
