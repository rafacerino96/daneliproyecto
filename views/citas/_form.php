<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\datecontrol\DateControl;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="citas-form">

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Formulario de Citas
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                </strong>
            </div>
            <div class="panel-body">
               <div class="usuaria-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <!-- 
        /Nuevos 
    -->

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'cit_municipio')->dropDownList($municipios, [
            'id' => 'fkmunicipio',
            'prompt'=>'Seleccione la entidad ...',
        ]); ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'cit_id_profesionista')->widget(DepDrop::classname(), [
                'data' => $profesionistas,
                'options'=>[
                    'id'=>'fkprofesionista',
                    'onchange' => 'redirect(this)'
                ],
                'pluginOptions'=>[
                    'depends'=>['fkmunicipio'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/citas/profesionistas'])
                ],
                /*'pluginEvents' =>[
                    "change" => "function() {redirect();}"
                ],*/
            ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'cit_id_usuaria')->widget(Select2::classname(), [
            'data' => $usuarias,
            'options' => [
                'placeholder' => 'Select a state ...',
                'onchange' => 'redirectUsuaria()',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>
        </div>
        <div class="col-xs-3">
            <?= 
                /*$form->field($model, 'cit_fecha_atencion')->widget(DateControl::classname(), [
                    'type' => 'date',
                    'id' => 'fkfecha',
                    'options'=>['id'=>'fkfecha'],
                    'ajaxConversion' => true,
                    'autoWidget' => true,
                    'widgetClass' => '',
                    'displayFormat' => 'php:d-F-Y',
                    'saveFormat' => 'php:Y-m-d',
                    'saveTimezone' => 'UTC',
                    'displayTimezone' => 'America/Cancun',
                    'widgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'php:d-F-Y'
                        ]
                    ],
                    'language' => 'es',
                ]);*/

                /*$form->field($model, 'cit_fecha_atencion')->widget(DatePicker::classname(), [
                    //'name' => 'dp_1',
                    'id' => 'fkfecha',
                    'options'=>['id'=>'fkfecha'],
                    'type' => DatePicker::TYPE_INLINE,
                    //'value' => '23-Feb-1982',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'daysOfWeekDisabled' => [ 0 , 6 ],
                        'daysOfWeekHighlighted' => $dias,
                        'datesDisabled' => $datesDisabled,
                    ],
                ]);*/

                $form->field($model, 'cit_fecha_atencion')->widget(DatePicker::classname(), [
                    'id' => 'fkfecha',
                    'options'=>['id'=>'fkfecha'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'daysOfWeekDisabled' => [ 0 , 6 ],
                        'daysOfWeekHighlighted' => $dias,
                        'datesDisabled' => $datesDisabled,
                    ],
                ]);
            ?>
        </div>

        <div class="col-xs-3">
            <?= 
            
            $form->field($model, 'cit_horario')->widget(DepDrop::classname(), [
                'data' => $horarios,
                'options'=>['id'=>'fkhorario'],
                'pluginOptions'=>[
                    'depends'=>['fkprofesionista', 'fkfecha'],
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/citas/horarios'])
                ],
                'type'=> DepDrop::TYPE_SELECT2,
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4">
            <?= $form->field($model, 'cit_proceso')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'cit_servicio')->widget(Select2::classname(), [
            'data' => $servicios,
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <?php if($escenario == 'update'){ ?>
        <div class="col-xs-4">
            <?= $form->field($modelUsuaria, 'reg_id_estatus')->widget(Select2::classname(), [
            'data' => $estatus,
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <?php } ?>
    </div>
    <?php 
        if($escenario == 'update'){
            echo '<div class="row">';
            echo '<div class="col-xs-12">';
            echo $form->field($model, 'cit_observaciones')->textarea(['rows' => 6]);
            echo '</div></div>';
        }
    ?>

    </div></div></div></div>

    <div class="row">
        <div class="col-md-2 col-md-offset-5">
            <div class="form-group">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$id = '';
if($escenario == 'update'){
    $id = '&id=' . $model->cit_id_cita;
}

$js = <<< SCRIPT

function redirect(data){
    municipio = $('#fkmunicipio').val();
    usuaria = $('#citas-cit_id_usuaria').val();
    proceso = $('#citas-cit_proceso').val();
    servicio = $('#citas-cit_servicio').val();
    estatus = $('#citas-cit_id_estatus').val();
    if($(data).val() != "") {
        $(location).attr('href','{$escenario}'+'?municipio='+municipio+'&profesionista='+$(data).val()+'&usuaria='+usuaria+'&proceso='+proceso+'&servicio='+servicio+'&estatus='+estatus+'{$id}');
    }
    
}

function redirectUsuaria(){
    profesionista = $('#fkprofesionista').val();
    municipio = $('#fkmunicipio').val();
    usuaria = $('#citas-cit_id_usuaria').val();
    proceso = $('#citas-cit_proceso').val();
    servicio = $('#citas-cit_servicio').val();
    estatus = $('#citas-cit_id_estatus').val();
    if(usuaria != "") {
        $(location).attr('href','{$escenario}'+'?municipio='+municipio+'&profesionista='+profesionista+'&usuaria='+usuaria+'&proceso='+proceso+'&servicio='+servicio+'&estatus='+estatus+'{$id}');
    }
    //alert(profesionista);
}

SCRIPT;

$this->registerJs($js, $this::POS_END);
?>
