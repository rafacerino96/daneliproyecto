<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CitasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="citas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cit_id_cita') ?>

    <?= $form->field($model, 'cit_observaciones') ?>

    <?= $form->field($model, 'cit_id_estatus') ?>

    <?= $form->field($model, 'cit_servicio') ?>

    <?= $form->field($model, 'cit_proceso') ?>

    <?php // echo $form->field($model, 'cit_fecha_atencion') ?>

    <?php // echo $form->field($model, 'cit_municipio') ?>

    <?php // echo $form->field($model, 'cit_fecha_registro') ?>

    <?php // echo $form->field($model, 'cit_fecha_modificacion') ?>

    <?php // echo $form->field($model, 'cit_id_usuaria') ?>

    <?php // echo $form->field($model, 'cit_id_profesionista') ?>

    <?php // echo $form->field($model, 'cit_horario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
