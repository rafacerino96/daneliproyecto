<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */

$this->title = 'Create Citas';
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citas-create">

    <?= $this->render('_form', [
        'model' => $model,
        'municipios' => $municipios,
        'profesionistas' => $profesionistas,
        'usuarias' => $usuarias,
        'servicios' => $servicios,
        'estatus' => $estatus,
        'horarios' => $horarios,
        'escenario' => $escenario,
        'dias' => $dias,
        'datesDisabled' => $datesDisabled,
        'modelUsuaria' => $modelUsuaria,
    ]) ?>

</div>
