<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */

$this->title = $model->cit_id_cita;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="citas-view">

    <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Formulario de Registro
                        <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('Update', ['update?id=' . $model->cit_id_cita], ['class' => 'btn btn-sm btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->cit_id_cita], [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?= Html::a('Expediente', ['/usuaria/view/', 'id' => $model->citIdUsuaria->reg_id], ['class' => 'btn btn-sm btn-default']) ?>
                    </strong>
                </div>
                <div class="panel-body">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'estatus',
            'servicio',
            'cit_proceso',
            'cit_fecha_atencion',
            'municipio',
            'cit_fecha_registro',
            'cit_fecha_modificacion',
            'usuaria',
            'profesionista',
            'horario',
            'cit_observaciones:ntext',
        ],
    ]) ?>

    </div></div></div></div>

</div>
