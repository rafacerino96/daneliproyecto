<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Citas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citas-index">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Citas
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/site/index'], ['class' => 'btn btn-sm btn-warning']) ?>
                    <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['create'], ['class' => 'btn  btn-sm btn-success']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header'  => Html::a('<i class="glyphicon glyphicon-filter"></i>', ['index'], ['title' => 'Limpiar filtros']),
            ],

            'usuaria',
            'profesionista',
            //'cit_fecha_atencion',

            [
                'attribute'         => 'cit_fecha_atencion',
                //'filter'            => \app\models\Municipios::map(),
                'filterType'        => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                    ],
                ],
                /*'value'             => function ($model) {
                    return $model->municipio;
                }*/
            ],

            'municipio',
            //'cit_fecha_atencion',
            //'cit_municipio',
            //'cit_fecha_registro',
            //'cit_fecha_modificacion',
            //'cit_id_usuaria',
            //'cit_id_profesionista',
            //'cit_horario',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        //$t = 'index.php?r=site/update&id='.$model->id;
                        //return Html::a('Update', $url);
                        return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update?id=' . $model->cit_id_cita]) );
                    },
                ],
            ],
        ],
    ]); ?>

    </div></div></div></div>


</div>
