<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */

$this->title = 'Update Citas: ' . $model->cit_id_cita;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cit_id_cita, 'url' => ['view', 'id' => $model->cit_id_cita]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="citas-update">

    <?= $this->render('_form', [
        'model' => $model,
        'municipios' => $municipios,
        'profesionistas' => $profesionistas,
        'usuarias' => $usuarias,
        'servicios' => $servicios,
        'estatus' => $estatus,
        'horarios' => $horarios,
        'escenario' => $escenario,
        'dias' => $dias,
        'datesDisabled' => $datesDisabled,
        'modelUsuaria' => $modelUsuaria,
    ]) ?>

</div>
