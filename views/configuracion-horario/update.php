<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracionHorario */

$this->title = 'Update Configuracion Horario: ' . $model->conf_id;
$this->params['breadcrumbs'][] = ['label' => 'Configuracion Horarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->conf_id, 'url' => ['view', 'id' => $model->conf_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="configuracion-horario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
