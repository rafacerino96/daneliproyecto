<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConfiguracionHorario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="configuracion-horario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'conf_id')->textInput() ?>

    <?= $form->field($model, 'conf_descripcion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
