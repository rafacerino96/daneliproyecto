<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiposViolencia */

$this->title = 'Update Tipos Violencia: ' . $model->vio_id_tipo;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Violencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vio_id_tipo, 'url' => ['view', 'id' => $model->vio_id_tipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipos-violencia-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
