<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TiposViolencia */

$this->title = $model->vio_id_tipo;
$this->params['breadcrumbs'][] = ['label' => 'Tipos Violencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tipos-violencia-view">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Tipo de violencia
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('Actualizar', ['update?id=' . $model->vio_id_tipo], ['class' => 'btn btn-sm btn-primary']) ?>
                        <?= Html::a('Eliminar', ['delete', 'id' => $model->vio_id_tipo], [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vio_id_tipo',
            'vio_nombre',
        ],
    ]) ?>

    </div></div></div></div>

</div>
