<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiposViolencia */

$this->title = 'Create Tipos Violencia';
$this->params['breadcrumbs'][] = ['label' => 'Tipos Violencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipos-violencia-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
