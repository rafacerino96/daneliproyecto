<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentosCapacitaciones */

$this->title = 'Create Documentos Capacitaciones';
$this->params['breadcrumbs'][] = ['label' => 'Documentos Capacitaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentos-capacitaciones-create">

    <?= $this->render('_form', [
        'capacitacion' => $capacitacion,
        'documents' => $documents,
        'message' => $message,
    ]) ?>

</div>
