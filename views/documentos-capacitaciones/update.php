<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentosCapacitaciones */

$this->title = 'Update Documentos Capacitaciones: ' . $model->docc_id;
$this->params['breadcrumbs'][] = ['label' => 'Documentos Capacitaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->docc_id, 'url' => ['view', 'id' => $model->docc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="documentos-capacitaciones-update">

<div class="row">
            <div class="col-sm-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>
                            <span class="glyphicon glyphicon-th"></span> Actualizar Documentos
                            <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/capacitaciones/view', 'id' => $model->docc_fkcapacitacion], ['class' => 'btn btn-sm btn-warning']) ?>
                        </strong>
                    </div>
                    <div class="panel-body">

                        <div class="documentos-form">

                        <?php $form = ActiveForm::begin(); ?>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <?= $form->field($model, 'docc_nombre')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-2 col-md-offset-5">
                                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
                                    </div>
                                </div>
                        
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
