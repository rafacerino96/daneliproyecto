<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentosCapacitaciones */
/* @var $form yii\widgets\ActiveForm */

if($message == 1){ 
    echo Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'title' => 'Éxito!',
        'icon' => 'fas fa-check-circle',
        'body' => 'El documento se guardo correctamente.',
        'showSeparator' => true,
        'delay' => 8000
    ]);
}

if($message == 2){
    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'title' => 'Error!',
        'icon' => 'fas fa-times-circle',
        'body' => 'No se pudo guardar el archivo en la carpeta, verifica que se tenga los permisos',
        'showSeparator' => true,
        'delay' => 8000
    ]);
}

if($message == 3){
    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'title' => 'Error!',
        'icon' => 'fas fa-times-circle',
        'body' => 'No se guardo en la base de datos',
        'showSeparator' => true,
        'delay' => 8000
    ]);
}



?>



<div class="documentos-form">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Agregar Documentos 
                        <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/capacitaciones/view', 'id' => $capacitacion->cap_id], ['class' => 'btn btn-sm btn-warning']) ?>
                    </strong>
                </div>
                <div class="panel-body">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=
    FileInput::widget([
        'model' => $documents,
        'attribute' => 'imageFile',
        'options' => ['multiple' => false]
    ]);
    ?>
    
    <?php ActiveForm::end(); ?>

    </div></div></div></div>

</div>

