<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentosCapacitacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documentos-capacitaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'docc_id') ?>

    <?= $form->field($model, 'docc_ruta') ?>

    <?= $form->field($model, 'docc_fkcapacitacion') ?>

    <?= $form->field($model, 'docc_nombre') ?>

    <?= $form->field($model, 'docc_estatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
