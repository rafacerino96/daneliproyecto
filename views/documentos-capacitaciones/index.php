<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentosCapacitacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documentos Capacitaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentos-capacitaciones-index">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Documentos 
                    <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['/documentos-capacitaciones/create', 'id' => $model->cap_id], ['class' => 'btn  btn-sm btn-success']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header'  => Html::a('<i class="glyphicon glyphicon-filter"></i>', ['index'], ['title' => 'Limpiar filtros']),
            ],
            //'docc_id',
            //'docc_ruta',
            //'docc_fkcapacitacion',
            'docc_nombre',
            //'docc_estatus',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'documentos-capacitaciones',
            ],
        ],
    ]); ?>
    </div></div></div></div>

</div>
