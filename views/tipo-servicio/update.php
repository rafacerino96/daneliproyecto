<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoServicio */

$this->title = 'Update Tipo Servicio: ' . $model->serv_id_tipo;
$this->params['breadcrumbs'][] = ['label' => 'Tipo Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->serv_id_tipo, 'url' => ['view', 'id' => $model->serv_id_tipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipo-servicio-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
