<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoServicio */

$this->title = 'Create Tipo Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Tipo Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-servicio-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
