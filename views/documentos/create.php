<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Documentos */

$this->title = $usuaria->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentos-create">

    <?= $this->render('_form', [
        'usuaria' => $usuaria,
        'documents' => $documents,
        'message' => $message,
        'usuaria' => $usuaria,
    ]) ?>

</div>
