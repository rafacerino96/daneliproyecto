<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EstadoCivilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estado Civils';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-civil-index">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Estado Civil
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/site/index'], ['class' => 'btn btn-sm btn-warning']) ?>
                    <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['create'], ['class' => 'btn  btn-sm btn-success']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header'  => Html::a('<i class="glyphicon glyphicon-filter"></i>', ['index'], ['title' => 'Limpiar filtros']),
            ],

            //'civ_id_estado_civil',
            'civ_nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div></div></div></div>


</div>
