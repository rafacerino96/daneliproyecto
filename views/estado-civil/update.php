<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoCivil */

$this->title = 'Update Estado Civil: ' . $model->civ_id_estado_civil;
$this->params['breadcrumbs'][] = ['label' => 'Estado Civils', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->civ_id_estado_civil, 'url' => ['view', 'id' => $model->civ_id_estado_civil]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estado-civil-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
