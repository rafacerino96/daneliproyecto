<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HorasAusenciaProfesionista */

$this->title = 'Create Horas Ausencia Profesionista';
$this->params['breadcrumbs'][] = ['label' => 'Horas Ausencia Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horas-ausencia-profesionista-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
