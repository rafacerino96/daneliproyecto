<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HorasAusenciaProfesionistaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="horas-ausencia-profesionista-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'hor_id') ?>

    <?= $form->field($model, 'hor_fkConfiguracionProfesionista') ?>

    <?= $form->field($model, 'hor_fkConfiguracionHorario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
