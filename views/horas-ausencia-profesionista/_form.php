<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HorasAusenciaProfesionista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="horas-ausencia-profesionista-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hor_fkConfiguracionProfesionista')->textInput() ?>

    <?= $form->field($model, 'hor_fkConfiguracionHorario')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
