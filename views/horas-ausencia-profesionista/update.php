<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HorasAusenciaProfesionista */

$this->title = 'Update Horas Ausencia Profesionista: ' . $model->hor_id;
$this->params['breadcrumbs'][] = ['label' => 'Horas Ausencia Profesionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->hor_id, 'url' => ['view', 'id' => $model->hor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="horas-ausencia-profesionista-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
