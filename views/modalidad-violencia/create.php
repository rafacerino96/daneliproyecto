<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ModalidadViolencia */

$this->title = 'Create Modalidad Violencia';
$this->params['breadcrumbs'][] = ['label' => 'Modalidad Violencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modalidad-violencia-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
