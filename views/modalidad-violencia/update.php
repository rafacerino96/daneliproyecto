<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ModalidadViolencia */

$this->title = 'Update Modalidad Violencia: ' . $model->mod_id_violencia;
$this->params['breadcrumbs'][] = ['label' => 'Modalidad Violencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mod_id_violencia, 'url' => ['view', 'id' => $model->mod_id_violencia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modalidad-violencia-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
