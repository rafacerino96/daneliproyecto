<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Capacitaciones */

$this->title = 'Update Capacitaciones: ' . $model->cap_id;
$this->params['breadcrumbs'][] = ['label' => 'Capacitaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cap_id, 'url' => ['view', 'cap_id' => $model->cap_id, 'cap_tip_id' => $model->cap_tip_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="capacitaciones-update">

    <?= $this->render('_form', [
        'model' => $model,
        'tipocapacitacion' => $tipocapacitacion,
    ]) ?>

</div>
