<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Capacitaciones */

$this->title = 'Crear Capacitación';
$this->params['breadcrumbs'][] = ['label' => 'Capacitaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="capacitaciones-create">

    <?= $this->render('_form', [
        'model' => $model,
        'tipocapacitacion' => $tipocapacitacion,
    ]) ?>

</div>
