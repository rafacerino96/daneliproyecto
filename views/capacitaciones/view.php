<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Capacitaciones */

$this->title = $model->cap_id;
$this->params['breadcrumbs'][] = ['label' => 'Capacitaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="capacitaciones-view">

<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Discapacidad
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('Actualizar', ['update?id=' . $model->cap_id], ['class' => 'btn btn-sm btn-primary']) ?>
                        <?= Html::a('Eliminar', ['delete', 'id' => $model->cap_id], [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                </strong>
            </div>
            <div class="panel-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cap_id',
            'cap_nombre',
            'cap_feha',
            'cap_hora_inicio',
            'cap_hora_termino',
            'cap_dependencia',
            'cap_total',
            'cap_hombres',
            'cap_mujeres',
            'tipoCapacitacion',
        ],
    ]) ?>

    </div></div></div></div>

    <?= $this->render('/documentos-capacitaciones/index', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
