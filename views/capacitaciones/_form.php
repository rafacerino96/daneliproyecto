<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use app\models\TipoCapacitacion;

/* @var $this yii\web\View */
/* @var $model app\models\Capacitaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="capacitaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-12"> 
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Formulario de capacitaciones
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                </strong>
            </div>
            <div class="panel-body">

    <div class="row">
        <div class= "col-xs-6">
            <?= $form->field($model, 'cap_nombre')->textInput(['maxlength' => true]) ?>
        </div>

        <div class= "col-xs-6">
            <?= $form->field($model, 'cap_dependencia')->textInput(['maxlength' => true]) ?>
        </div>
    </div>  
    
    <div class="row">
        <div class= "col-xs-3">
            <?= $form->field($model, 'cap_feha')->widget(DatePicker::classname(),[
                'options'=>['placeholder'=>'Ingrese la fecha...'],
                'pluginOptions'=>[
                    'format'=> 'yyyy-mm-dd',
                    'autoclose'=>true,
                ]
     ]); ?>
        </div>
        <div class= "col-xs-3">
            <?= $form->field($model, 'cap_hora_inicio')->widget(TimePicker::classname(), ['name' => 'birth_time',
    'options' => [
        'readonly' => true,
    ]
]); ?>
        </div>
        <div class= "col-xs-3">
            <?= $form->field($model, 'cap_hora_termino')->widget(TimePicker::classname(), ['name' => 'birth_time',
    'options' => [
        'readonly' => true,
    ]
]); ?>
        </div>
        <div class= "col-xs-3">
            <?= $form->field($model, 'cap_total')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class= "col-xs-4">
             <?= $form->field($model, 'cap_hombres')->textInput() ?>
        </div>
        <div class= "col-xs-4">
            <?= $form->field($model, 'cap_mujeres')->textInput() ?>
        </div>
        <div class= "col-xs-4">
            <?= $form->field($model, 'cap_tip_id')->dropDownList($tipocapacitacion,['prompt'=>'Seleccione el Tipo ...']) ?>
        </div>
    </div> 

    </div></div></div></div>

    <div class="row">
        <div class="col-xs-2 col-xs-offset-5">
            <div class="row">
                <div class="col-xs-4">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div></div></div></div>

    <?php ActiveForm::end(); ?>

</div>
