<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CapacitacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Capacitaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="capacitaciones-index">

<div class="usuaria-index">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Registro de capacitaciones
                        <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/site/index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['create'], ['class' => 'btn  btn-sm btn-success']) ?>
                    </strong>
                </div>
                <div class="panel-body">

<?php //Pjax::begin(); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cap_id',
            'cap_nombre',
            'cap_feha',
            //'cap_hora_inicio',
            //'cap_hora_termino',
            'cap_dependencia',
            'cap_total',
            //'cap_hombres',
            //'cap_mujeres',
            'tipoCapacitacion',
            ['class' => 'yii\grid\ActionColumn'],
            /*
             ['class' => 'yii\grid\ActionColumn',
                  'template'=>'{view}',
                    'header'=>'Ver',
                    'buttons'=>[
                      'view' => function ($url, $model) {   
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('yii', 'view'),'class' => 'btn btn-warning',
                        ]);                                

                      }
                  ]                            
             ],

             

             ['class' => 'yii\grid\ActionColumn',
                  'template'=>'{view}',
                    'header'=>'Actualizar',
                    //'visible'=> Yii::$app->user->identity->hasRole('Operador') || Yii::$app->user->identity->superadmin,
                    'buttons'=>[
                      'view' => function ($url, $model) {   
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'view'),'class' => 'btn btn-success',
                        ]);                                

                      }
                  ]                            
             ],
             ['class' => 'yii\grid\ActionColumn',
                  'template'=>'{view}',
                    'header'=>'Borrar',
                    //'visible'=> Yii::$app->user->identity->hasRole('Operador') || Yii::$app->user->identity->superadmin,
                    'buttons'=>[
                      'view' => function ($url, $model) {   
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'view'),'class' => 'btn btn-primary',
                        ]);                                

                      }
                  ]                            
             ],   */
        ],
    ]); ?>

<?php //Pjax::end(); ?>

</div></div></div></div>

</div> 
