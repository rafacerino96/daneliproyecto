<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CapacitacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="capacitaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cap_id') ?>

    <?= $form->field($model, 'cap_nombre') ?>

    <?= $form->field($model, 'cap_feha') ?>

    <?= $form->field($model, 'cap_hora_inicio') ?>

    <?= $form->field($model, 'cap_hora_termino') ?>

    <?php // echo $form->field($model, 'cap_dependencia') ?>

    <?php // echo $form->field($model, 'cap_total') ?>

    <?php // echo $form->field($model, 'cap_hombres') ?>

    <?php // echo $form->field($model, 'cap_mujeres') ?>

    <?php // echo $form->field($model, 'cap_tip_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
