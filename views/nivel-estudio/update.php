<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NivelEstudio */

$this->title = 'Update Nivel Estudio: ' . $model->niv_id_nivel_estudio;
$this->params['breadcrumbs'][] = ['label' => 'Nivel Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->niv_id_nivel_estudio, 'url' => ['view', 'id' => $model->niv_id_nivel_estudio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nivel-estudio-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
