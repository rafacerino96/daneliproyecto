<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NivelEstudio */

$this->title = 'Create Nivel Estudio';
$this->params['breadcrumbs'][] = ['label' => 'Nivel Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nivel-estudio-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
