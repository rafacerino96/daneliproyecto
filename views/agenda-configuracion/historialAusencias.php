<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\detail\DetailView;

 
?>
<div class="panel panel-primary" align="center">
	<div class="panel-heading">    
		<h1 class="panel-title"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?= Html::encode($this->title) ?>
			<?php if(Yii::$app->user->identity->hasPermission('createConfiguracion')){ ?>
				<?= Html::a('Agregar', ['create-ausencia'], ['class' => 'btn btn-xs btn-success pull-right']) ?>
			<?php } ?>
			<?php if (Yii::$app->user->identity->hasPermission('updateconfiguracion')){ ?>
				<?php if(!empty($ausencia)){ ?>
					<?= Html::a('Actualizar', ['update-ausencia'], ['class' => 'btn btn-xs btn-info pull-right']) ?>
				<?php } ?>
			<?php } ?>
		</h1>
	</div>
	<div class="panel-body" align="center" style="min-height: 720px; max-height: 748px; overflow-y: auto;">
		<div class="box">
			<div class="box-header" wfd-id="120">

				<h3 class="box-title">Historial de dias no laborales</h3>

			</div><!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<?php foreach ($ausencia as $a): ?>
						<?= DetailView::widget([
							'model' => $a,
							'condensed' => true,
							'hover' => true,
							'bordered' => true,
							'responsive' => true,
							'hAlign' => DetailView::ALIGN_LEFT,
							'labelColOptions' => ['style' => 'width: 15%'],
							'mode' => DetailView::MODE_VIEW,

							'attributes' => [
								 
								[
									'columns' => [
										[
											'attribute' => 'ageaus_motivo', 
											'label' => 'Motivo',
											'displayOnly' => true,
											'valueColOptions' => ['style' => 'width:75%']
										],
										 			  				 					
									],
								],
								 
								[
									'columns' => [
										[
											'attribute' => 'ageaus_inicio', 
											'label' => 'Inicio',
											'displayOnly' => true,
											'valueColOptions' => ['style' => 'width:30%']
										],
										[
											'attribute' => 'ageaus_final', 
											'label' => 'Final',
											'displayOnly' => true,
											'valueColOptions' => ['style' => 'width:30%']
										],  
										  				 					
									],
								], 
									
							],
						]); ?>
				<?php endforeach; ?>

			</div><!-- /.box-body -->
		</div>
	</div>
</div>