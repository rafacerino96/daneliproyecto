<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgendaConfiguracion */

$this->title = 'Create Agenda Configuracion';
$this->params['breadcrumbs'][] = ['label' => 'Agenda Configuracions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-configuracion-create">

    <?= $this->render('_form', [
        'ausencia' => $ausencia,
    ]) ?>

</div>
