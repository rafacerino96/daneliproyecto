<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\detail\DetailView;

$this->title = 'Ausencias';
$this->params['breadcrumbs'][] = $this->title;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<div class="panel panel-default">
            <div class="panel-heading">    
            <strong>
                    <span class="glyphicon glyphicon-th"></span> Configuración de Agenda
            </strong>
            <?php if(empty($ausencia)){ echo Html::a('Agregar', ['create'], ['class' => 'btn btn-xs btn-success pull-right']); } ?>
            <?php if(!empty($ausencia)){ echo Html::a('Actualizar', ['update'], ['class' => 'btn btn-xs btn-info pull-right']);}?>
        </h1>
    </div>
    <div class="panel-body" align="center" style="min-height: 720px; max-height: 748px; overflow-y: auto;">
        <div class="box">
            <div class="box-header" wfd-id="120">

                <h3 class="box-title">Historial de dias no laborales</h3>

            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <?php foreach ($ausencia as $a): ?>
                        <?= DetailView::widget([
                            'model' => $a,
                            'condensed' => true,
                            'hover' => true,
                            'bordered' => true,
                            'responsive' => true,
                            'hAlign' => DetailView::ALIGN_LEFT,
                            'labelColOptions' => ['style' => 'width: 15%'],
                            'mode' => DetailView::MODE_VIEW,

                            'attributes' => [
                                 
                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'age_motivo', 
                                            'label' => 'Motivo',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:75%']
                                        ],
                                                                                        
                                    ],
                                ],
                                 
                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'age_inicio', 
                                            'label' => 'Inicio',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:30%']
                                        ],
                                        [
                                            'attribute' => 'age_fin', 
                                            'label' => 'Final',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:30%']
                                        ],  
                                                                            
                                    ],
                                ], 
                                    
                            ],
                        ]); ?>
                <?php endforeach; ?>

            </div><!-- /.box-body -->
        </div>
    </div>
</div>