<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgendaConfiguracion */

$this->title = 'Update Agenda Configuracion: ' . $model->age_id_configuracion;
$this->params['breadcrumbs'][] = ['label' => 'Agenda Configuracions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->age_id_configuracion, 'url' => ['view', 'id' => $model->age_id_configuracion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agenda-configuracion-update">

    <?= $this->render('_form', [
        'ausencia' => $ausencia,
    ]) ?>

</div>
