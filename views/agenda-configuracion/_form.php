<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\form\ActiveForm;
use kartik\select2\Select2; 
use kartik\date\DatePicker;
use kartik\base\BootstrapInterface; 
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\growl\Growl; 

/* @var $this yii\web\View */
/* @var $model app\models\Agenda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agenda-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'id' => 'dynamic-form']); ?>
    <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Días no laborables
                </strong>
            </div>
        <div class="panel-body"> 
            
            <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper',
            'widgetBody' => '.container-itemx',
            'widgetItem' => '.itemx',
            'limit' => 99, //9999
            'min' => 1,
            'insertButton' => '.add-itemx',
            'deleteButton' => '.remove-itemx',
            'model' => $ausencia[0],
            'formId' => 'dynamic-form',
            'formFields' => [
               'age_inicio',
                'age_fin',
                'age_motivo',
            ],
        ]); ?>
                <div class="container-itemx"><!-- widgetContainer -->
                    <?php foreach ($ausencia as $fk => $fv): ?>
                        <div class="itemx panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading"> 
                                    <h3 class="panel-title text-center">Ausencia</h3> 
                                <div class="pull-right">
                                    <button type="button" class="add-itemx btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="remove-itemx btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php 
                                if (! $fv->isNewRecord) {
                                    echo Html::activeHiddenInput($fv, "[{$fk}]age_id_configuracion");
                                } ?>

                                <div class="row">
                                    <div class="col-md-3">
                                        <?= $form->field($fv, "[{$fk}]age_inicio")->widget(DatePicker::classname(), [
                                            'options' => [
                                            'autocomplete' => 'off',
                                            'placeholder' => 'Inicio'],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                            'autoclose' => true,
                                            'startDate' => date('Y-m-d'),
                                            'format' => 'yyyy-mm-dd']
                                    ]); ?>
                                    </div>

                                    <div class="col-md-3">
                                        <?= $form->field($fv, "[{$fk}]age_fin")->widget(DatePicker::classname(), [
                                            'options' => [
                                            'autocomplete' => 'off',
                                            'placeholder' => 'final'],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                            'autoclose' => true,
                                            'startDate' => date('Y-m-d'), 
                                            'format' => 'yyyy-mm-dd']
                                            ]); ?>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($fv, "[{$fk}]age_motivo")->textInput(['maxlength' => true, 'style' => 'border-radius: 5px;']) ?>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
        </div>
    </div>  
</div>

<div class="form-group">
    <div class="col text-center">
         <?= Html::submitButton('Guardar', ['class' => 'btn btn-success', 'id' =>'btnAusencia']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

 

