<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgendaConfiguracion */

$this->title = $model->age_id_configuracion;
$this->params['breadcrumbs'][] = ['label' => 'Agenda Configuracions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agenda-configuracion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->age_id_configuracion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->age_id_configuracion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'bordered' => true,
        'responsive' => true,
        'hAlign' => DetailView::ALIGN_LEFT,
        'labelColOptions' => ['style' => 'width: 15%'],
        'mode' => DetailView::MODE_VIEW,

        'attributes' => [
                                 
                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'age_motivo', 
                                            'label' => 'Motivo',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:75%']
                                        ],
                                                                                        
                                    ],
                                ],
                                 
                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'age_inicio', 
                                            'label' => 'Inicio',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:30%']
                                        ],
                                        [
                                            'attribute' => 'age_final', 
                                            'label' => 'Final',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:30%']
                                        ],  
                                                                            
                                    ],
                                ], 
                                    
                            ],
                        ]); ?>

</div>
