<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgendaConfiguracionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agenda-configuracion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'age_id_configuracion') ?>

    <?= $form->field($model, 'age_inicio') ?>

    <?= $form->field($model, 'age_fin') ?>

    <?= $form->field($model, 'age_motivo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
