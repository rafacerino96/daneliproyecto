<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Especialidad */

$this->title = 'Update Especialidad: ' . $model->esp_id;
$this->params['breadcrumbs'][] = ['label' => 'Especialidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->esp_id, 'url' => ['view', 'id' => $model->esp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="especialidad-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
