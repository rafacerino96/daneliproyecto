 <?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\detail\DetailView as DatailViewKartik;

/* @var $this yii\web\View */
/* @var $model app\models\Usuaria */

//$this->title = $model->reg_id;
$this->params['breadcrumbs'][] = ['label' => 'Usuarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuaria-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Detalles de Usuaria 
                        <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('Actualizar', ['update', 'id' => $model->reg_id], ['class' => 'btn btn-sm btn-primary']) ?>
                        <?= Html::a('Eliminar', ['delete', 'id' => $model->reg_id], [
                            'class' => 'btn btn-sm btn-danger',
                            'data' => [
                                'confirm' => 'Seguro quieres eliminar?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </strong>
                </div>
                <div class="panel-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reg_folio_banavim',
            'nombreCompleto',
            'reg_telefono',
            'reg_fecha_nacimiento',
            'totalHijos',
            'nacionalidad',
            'entidadFederativa',
            'municipio',
            'localidad',
            'nivelEstudio',
            'estadoCivil',
            'discapacidad',
            'reg_id_actividad_economica',
            'enfermedadCronica',
            'tipoViolencia',
            'modalidadViolencia',
            'seguro',
            'servicio',
            'ingreso',
            'ObtenerOficio',
            'AprenderOficio',
            'cabezaFamilia',
            'vivienda',
            'bienes',
            'reg_fecha_registro',
            'reg_fecha_modificacion',
            'estatus',
        ],
    ]) ?>
    </div></div></div></div>

    <?php if( Yii::$app->user->identity->hasRole(['Admin', 'profesionista', 'supervisor' ]) ){ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Documentos 
                        <?= Html::a('Guardar Documento', ['documentos/create', 'id' => $model->reg_id], ['class' => 'btn btn-sm btn-success']) ?>
                    </strong>
                </div>
                <div class="panel-body">

    <table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach ($documents as $document){
                echo "<tr>";
                echo '<th scope="row">' . $document->doc_nombre . "</th>";
                echo '<td>';
                echo Html::a('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                </svg>', ['documentos/view-document', 'id' => $document->doc_id]);
                echo Html::a('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                </svg>', ['documentos/update', 'id' => $document->doc_id]);
                echo Html::a('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                </svg>', ['documentos/delete', 'id' => $document->doc_id], [
                    'data' => [
                        'confirm' => 'Seguro quieres eliminar?',
                        'method' => 'post',
                    ],
                ]);
                echo '</td>';
                echo "</tr>";
            }
        ?>
    </tbody>
    </table>
    </div></div></div></div>

    <?php } ?>



    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Historial de citas
                    </strong>
                </div>
                <div class="panel-body">
            
                <?php foreach ($citas as $a){ ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <strong>
                                        <span class="glyphicon glyphicon-th"></span> Cita
                                        <?= Html::a('Ver', ['/citas/view', 'id' => $a->cit_id_cita], ['class' => 'btn btn-xs btn-info']) ?>
                                        <?= Html::a('Actualizar', ['/citas/update?id=' . $a->cit_id_cita], ['class' => 'btn btn-xs btn-primary']) ?>
                                        <?= Html::a('Eliminar', ['citas/delete', 'id' => $a->cit_id_cita], [
                                            'class' => 'btn btn-xs btn-danger',
                                            'data' => [
                                                'confirm' => 'Are you sure you want to delete this item?',
                                                'method' => 'post',
                                            ],
                                        ]) ?>
                                    </strong>
                                </div>
                                <div class="panel-body">

                <?= DatailViewKartik::widget([
                            'model' => $a,
                            'condensed' => true,
                            'hover' => true,
                            'bordered' => true,
                            'responsive' => true,
                            'hAlign' => DatailViewKartik::ALIGN_LEFT,
                            'labelColOptions' => ['style' => 'width: 15%'],
                            'mode' => DatailViewKartik::MODE_VIEW,

                            'attributes' => [
                                 
                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'profesionista', 
                                            'label' => 'Profesionista',
                                            'displayOnly' => true,
                                            //'valueColOptions' => ['style' => 'width:75%']
                                        ],
                                        [
                                            'attribute' => 'profesionistaEspecialidad', 
                                            'label' => 'Especialidad',
                                            'displayOnly' => true,
                                            //'valueColOptions' => ['style' => 'width:75%']
                                        ],
                                                                                        
                                    ],
                                    
                                ],
                                 
                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'cit_fecha_atencion', 
                                            'label' => 'Fecha de atención',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:30%']
                                        ],
                                        [
                                            'attribute' => 'horario', 
                                            'label' => 'Hora de atención',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:30%']
                                        ],  
                                                                            
                                    ],
                                ],

                                [
                                    'columns' => [
                                        [
                                            'attribute' => 'cit_observaciones', 
                                            'label' => 'Observaciones',
                                            'displayOnly' => true,
                                            'valueColOptions' => ['style' => 'width:75%']
                                        ],
                                                                            
                                    ],
                                ],
                                    
                            ],
                        ]); ?>

                        </div></div></div></div>
                    <?php } ?>

                        

                </div>
            </div>
        </div>
    </div>

</div>
