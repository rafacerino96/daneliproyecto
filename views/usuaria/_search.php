<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuariaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuaria-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reg_id') ?>

    <?= $form->field($model, 'reg_folio_banavim') ?>

    <?= $form->field($model, 'reg_nombre_usuaria') ?>

    <?= $form->field($model, 'reg_apellidop') ?>

    <?= $form->field($model, 'reg_apellidom') ?>

    <?php // echo $form->field($model, 'reg_telefono') ?>

    <?php // echo $form->field($model, 'reg_fecha_nacimiento') ?>

    <?php // echo $form->field($model, 'reg_total_hijos') ?>

    <?php // echo $form->field($model, 'reg_total_niños') ?>

    <?php // echo $form->field($model, 'reg_niñas') ?>

    <?php // echo $form->field($model, 'reg_id_estatus') ?>

    <?php // echo $form->field($model, 'reg_id_nivel_estudio') ?>

    <?php // echo $form->field($model, 'reg_id__estado_civil') ?>

    <?php // echo $form->field($model, 'reg_id_discapacidad') ?>

    <?php // echo $form->field($model, 'reg_id_actividad_economica') ?>

    <?php // echo $form->field($model, 'reg_id_enfermedades_cronicas') ?>

    <?php // echo $form->field($model, 'reg_id_tipo_violencia') ?>

    <?php // echo $form->field($model, 'reg_id_modalidad_violencia') ?>

    <?php // echo $form->field($model, 'reg_seguro') ?>

    <?php // echo $form->field($model, 'reg_servicio_social') ?>

    <?php // echo $form->field($model, 'reg_algun_ingreso') ?>

    <?php // echo $form->field($model, 'reg_obtener_oficio') ?>

    <?php // echo $form->field($model, 'reg_aprender_oficio') ?>

    <?php // echo $form->field($model, 'reg_cabeza_familia') ?>

    <?php // echo $form->field($model, 'reg_vivienda_propia') ?>

    <?php // echo $form->field($model, 'reg_bienes_propios') ?>

    <?php // echo $form->field($model, 'reg_fecha_registro') ?>

    <?php // echo $form->field($model, 'reg_fecha_modificacion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
