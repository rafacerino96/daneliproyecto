<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuaria */

$this->title = 'Registrar Usuaria';
$this->params['breadcrumbs'][] = ['label' => 'Usuarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuaria-create">

    <?= $this->render('_form', [
        'model' => $model,
        'nivelestudio' => $nivelestudio,
        'estadocivil' => $estadocivil,
        'discapacidad' => $discapacidad,
        'enfermedades' => $enfermedades,
        'tipos_violencia' => $tipos_violencia,
        'modalidad_violencia' => $modalidad_violencia,
        'entidades_federativas' => $entidades_federativas,
        'nacionalidad'=>$nacionalidad,
        'municipios' => $municipios,
        'localidades' => $localidades,
    ]) ?>

</div>
