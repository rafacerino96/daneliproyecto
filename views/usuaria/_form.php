<?php

use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\NivelEstudio;
use app\models\EstadoCivil;
use app\models\Discapacidad;
use app\models\EnfermedadesCronicas;
use app\models\TiposViolencia;
use app\models\ModalidadViolencia;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Usuaria */
/* @var $form yii\widgets\ActiveForm */
?>
 <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                <strong>
                    <span class="glyphicon glyphicon-th"></span> Formulario de Registro
                    <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['index'], ['class' => 'btn btn-sm btn-warning']) ?>
                </strong>
            </div>
            <div class="panel-body">
               <div class="usuaria-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_folio_banavim')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_nombre_usuaria')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_apellidop')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_apellidom')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-3"> 

        <?= $form->field($model, 'reg_nacionalidad')->widget(Select2::classname(), [
            'data' => $nacionalidad,
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_entidad_federativa')->dropDownList($entidades_federativas, [
        'id'=>'id_entidad',
        'prompt'=>'Seleccione la entidad ...',
        ]) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_minicipio')->widget(DepDrop::classname(), [
        'data' => $municipios,
        'options'=>['id'=>'subcat-id'],
        'pluginOptions'=>[
            'depends'=>['id_entidad'],
            'placeholder'=>'Select...',
            'url'=>Url::to(['/usuaria/municipios'])
        ]
    ]);?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_localidad')->widget(DepDrop::classname(), [
        'data' => $localidades,
        'options'=>['id'=>'subsubcat-id'],
        'pluginOptions'=>[
            'depends'=>['id_entidad', 'subcat-id'],
            'placeholder'=>'Select...',
            'url'=>Url::to(['/usuaria/localidades'])
        ]
    ]);?>
    </div>
</div>
<div class="row">
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_telefono')->textInput() ?>
    </div>
    <div class="col-xs-4">
    <?= $form->field($model, 'reg_fecha_nacimiento')->widget(DatePicker::classname(),[
                'options'=>['placeholder'=>'Ingrese la fecha...'],
                'pluginOptions'=>[
                    'format'=> 'yyyy-mm-dd',
                    'autoclose'=>true,
                    'endDate' => date('Y-m-d'),
                ]
     ]); ?>
    </div>
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_total_niños')->textInput() ?>
    </div>
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_niñas')->textInput() ?>
    </div>
</div>
    <div class="row">
        <div class="col-xs-3">
    <?= $form->field($model, 'reg_id_nivel_estudio')->dropDownList($nivelestudio,['prompt'=>'Seleccione el Nivel de Estudio ...']) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_id__estado_civil')->dropDownList($estadocivil,['prompt'=>'Seleccione el Estado Civil ...']) ?>
    </div>
     <div class="col-xs-3">
    <?= $form->field($model, 'reg_id_discapacidad')->dropDownList($discapacidad,['prompt'=>'Seleccione la Discapacidad...']) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'reg_id_enfermedades_cronicas')->dropDownList($enfermedades,['prompt'=>'Seleccione la Discapacidad...']) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_id_actividad_economica')->textInput(['maxlength' => true]) ?>
    </div>
      
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_seguro')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
     <div class="col-xs-2">
    <?= $form->field($model, 'reg_servicio_social')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
     <div class="col-xs-2">
    <?= $form->field($model, 'reg_algun_ingreso')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_cabeza_familia')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-xs-2">
    <?= $form->field($model, 'reg_vivienda_propia')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
     <div class="col-xs-2">
    <?= $form->field($model, 'reg_bienes_propios')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
     <div class="col-xs-2">
     <?= $form->field($model, 'reg_obtener_oficio')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
     <div class="col-xs-2">
     <?= $form->field($model, 'reg_aprender_oficio')->widget(Select2::classname(), [
            'data' => Yii::$app->params['sino'],
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    
</div>
<div class="row">
<div class="col-xs-6">
    <?= $form->field($model, 'reg_id_tipo_violencia')->widget(Select2::classname(), [
            'data' => $tipos_violencia,
            'options' => [
                'placeholder' => 'Select a state ...',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
    </div>
    <div class="col-xs-6">
    <?= $form->field($model, 'reg_id_modalidad_violencia')->widget(Select2::classname(), [
            'data' => $modalidad_violencia,
            'options' => [
                'placeholder' => 'Select a state ...',
                'multiple' => true,
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
   </div>
</div>
</div>
            </div>
        </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-md-offset-5">
            <div class="form-group">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
