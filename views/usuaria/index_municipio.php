<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\detail\DetailView;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuariaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarias atendidas por municipio';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="usuaria-index">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Usuarias atendidas por municipio
                    </strong>
                </div>
                <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                <div class="col-md-3 col-md-offset-2">
                        <div class="form-group">
                            <?php echo Select2::widget([
                                //'model' => $model,
                                'name' => 'year',
                                'value' => $year,
                                'attribute' => 'state_2',
                                'data' => $years,
                                'options' => ['placeholder' => 'Año', 'id'=>'year-id'],
                            ]);?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo DepDrop::widget([
                                'type' => DepDrop::TYPE_SELECT2,
                                'name' => 'mes',
                                'value' => $mes,
                                'data' => $meses,
                                'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                                'options'=>['id'=>'mes-id', 'placeholder' => 'Select ...'],
                                'pluginOptions'=>[
                                    'depends'=>['year-id'],
                                    //'placeholder'=>'Seleccione un mes',
                                    'url'=>Url::to(['/usuaria/meses']),
                                ],
                            ]);?>
                            <?php /*echo Select2::widget([
                                //'model' => $model,
                                'name' => 'meses',
                                'attribute' => 'state_2',
                                'data' => $meses,
                                'options' => ['placeholder' => 'Mes'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);*/?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                                <?= Html::submitButton('Filtrar', ['class' => 'btn btn-success']) ?>
                        </div>

                    </div>
                </div>

                <?php ActiveForm::end(); ?>
                
                <div class="row">

                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descartados</th>
                        <th scope="col">Sin Actualizar</th>
                        <th scope="col">En proceso</th>
                        <th scope="col">Concluidos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($table as $item){
                        echo '<tr>';
                            echo '<th>'. $item['nombre'] . '</th>';
                            echo '<th>'. $item['descartados'] . '</th>';
                            echo '<th>'. $item['sinActualizar'] . '</th>';
                            echo '<th>'. $item['enProceso'] . '</th>';
                            echo '<th>'. $item['concluidos'] . '</th>';
                        echo '</tr>';
                        } ?>
                    </tbody>
                </table>

                </div>
                    
                </div>
            </div>
        </div> 
    </div>
</div>