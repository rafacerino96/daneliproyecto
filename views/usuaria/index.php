<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuariaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarias';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="usuaria-index">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Usuarias
                        <?= Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Regresar', ['/site/index'], ['class' => 'btn btn-sm btn-warning']) ?>
                        <?= Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Crear', ['create'], ['class' => 'btn  btn-sm btn-success']) ?>
                    </strong>
                </div>
                <div class="panel-body">

                    <?= GridView::widget([
                        'dataProvider'  => $dataProvider,
                        'filterModel'   => $searchModel,
                        'columns'       => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header'  => Html::a('<i class="glyphicon glyphicon-filter"></i>', ['index'], ['title' => 'Limpiar filtros']),
                            ],

                            'reg_folio_banavim',
                            'nombreCompleto',
                            [
                                'attribute'         => 'reg_minicipio',
                                'filter'            => \app\models\Municipios::map(),
                                'filterType'        => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'options'   => [
                                        'prompt' => 'Seleccione el municipio',
                                    ],
                                ],
                                'value'             => function ($model) {
                                    return $model->municipio;
                                }
                            ],

                            [
                                'attribute'         => 'reg_id_estatus',
                                'filter'            => \app\models\Estatus::map(),
                                'filterType'        => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'options'   => [
                                        'prompt' => 'Seleccione el estatus',
                                    ],
                                ],
                                'value'             => function ($model) {
                                    return $model->estatus;
                                }
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div> 
    </div>
</div>