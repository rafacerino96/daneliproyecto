<?php

namespace app\controllers;

use Yii;
use app\models\ConfiguracionProfesionista;
use app\models\ConfiguracionProfesionistaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Profesionista;
use app\models\HorasAusenciaProfesionista;
use app\models\ConfiguracionHorario;

/**
 * ConfiguracionProfesionistaController implements the CRUD actions for ConfiguracionProfesionista model.
 */
class ConfiguracionProfesionistaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all ConfiguracionProfesionista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConfiguracionProfesionistaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ConfiguracionProfesionista model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ConfiguracionProfesionista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ConfiguracionProfesionista();
        $configuracionHorario = ConfiguracionHorario::find()->all();

        if ($model->load(Yii::$app->request->post())) {

            if($model->con_todo_el_dia == null){$model->con_todo_el_dia = 0;}
            $model->save();

            foreach (Yii::$app->request->post() as $key => $value) {//var_dump($key);

                $hora = new HorasAusenciaProfesionista();

                foreach ($configuracionHorario as $configuracion) {


                    if( 'checkbox-'.$configuracion->conf_id == $key){
                        $hora->hor_fkConfiguracionProfesionista = $model->con_id;
                        $hora->hor_fkConfiguracionHorario = $configuracion->conf_id;
                        $hora->save();
                    }
                }
                
            }//die;


            return $this->redirect(['view', 'id' => $model->con_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'profesionista'  => ArrayHelper::map(Profesionista::find()->all(), 'pro_id', 'nombreCompleto'),
            'horas'=>$horas,
            'configuracionHorario'  => $configuracionHorario,
            'horasAusenciaProfesionista' => [],
        ]);
    }

    /**
     * Updates an existing ConfiguracionProfesionista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $horasAusenciaProfesionista = HorasAusenciaProfesionista::find()->where(['hor_fkConfiguracionProfesionista' => $model->con_id])->all();
        $configuracionHorario = ConfiguracionHorario::find()->all();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            foreach (Yii::$app->request->post() as $key => $value) {//var_dump($key);

                $hora = new HorasAusenciaProfesionista();

                foreach ($configuracionHorario as $configuracion) {


                    if( 'checkbox-'.$configuracion->conf_id == $key){
                        $hora->hor_fkConfiguracionProfesionista = $model->con_id;
                        $hora->hor_fkConfiguracionHorario = $configuracion->conf_id;
                        $hora->save();
                    }
                }

                foreach ($horasAusenciaProfesionista as $key => $value) {
                    $value->delete();
                }
                
            }//die;

            return $this->redirect(['view', 'id' => $model->con_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'profesionista'  => ArrayHelper::map(Profesionista::find()->all(), 'pro_id', 'nombreCompleto'),
            'configuracionHorario'  => $configuracionHorario,
            'horasAusenciaProfesionista' => $horasAusenciaProfesionista,
        ]);
    }

    /**
     * Deletes an existing ConfiguracionProfesionista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->con_status = 0;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the ConfiguracionProfesionista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConfiguracionProfesionista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ConfiguracionProfesionista::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
