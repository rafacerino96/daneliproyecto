<?php

namespace app\controllers;

use Yii;
use app\models\Profesionista;
use app\models\ProfesionistaSearch;
use app\models\Citas;
use app\models\CitasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;
use app\models\Municipios;
use app\models\Especialidad;
use webvimark\modules\UserManagement\models\User;

/**
 * ProfesionistaController implements the CRUD actions for Profesionista model.
 */
class ProfesionistaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Profesionista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfesionistaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProfesionista($date = null)
    {
        if(!Profesionista::isProfesionista(Yii::$app->user->id)){
            return $this->redirect(['/site/index']);
        }
        $searchModel = new CitasSearch();
        $profesionista = Profesionista::findOne(['pro_id_user' => Yii::$app->user->id]);

        if($date == null){
            $date = date('Y-m-d');
        }

        if($profesionista !== null){
            $dataProvider = $searchModel->searchProfecionista(Yii::$app->request->queryParams, $date, $profesionista->pro_id);
        }else{
            $dataProvider = $searchModel->searchProfecionista(Yii::$app->request->queryParams, $date);
        }

        return $this->render('index_profesionista', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date' => $date,
        ]);
    }

    /**
     * Displays a single Profesionista model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Profesionista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profesionista();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pro_id]);
        }

        $municipios = ArrayHelper::map(Municipios::find()->where(['efe_key' => '27'])->all(),'catalog_key_mun','municipio');
        $especialidades = ArrayHelper::map(Especialidad::find()->all(),'esp_id','esp_nombre');
        $profecionistas = ArrayHelper::map(Profesionista::find()->all(),'proIdUser.id','proIdUser.username');
        $usuarios = ArrayHelper::map(User::find()->all(),'id','username');
        $usuarios = array_diff($usuarios, $profecionistas);
        //$profecionistas = implode(",", ArrayHelper::getColumn(Profesionista::find()->all(), 'pro_id_user'));
        //$usuarios = unset($usuarios[0]);
        //var_dump($usuarios);die;

        return $this->render('create', [
            'model' => $model,
            'municipios' => $municipios,
            'especialidades' => $especialidades,
            'usuarios' => $usuarios,
        ]);
    }

    /**
     * Updates an existing Profesionista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pro_id]);
        }

        $municipios = ArrayHelper::map(Municipios::find()->where(['efe_key' => '27'])->all(),'catalog_key_mun','municipio');
        $especialidades = ArrayHelper::map(Especialidad::find()->all(),'esp_id','esp_nombre');
        $profecionistas = ArrayHelper::map(Profesionista::find()->all(),'proIdUser.id','proIdUser.username');
        $usuarios = ArrayHelper::map(User::find()->all(),'id','username');
        //$thisUsuario = ArrayHelper::map(User::find()->where(['id' => $model->pro_id_user])->all(),'id','username');
        $thisUsuario = [ $model->pro_id_user => $usuarios[$model->pro_id_user]];
        $usuarios = array_diff($usuarios, $profecionistas);
        $usuarios = $usuarios + $thisUsuario;

        //var_dump($usuarios);die;
        

        return $this->render('update', [
            'model' => $model,
            'municipios' => $municipios,
            'especialidades' => $especialidades,
            'usuarios' => $usuarios,
        ]);
    }

    /**
     * Deletes an existing Profesionista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profesionista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profesionista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profesionista::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
