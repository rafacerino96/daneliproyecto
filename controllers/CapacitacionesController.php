<?php

namespace app\controllers;

use Yii;
use app\models\Capacitaciones;
use app\models\CapacitacionesSearch;
use app\models\DocumentosCapacitaciones;
use app\models\DocumentosCapacitacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\TipoCapacitacion;
use yii\helpers\ArrayHelper;

/**
 * CapacitacionesController implements the CRUD actions for Capacitaciones model.
 */
class CapacitacionesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Capacitaciones models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CapacitacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Capacitaciones model.
     * @param integer $cap_id
     * @param integer $cap_tip_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new DocumentosCapacitacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Capacitaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Capacitaciones();
        $tipocapacitacion  = ArrayHelper::map(TipoCapacitacion::find()->all(),'tip_id_capacitacion','tip_nombre');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cap_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tipocapacitacion' => $tipocapacitacion,
            ]);
        }
    }

    /**
     * Updates an existing Capacitaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cap_id
     * @param integer $cap_tip_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tipocapacitacion  = ArrayHelper::map(TipoCapacitacion::find()->all(),'tip_id_capacitacion','tip_nombre');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cap_id]);
        } else{
        return $this->render('update', [
            'model' => $model,
            'tipocapacitacion' => $tipocapacitacion,
        ]);
    }
}

    /**
     * Deletes an existing Capacitaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cap_id
     * @param integer $cap_tip_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Capacitaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cap_id
     * @param integer $cap_tip_id
     * @return Capacitaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Capacitaciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
