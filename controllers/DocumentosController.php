<?php

namespace app\controllers;

use Yii;
use app\models\Documentos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\UploadForm;
use app\models\Usuaria;
use yii\web\UploadedFile;

/**
 * DocumentosController implements the CRUD actions for Documentos model.
 */
class DocumentosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Documentos models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Documentos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Displays a single Documentos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Documentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionCreate($id = null)
    {
        if(is_null($id)){
            return $this->redirect('/usuaria');
        }

        $model = new Documentos();
        $documents = new UploadForm();
        $usuaria = Usuaria::findOne(['reg_id' => $id]);
        $message = 0;

        if(is_null($usuaria)){
            return $this->redirect('/usuaria');
        }
            
        if (Yii::$app->request->isPost) {
            $documents->imageFile = UploadedFile::getInstance($documents, 'imageFile');
            if ($documentname = $documents->upload($usuaria->reg_id)) {
                $model->doc_fkusuaria = $usuaria->reg_id;
                $model->doc_nombre = $documents->imageFile->baseName;
                $model->doc_ruta = $documentname;

                if(!$model->save()){
                    return $this->render('create', [
                        'usuaria' => $usuaria,
                        'documents' => $documents,
                        'message' => 3,
                        'usuaria' => $id,
                    ]);
                }
                //return $documentname;
            }else{
                return $this->render('create', [
                    'usuaria' => $usuaria,
                    'documents' => $documents,
                    'message' => 2,
                    'usuaria' => $id,
                ]);
            }

            $message = 1;
        }

        return $this->render('create', [
            'usuaria' => $usuaria,
            'documents' => $documents,
            'message' => $message,
            'usuaria' => $id,
        ]);
    }

    /**
     * Updates an existing Documentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['usuaria/' . $model->doc_fkusuaria]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Documentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if($model = $this->findModel($id)){
            $model->doc_estatus = 0;
            if($model->save()){
                return $this->redirect(['usuaria/' . $model->doc_fkusuaria]);
            }
        }

        return $this->redirect(['index']);
    }

    public function actionViewDocument($id)
    {
        if($document = Documentos::findOne(['doc_id' => $id])){
            return $this->redirect(['uploads/' . $document->doc_ruta]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Documentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documentos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
