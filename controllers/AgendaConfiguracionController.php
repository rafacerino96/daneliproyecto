<?php

namespace app\controllers;

use Yii;
use app\models\AgendaConfiguracion;
use app\models\AgendaConfiguracionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\base\ModelDinamicoAgendaConfiguracion;

 
class AgendaConfiguracionController extends Controller
{ 
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    } 
    public function actionIndex()
    {
        return $this->render('index', [
                //'ausencia' => AgendaConfiguracion::find()->where(['>','age_fin', date('Y-m-d')])->andWhere(['age_status'=>1])->all(),
                'ausencia' => AgendaConfiguracion::find()->where(['age_status'=>1])->all(),
            ]);
    } 
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    } 
    public function actionCreate()
    {
        $fecha=  true;
        $ausencia= [new AgendaConfiguracion];

        if( !empty(Yii::$app->request->post('AgendaConfiguracion')) ){
           
           $ausencia = ModelDinamicoAgendaConfiguracion::createMultiple(AgendaConfiguracion::classname()); //var_dump($ausencia);die;
            ModelDinamicoAgendaConfiguracion::loadMultiple($ausencia, Yii::$app->request->post());//var_dump($ausencia);die;
            // var_dump($configuracion);die();
            foreach ($ausencia as $a) {
 
                //VALIDACION    
                $valid = 0;
                $valid = ModelDinamicoAgendaConfiguracion::validateMultiple($ausencia);
                
                     if ($valid) {
                        $transaction = \Yii::$app->db->beginTransaction();

                        $flag = 1;
                        
                        try { 

                            if($flag){
                                foreach ($ausencia as $a) {
                                    $a->age_status=1;
                                    if(!$fecha = $this->validFecha($a->age_inicio, $a->age_fin)){ 
                                        $flag = false;

                                        //$transaction->rollBack();
                                        break; 
                                    }
                                    if (!($flag = $a->save(false))) {

                                        //$transaction->rollBack();
                                        break; 
                                        
                                    } 
                                }
                            } 
                            if ($flag) {
                                $transaction->commit();

                                return $this->redirect(['agenda-configuracion/index']);
                            }
                            else{
                                $transaction->rollBack();
                            } 
                            
                        } catch (Exception $e) {
                            $transaction->rollBack();
                        }
                    } 
                
            }
        }
        // var_dump($fecha);die();
            return $this->render('create',[ 
                'ausencia'=>$ausencia,

            ]);
    } 
    public function actionUpdate()
    {
        $fecha = true;
       //trae los dias festivos mayores a hoy y con estatus 1
        //$ausencia = AgendaConfiguracion::find()->where(['>','age_fin', date('Y-m-d')])->andWhere(['age_status'=>1])->all();//var_dump(Yii::$app->request->post());die;
        $ausencia = AgendaConfiguracion::find()->where(['age_status'=>1])->all();//var_dump(Yii::$app->request->post());die;

        if( !empty(Yii::$app->request->post()) ){ 

           $oldAusencia = $ausencia; 
            $ausencia = ModelDinamicoAgendaConfiguracion::createMultiple(AgendaConfiguracion::classname(), $ausencia); //var_dump($ausencia);die;
            ModelDinamicoAgendaConfiguracion::loadMultiple($ausencia, Yii::$app->request->post());//var_dump($ausencia);die;
             
            //VALIDACION    
            $valid = 1; 

             if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();

                $flag = 1; 

                try {
                    
                    if($flag){
                        //modificar los que se quedan
                        foreach ($ausencia as $c) {
                            $c->age_status=1;


                            if(!$fecha = $this->validFecha($c->age_inicio , $c->age_fin)){ 
                                $flag = false; 
                                break; 
                            }
                             
                            if (! ($flag = $c->save(false))) {
                                $transaction->rollBack();
                                break;
                            } 
                        }
                    } 
                    //eliminar los que se van
                    if($flag){
                         $deleted = $this->arrayDeletedAusencia($oldAusencia, $ausencia);//var_dump($ausencia);die();
                        foreach ($deleted as $c) {

                            foreach ($oldAusencia as $key => $a) {
                                if($c->age_id_configuracion == $a->age_id_configuracion){
                                    $a->age_status = 0;
                                    if (! ($flag = $a->save(false))) {
                                        $transaction->rollBack();
                                        break;
                                    }
                                } 
                            }
                        }                       
                    } 
                    if($flag){
                        $transaction->commit();
                        return $this->redirect(['agenda-configuracion/index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } 
            //return $this->redirect(['index']); 
        }
        return $this->render('update',  [
            'ausencia'=>$ausencia,

        ]);
    } 
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    } 
    protected function findModel($id)
    {
        if (($model = AgendaConfiguracion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
     public function validFecha($fechaInicial, $fechaFinal){
        //var_dump($fechaInicial, $fechaFinal); die();
        $inicial = strtotime($fechaInicial);
        $final = strtotime($fechaFinal);
        if($final >= $inicial){
            return true;
        }
        else{
            return false;
        }
    }
    private function arrayDeletedAusencia($old, $new){
         $arrayDeleted = array();

         foreach ($old as $key => $o) {
             $deleted = false;
             foreach ($new as $key => $n) {
                $n = (object) $n;
                 if($o->age_id_configuracion == $n->age_id_configuracion){
                    $deleted = true;
                 }
             }
             if($deleted == false){
                array_push($arrayDeleted, $o);
             }
         }
         return $arrayDeleted;
    }

}
