<?php

namespace app\controllers;

use Yii;
use app\models\DocumentosCapacitaciones;
use app\models\DocumentosCapacitacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\UploadForm;
use app\models\Capacitaciones;
use yii\web\UploadedFile;

/**
 * DocumentosCapacitacionesController implements the CRUD actions for DocumentosCapacitaciones model.
 */
class DocumentosCapacitacionesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [ 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all DocumentosCapacitaciones models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $searchModel = new DocumentosCapacitacionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Displays a single DocumentosCapacitaciones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if($document = DocumentosCapacitaciones::findOne(['docc_id' => $id])){
            return $this->redirect(['capacitaciones-uploads/' . $document->docc_ruta]);
        }

        return $this->redirect(['/capacitaciones/index']);
    }

    /**
     * Creates a new DocumentosCapacitaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        /*$model = new DocumentosCapacitaciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->docc_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);*/

        if(is_null($id)){
            return $this->redirect('/capacitaciones');
        }

        $model = new DocumentosCapacitaciones();
        $documents = new UploadForm();
        $capacitacion = Capacitaciones::findOne(['cap_id' => $id]);
        $message = 0;

        if(is_null($capacitacion)){
            return $this->redirect('/capacitaciones');
        }
            
        if (Yii::$app->request->isPost) {
            $documents->imageFile = UploadedFile::getInstance($documents, 'imageFile');
            if ($documentname = $documents->uploadCapacitacion($capacitacion->cap_id)) {
                $model->docc_fkcapacitacion = $capacitacion->cap_id;
                $model->docc_nombre = $documents->imageFile->baseName;
                $model->docc_ruta = $documentname;//$model->validate();var_dump($model->errors);die;

                if(!$model->save()){
                    return $this->render('create', [
                        'capacitacion' => $capacitacion,
                        'documents' => $documents,
                        'message' => 3,
                    ]);
                }
                //return $documentname;
            }else{
                return $this->render('create', [
                    'capacitacion' => $capacitacion,
                    'documents' => $documents,
                    'message' => 2,
                ]);
            }

            $message = 1;
        }

        return $this->render('create', [
            'capacitacion' => $capacitacion,
            'documents' => $documents,
            'message' => $message,
        ]);
    }

    /**
     * Updates an existing DocumentosCapacitaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->docc_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DocumentosCapacitaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->docc_estatus = 0;
        $model->save();

        return $this->redirect(['/capacitaciones/view', 'id' => $model->docc_fkcapacitacion]);
    }

    /**
     * Finds the DocumentosCapacitaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DocumentosCapacitaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DocumentosCapacitaciones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
