<?php

namespace app\controllers;

use Yii;
use app\models\Usuaria;
use app\models\UsuariaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\NivelEstudio;
use app\models\EstadoCivil;
use app\models\Discapacidad;
use app\models\EnfermedadesCronicas;
use app\models\TiposViolencia;
use app\models\ModalidadViolencia;
use app\models\EntidadesFederativas;
use app\models\Municipios;
use app\models\Nacionalidad;
use app\models\Localidades;

use app\models\Documentos; 
use app\models\Citas; 

/**
 * UsuariaController implements the CRUD actions for Usuaria model.
 */
class UsuariaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]; */

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Usuaria models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuariaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexMunicipio()
    {
        $year = 0;
        $mes = null;
        $years = null;
        $meses = null;
        $request = null;
        $mesQuery = null;
        $yearQuery = null;

        if($request = Yii::$app->request->post()){
            $year = $request['year'];
            $mes = $request['mes'];
        }

        if($year == '0' || $year == null ){
            $meses = ['0' => 'Ninguno'];
        }else{
            $meses = Usuaria::meses();
        }

        if($year != '0' && $year != null){
            $yearQuery = $year;
        }

        if($mes != '0' && $mes != '13' && $mes != null){
            $mesQuery = $mes;
        }

        return $this->render('index_municipio', [
            'table' => $this->queryTabla($yearQuery , $mesQuery),
            'years' => Usuaria::years(),
            'meses' => $meses,
            'year' => $year,
            'mes' => $mes,
        ]);
    }

    public function queryTabla($year = null, $mes = null){
        $descartados = $this->queryEstadistica(1, $year, $mes);
        $sinActualizar = $this->queryEstadistica(2, $year, $mes);
        $enProceso = $this->queryEstadistica(3, $year, $mes);
        $concluidos = $this->queryEstadistica(4, $year, $mes);
        $arrayTabla = [];
        $municipios = Municipios::map();
        unset($municipios[999]);

        foreach ($municipios as $key => $municipio){
            $item = [
                'id' => $key, 
                "nombre" => $municipio, 
                'descartados' => isset($descartados[$key]) ? $descartados[$key] : 0,
                'sinActualizar' => isset($sinActualizar[$key]) ? $sinActualizar[$key] : 0,
                'enProceso' => isset($enProceso[$key]) ? $enProceso[$key] : 0,
                'concluidos' => isset($concluidos[$key]) ? $concluidos[$key] : 0,
            ];

            $arrayTabla[] = $item;
        }

        return $arrayTabla;
    }

    public function queryEstadistica($estatus = null, $year = null, $mes = null){
        $query = (new \yii\db\Query())
            ->select(['reg_minicipio', 'COUNT(*) as numero'])
            ->from('usuaria')
            ->where(['reg_estatus' => 1]);

            if($estatus != null){
                $query->andWhere(['reg_id_estatus' => $estatus]);
            }

            if($year != null){
                $query->andWhere(['YEAR(reg_fecha_modificacion)' => $year]);
            }

            if($mes != null){
                $query->andWhere(['MONTH(reg_fecha_modificacion)' => $mes]);
            }

        $query->groupBy(['reg_minicipio']);
        $command = $query->createCommand();
        $rows = $command->queryAll();

        return ArrayHelper::map($rows, 'reg_minicipio', 'numero');    
    }

    /**
     * Displays a single Usuaria model.
     * @param integer $reg_id
     * @param integer $reg_id_estatus
     * @param integer $reg_id_nivel_estudio
     * @param integer $reg_id__estado_civil
     * @param integer $reg_id_discapacidad
     * @param integer $reg_id_enfermedades_cronicas
     * @param integer $reg_id_tipo_violencia
     * @param integer $reg_id_modalidad_violencia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'documents' => Documentos::find()->where(['doc_fkusuaria' => $id, 'doc_estatus' => 1])->all(),
            'citas' => Citas::find()->where(['cit_id_usuaria' => $id])->all(),
        ]);
    }

    /**
     * Creates a new Usuaria model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuaria();
        $nivelestudio  = ArrayHelper::map(NivelEstudio::find()->all(),'niv_id_nivel_estudio','niv_nombre');
        $estadocivil  = ArrayHelper::map(EstadoCivil::find()->all(),'civ_id_estado_civil','civ_nombre');
        $discapacidad  = ArrayHelper::map(Discapacidad::find()->all(),'dis_id_discapacidad','dis_nombre');
        $enfermedades  = ArrayHelper::map(EnfermedadesCronicas::find()->all(),'enf_id_enfermedades_cronicas','enf_nombre');
        $tipos_violencia  = ArrayHelper::map(TiposViolencia::find()->all(),'vio_id_tipo','vio_nombre');
        $modalidad_violencia  = ArrayHelper::map(ModalidadViolencia::find()->all(),'mod_id_violencia','mod_nombre');
        $entidades_federativas  = ArrayHelper::map(EntidadesFederativas::find()->all(),'catalog_key','entidad_federativa');
        $nacionalidad = ArrayHelper::map(Nacionalidad::find()->all(),'codigo_pais', 'pais');
        
        if ($model->load(Yii::$app->request->post())) {
            $model->reg_id_tipo_violencia = json_encode($model->reg_id_tipo_violencia);
            $model->reg_id_modalidad_violencia = json_encode($model->reg_id_modalidad_violencia);
            $model->reg_fecha_registro = date("Y-m-d H:i:s");
            $model->reg_fecha_modificacion = date("Y-m-d H:i:s");
            $model->reg_id_estatus = 1;

            $model->save();
            return $this->redirect(['view', 'id' => $model->reg_id]);
        }
else{
        return $this->render('create', [
            'model' => $model,
            'nivelestudio' => $nivelestudio,
            'estadocivil' => $estadocivil,
            'discapacidad' => $discapacidad,
            'enfermedades' => $enfermedades,
            'tipos_violencia' => $tipos_violencia,
            'modalidad_violencia' => $modalidad_violencia,
            'entidades_federativas' => $entidades_federativas,
            'nacionalidad' =>$nacionalidad,
            'municipios' => [],
            'localidades' => [],
        ]);
    }
}
    /**
     * Updates an existing Usuaria model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $reg_id
     * @param integer $reg_id_estatus
     * @param integer $reg_id_nivel_estudio
     * @param integer $reg_id__estado_civil
     * @param integer $reg_id_discapacidad
     * @param integer $reg_id_enfermedades_cronicas
     * @param integer $reg_id_tipo_violencia
     * @param integer $reg_id_modalidad_violencia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nivelestudio  = ArrayHelper::map(NivelEstudio::find()->all(),'niv_id_nivel_estudio','niv_nombre');
        $estadocivil  = ArrayHelper::map(EstadoCivil::find()->all(),'civ_id_estado_civil','civ_nombre');
        $discapacidad  = ArrayHelper::map(Discapacidad::find()->all(),'dis_id_discapacidad','dis_nombre');
        $enfermedades  = ArrayHelper::map(EnfermedadesCronicas::find()->all(),'enf_id_enfermedades_cronicas','enf_nombre');
        $tipos_violencia  = ArrayHelper::map(TiposViolencia::find()->all(),'vio_id_tipo','vio_nombre');
        $modalidad_violencia  = ArrayHelper::map(ModalidadViolencia::find()->all(),'mod_id_violencia','mod_nombre');
        $entidades_federativas  = ArrayHelper::map(EntidadesFederativas::find()->all(),'catalog_key','entidad_federativa');
        $nacionalidad = ArrayHelper::map(Nacionalidad::find()->all(),'codigo_pais', 'pais');

        if ($model->load(Yii::$app->request->post())) {
            $model->reg_id_tipo_violencia = json_encode($model->reg_id_tipo_violencia);
            $model->reg_id_modalidad_violencia = json_encode($model->reg_id_modalidad_violencia);
            $model->reg_fecha_modificacion = date("Y-m-d H:i:s");
            $model->save();
            return $this->redirect(['view', 'id' => $model->reg_id]);
        }

        $model->reg_id_tipo_violencia = json_decode($model->reg_id_tipo_violencia);
        $model->reg_id_modalidad_violencia = json_decode($model->reg_id_modalidad_violencia);
        $municipios = ArrayHelper::map(Municipios::find()->andWhere(['efe_key' => $model->reg_entidad_federativa])->all(),'catalog_key_mun','municipio');
        $localidades = ArrayHelper::map(Localidades::find()->andWhere(['cve_ent' => $model->reg_entidad_federativa, 'cve_mun' => $model->reg_minicipio])->all(),'cve_loc','nom_loc');

        return $this->render('update', [
            'model' => $model,
             'nivelestudio' => $nivelestudio,
            'estadocivil' => $estadocivil,
            'discapacidad' => $discapacidad,
            'enfermedades' => $enfermedades,
            'tipos_violencia' => $tipos_violencia,
            'modalidad_violencia' => $modalidad_violencia,
            'entidades_federativas' => $entidades_federativas,
            'nacionalidad' =>$nacionalidad,
            'municipios' => $municipios,
            'localidades' => $localidades,
        ]);
    }

    /**
     * Deletes an existing Usuaria model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $reg_id
     * @param integer $reg_id_estatus
     * @param integer $reg_id_nivel_estudio
     * @param integer $reg_id__estado_civil
     * @param integer $reg_id_discapacidad
     * @param integer $reg_id_enfermedades_cronicas
     * @param integer $reg_id_tipo_violencia
     * @param integer $reg_id_modalidad_violencia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->reg_estatus = 0;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuaria model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuaria the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuaria::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMunicipios() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Municipios::find()->andWhere(['efe_key' => $id])->asArray()->all();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $account) {
                    $out[] = ['id' => $account['catalog_key_mun'], 'name' => $account['municipio']];
                    if ($i == 0) {
                        $selected = $account['catalog_key_mun'];
                    }
                }
                // Shows how you can preselect a value
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public function actionLocalidades() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $ent = empty($ids[0]) ? null : $ids[0];
            $mun = empty($ids[1]) ? null : $ids[1];
            $list = Localidades::find()->andWhere(['cve_ent' => $ent, 'cve_mun' => $mun])->asArray()->all();//return $list;
            $selected  = null;
            if ($ent != null && $mun != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $account) {
                    $out[] = ['id' => $account['cve_loc'], 'name' => $account['nom_loc']];
                    if ($i == 0) {
                        $selected = $account['cve_loc'];
                    }
                }
                // Shows how you can preselect a value
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public function actionMeses() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Usuaria::meses();
            //$selected  = null;
            if($id == '0'){
                $out[] = ['id' => '0', 'name' => 'Ninguno'];
                return ['output' => $out, 'selected' => '0'];
            }else{
                $selected = '';
                foreach ($list as $i => $account) {
                    $out[] = ['id' => $i, 'name' => $account];
                }
                // Shows how you can preselect a value
                return ['output' => $out, 'selected' => '13'];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
}
