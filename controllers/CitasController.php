<?php

namespace app\controllers;

use Yii;
use app\models\Citas;
use app\models\CitasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use app\models\Municipios;
use app\models\Profesionista;
use app\models\Usuaria;
use app\models\ConfiguracionHorario;
use app\models\TipoServicio;
use app\models\Estatus;
use app\models\Dias;
use app\models\AgendaConfiguracion;
use app\models\ConfiguracionProfesionista;
use app\models\HorasAusenciaProfesionista;

/**
 * CitasController implements the CRUD actions for Citas model.
 */
class CitasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Citas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CitasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Citas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Citas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($municipio = null, $profesionista = null, $usuaria = null, $proceso = null, $servicio = null, $estatus = null)
    {
        $model = new Citas();
        
        if($municipio != null){$model->cit_municipio = $municipio;}
        if($profesionista != null){$model->cit_id_profesionista = $profesionista;}
        if($usuaria != null){$model->cit_id_usuaria = $usuaria;}
        if($proceso != null){$model->cit_proceso = $proceso;}
        if($servicio != null){$model->cit_servicio = $servicio;}
        if($estatus != null){$model->cit_id_estatus = $estatus;}

        if ($model->load(Yii::$app->request->post())) {
            $model->cit_id_estatus = 1;
            $model->cit_fecha_registro = date("Y-m-d H:i:s");
            $model->cit_fecha_modificacion = date("Y-m-d H:i:s");

            $model->save();

            return $this->redirect(['view', 'id' => $model->cit_id_cita]);
        }

        $municipios = ArrayHelper::map(Municipios::find()->where(['efe_key' => '27'])->all(),'catalog_key_mun','municipio');
        $usuarias = ArrayHelper::map(Usuaria::find()->all(),'reg_id','folioNombreCompleto'); 
        $servicios = ArrayHelper::map(TipoServicio::find()->all(),'serv_id_tipo','serv_nombre');
        $estatus = ArrayHelper::map(Estatus::find()->all(),'est_id_estatus','est_nombre');
        $dias = $this->getDias();//var_dump($dias);die;
        $datesDisabled = $this->getDateDisabled(AgendaConfiguracion::find()->where(['age_status' => 1])->all());
        if($model->cit_id_profesionista !== null){
            $dates = $this->getDateDisabledProfesionista(ConfiguracionProfesionista::find()->where(['con_status' => 2, 'con_todo_el_dia' => 1, 'con_fkProfesionista' => $model->cit_id_profesionista])->all());
            $datesDisabled = array_merge($dates, $datesDisabled);
            //var_dump($datesDisabled);die;
        }
        //var_dump(AgendaConfiguracion::find()->where(['age_status' => 1])->all());die;

        return $this->render('create', [
            'model' => $model,
            'municipios' => $municipios,
            'profesionistas' => $profesionista != null ? ArrayHelper::map(Profesionista::find()->where(['pro_municipio_atencion' => $model->cit_municipio])->all(),'pro_id','nombreCompleto') : [],
            'usuarias' => $usuarias,
            'servicios' => $servicios,
            'estatus' => $estatus,
            'horarios' => [],
            'escenario' => 'create',
            'dias' => $dias,
            'datesDisabled' => $datesDisabled,
            'modelUsuaria' => null,
        ]);
    }

    /**
     * Updates an existing Citas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($municipio = null, $profesionista = null, $usuaria = null, $proceso = null, $servicio = null, $estatus = null, $id)
    {
        $model = $this->findModel($id);

        if($municipio != null){$model->cit_municipio = $municipio;}
        if($profesionista != null){$model->cit_id_profesionista = $profesionista;}
        if($usuaria != null){$model->cit_id_usuaria = $usuaria;}
        if($proceso != null){$model->cit_proceso = $proceso;}
        if($servicio != null){$model->cit_servicio = $servicio;}
        if($estatus != null){$model->cit_id_estatus = $estatus;}

        $modelUsuaria = Usuaria::findOne(['reg_id' => $model->cit_id_usuaria]);//var_dump(Yii::$app->request->post());die;

        if ($model->load(Yii::$app->request->post()) && $modelUsuaria->load(Yii::$app->request->post()) ) {

            $model->cit_fecha_modificacion = date("Y-m-d H:i:s");
            $model->save();

            $modelUsuaria->reg_fecha_modificacion = date("Y-m-d H:i:s");
            $modelUsuaria->save();

            return $this->redirect(['view', 'id' => $model->cit_id_cita]);
        }

        $municipios = ArrayHelper::map(Municipios::find()->where(['efe_key' => '27'])->all(),'catalog_key_mun','municipio');
        $profesionistas = ArrayHelper::map(Profesionista::find()->where(['pro_municipio_atencion' => $model->cit_municipio])->all(),'pro_id','nombreCompleto');
        $usuarias = ArrayHelper::map(Usuaria::find()->all(),'reg_id','folioNombreCompleto');
        $servicios = ArrayHelper::map(TipoServicio::find()->all(),'serv_id_tipo','serv_nombre');
        $estatus = ArrayHelper::map(Estatus::find()->all(),'est_id_estatus','est_nombre');
        $horarios = ArrayHelper::map(ConfiguracionHorario::find()->all(), 'conf_id', 'conf_descripcion');
        $listCitas = ArrayHelper::map(Citas::find()->andWhere(['cit_id_profesionista' => $model->cit_id_profesionista, 'cit_fecha_atencion' => $model->cit_fecha_atencion])->all(),'citHorario.conf_id','citHorario.conf_descripcion');
        $citaHorario = ArrayHelper::map(Citas::find()->andWhere(['cit_id_cita' => $model->cit_id_cita])->all(),'citHorario.conf_id','citHorario.conf_descripcion');
        $dias = $this->getDias();

        $disabledProfesionista =  [];
            
        $configuracionProfesionista = ConfiguracionProfesionista::find()->where(['con_status' => 2, 'con_todo_el_dia' => 0, 'con_fkProfesionista' => $model->cit_id_profesionista])->all();
        foreach ($configuracionProfesionista as $confProfesionista){
            if($this->check_in_range($confProfesionista->con_dia_inicial, $confProfesionista->con_dia_final, $model->cit_fecha_atencion)){
                $arrayProfesionista = ArrayHelper::map(HorasAusenciaProfesionista::find()->where(['hor_fkConfiguracionProfesionista' => $confProfesionista->con_id])->all(), 'horFkConfiguracionHorario.conf_id', 'horFkConfiguracionHorario.conf_descripcion');
                $disabledProfesionista = $disabledProfesionista + $arrayProfesionista;
            }
        }

        $ArrayDisabled = $listCitas + $disabledProfesionista;

        $horarios = array_diff($horarios, $ArrayDisabled);
        $horarios = $horarios + $citaHorario;
        asort($horarios);


        $datesDisabled = $this->getDateDisabled(AgendaConfiguracion::find()->where(['age_status' => 1])->all());

        return $this->render('update', [
            'model' => $model,
            'municipios' => $municipios,
            'profesionistas' => $profesionistas ,
            'usuarias' => $usuarias,
            'servicios' => $servicios,
            'estatus' => $estatus,
            'horarios' => $horarios,
            'escenario' => 'update',
            'dias' => $dias,
            'datesDisabled' => $datesDisabled,
            'modelUsuaria' => $modelUsuaria,
        ]);
    }

    /**
     * Deletes an existing Citas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Citas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Citas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Citas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    //funciones nuevas 
    public function actionProfesionistas() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = Profesionista::find()->andWhere(['pro_municipio_atencion' => $id])->asArray()->all();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $account) {
                    $out[] = ['id' => $account['pro_id'], 'name' => $account['pro_nombre'] . " ". $account['pro_apellidop'] . " " . $account['pro_apellidom'] ];
                    //$out[] = ['id' => $account['pro_id'], 'name' => Html::a('Update', ['update', 'id' => $model->reg_id], ['class' => 'btn btn-primary']) ];
                    if ($i == 0) {
                        $selected = null;
                    }
                }
                // Shows how you can preselect a value
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public function actionHorarios() {
        /*Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $profesionista = empty($ids[0]) ? null : $ids[0];
            $fecha = empty($ids[1]) ? null : $ids[1];
            $listCitas = Citas::find()->andWhere(['cit_id_profesionista' => $profesionista, 'cit_fecha_atencion' => $fecha])->asArray()->all();//return $list;
            $listHorarios = ConfiguracionHorario::find()->asArray()->all();//return $listHorarios;
            $selected  = null;
            if ($profesionista != null && $fecha != null && count($listHorarios) > 0) {
                $selected = '';
                foreach ($listHorarios as $i => $account) {
                    $out[] = ['id' => $account['conf_id'], 'name' => $account['conf_descripcion']];
                    if ($i == 0) {
                        $selected = $account['conf_id'];
                    }
                }
                // Shows how you can preselect a value
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];*/

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $profesionista = empty($ids[0]) ? null : $ids[0];
            $fecha = empty($ids[1]) ? null : $ids[1];
            $listCitas = ArrayHelper::map(Citas::find()->andWhere(['cit_id_profesionista' => $profesionista, 'cit_fecha_atencion' => $fecha])->all(),'citHorario.conf_id','citHorario.conf_descripcion');
            $listHorarios = ArrayHelper::map(ConfiguracionHorario::find()->all(),'conf_id','conf_descripcion');
            
            $disabledProfesionista =  [];
            
            $configuracionProfesionista = ConfiguracionProfesionista::find()->where(['con_status' => 2, 'con_todo_el_dia' => 0, 'con_fkProfesionista' => $profesionista])->all();
            foreach ($configuracionProfesionista as $confProfesionista){
                if($this->check_in_range($confProfesionista->con_dia_inicial, $confProfesionista->con_dia_final, $fecha)){
                    $arrayProfesionista = ArrayHelper::map(HorasAusenciaProfesionista::find()->where(['hor_fkConfiguracionProfesionista' => $confProfesionista->con_id])->all(), 'horFkConfiguracionHorario.conf_id', 'horFkConfiguracionHorario.conf_descripcion');
                    $disabledProfesionista = $disabledProfesionista + $arrayProfesionista;
                }
            }

            $ArrayDisabled = $listCitas + $disabledProfesionista;//return $ArrayDisabled;

            $arrayHorarios = array_diff($listHorarios, $ArrayDisabled);

            $selected  = null;
            if ($profesionista != null && $fecha != null && count($arrayHorarios) > 0) {
                $selected = '';
                foreach ($arrayHorarios as $key => $account) {
                    $out[] = ['id' => $key, 'name' => $account];
                    if ($key == 0) {
                        $selected = $account[$key];
                    }
                }
                // Shows how you can preselect a value
                return ['output' => $out, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public function getDias(){
        $arrayDias = [];
        $dias = Dias::findOne(['dia_id' => 1]);

        if($dias->dia_domingo == 1){
            $arrayDias = array_merge($arrayDias,array(0));
        }

        if($dias->dia_lunes == 1){
            $arrayDias = array_merge($arrayDias,array(1));
        }

        if($dias->dia_martes == 1){
            $arrayDias = array_merge($arrayDias,array(2));
        }

        if($dias->dia_miercoles == 1){
            $arrayDias = array_merge($arrayDias,array(3));
        }

        if($dias->dia_jueves == 1){
            $arrayDias = array_merge($arrayDias,array(4));
        }

        if($dias->dia_viernes == 1){
            $arrayDias = array_merge($arrayDias,array(5));
        }

        if($dias->dia_sabado == 1){
            $arrayDias = array_merge($arrayDias,array(6));
        }

        return $arrayDias;
    }

    public function getDateDisabled($fechas){

        $arratDates = [];

        foreach($fechas as $rango){
            $fecha1 = $rango->age_inicio;
            $fecha2 = $rango->age_fin;

            for($i=$fecha1;$i<=$fecha2;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                $arratDates = array_merge($arratDates, [$i]);
            }
        }

        return $arratDates;

        
    }

    public function getDateDisabledProfesionista($fechas){

        $arratDates = [];

        foreach($fechas as $rango){
            $fecha1 = $rango->con_dia_inicial;
            $fecha2 = $rango->con_dia_final;

            for($i=$fecha1;$i<=$fecha2;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                $arratDates = array_merge($arratDates, [$i]);
            }
        }

        return $arratDates;

        
    }

    function check_in_range($date_start, $date_end, $date_now) {
        $date_start = strtotime($date_start);
        $date_end = strtotime($date_end);
        $date_now = strtotime($date_now);
        if (($date_now >= $date_start) && ($date_now <= $date_end))
            return true;
        return false;
     }

}
